--------------------------------------------------------------------------------
--  Function......... : ParseMoneyBlockConfig
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.ParseMoneyBlockConfig ( )
--------------------------------------------------------------------------------
 if(this.xMoneyBlockConfig ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xMoneyBlockConfig ( ))
       
    if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.recurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htGameConfig ( ),ht)
        
    end
    
  
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
