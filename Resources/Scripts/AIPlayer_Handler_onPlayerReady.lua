--------------------------------------------------------------------------------
--  Handler.......... : onPlayerReady
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onPlayerReady ( nUserID  )
--------------------------------------------------------------------------------

    if(hashtable.contains ( this.htPlayerReady ( ), ""..nUserID ))then
         hashtable.set( this.htPlayerReady ( ), ""..nUserID, true )
    else
        hashtable.add ( this.htPlayerReady ( ), ""..nUserID, true )
    end
    --log.message ( "Player "..nUserID.." is Ready" )

    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onPlayerReady", nUserID )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
