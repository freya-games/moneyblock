--------------------------------------------------------------------------------
--  Function......... : CloseContainerCardsHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CloseContainerCardsHUD ( )
--------------------------------------------------------------------------------
	
    local sAction = "PlayerMainHUD.CloseContainerCards"
    if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction ) end
    hud.callAction ( this.getUser ( ), sAction )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
