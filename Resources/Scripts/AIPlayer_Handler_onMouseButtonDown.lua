--------------------------------------------------------------------------------
--  Handler.......... : onMouseButtonDown
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onMouseButtonDown ( nButton, nPointX, nPointY, nRayPntX, nRayPntY, nRayPntZ, nRayDirX, nRayDirY, nRayDirZ )
--------------------------------------------------------------------------------
       --Process only if the mouse click is in the user viewport
       if(this.bGameStarted ( ))
       then

            if ( ( nPointX > -1 ) and ( nPointX < 1 ) and ( nPointY > -1 ) and ( nPointY < 1 ) )
            then
            if( not hud.getUnderCursorComponent ( this.getUser ( ) ) )
            then
                user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onUnselectAllBuy" )

            end
            end
        end
    --------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
