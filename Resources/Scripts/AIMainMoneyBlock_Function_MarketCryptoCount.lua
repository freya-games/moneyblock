--------------------------------------------------------------------------------
--  Function......... : MarketCryptoCount
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.MarketCryptoCount (nCardIndex )
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htMarketCards ( ), "MarketCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="MarketCards.MarketCard."..nCardIndex
    else

        sTag="MarketCards.MarketCard"
    end

--     local sName =hashtable.get (this.htMarketCards ( ),sTag..".Name")
--     log.warning ( sName.." Market" )


    local nMin =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Min")
    if(nMin)
    then

        local nMax =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Max")

        local nAmount=nMin


            local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")


            for j=0,nCount-1
            do
                local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")

                if(this.getResourceType ( sCryptoID )=="CRYPTO")
                then

                         if(nMin~=nMax)
                then
                    nAmount=math.roundToNearestInteger (  math.random ( nMin,nMax ))
                end


                    for i =0, this.nPlayerCount ( )-1
                    do
                        local nLock=  hashtable.get (this.htRuntimeData ( ), "Player."..i.."."..sCryptoID..".Count.Lock" )
                      if(nLock)
                      then
                        user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),i) ),"AIPlayer","onReceiveMessage" ,"[MARKET CARD] Crypto not affected "..sCryptoID)
                      else
                        this.ChangePlayerResource (  i,sCryptoID,nAmount )

                        user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),i) ),"AIPlayer","onReceiveMessage" ,"[MARKET CARD] Crypto Count Effect Applied on "..sCryptoID.." with Amount "..nAmount)

                    end
                end
            end
        end
    end



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
