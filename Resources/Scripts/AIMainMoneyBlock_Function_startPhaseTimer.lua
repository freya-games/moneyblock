--------------------------------------------------------------------------------
--  Function......... : startPhaseTimer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.startPhaseTimer ( )
--------------------------------------------------------------------------------
    
    if(this.nPhase ( )>0)
    then
        local nIndex=this.nPhase()-1
        local nCurrentPhaseTimeLim=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.Phase."..nIndex..".TimeLimit")

        if(nCurrentPhaseTimeLim and nCurrentPhaseTimeLim>0)
        then
            this.nCurrentPhaseTimeLimit ( nCurrentPhaseTimeLim)
            this.bPhaseTimerLimit ( true)
        else
            this.bPhaseTimerLimit ( false)
        end
         this.bPhaseTimer ( true)
     end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
