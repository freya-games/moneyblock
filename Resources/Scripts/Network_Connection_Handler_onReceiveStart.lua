--------------------------------------------------------------------------------
--  Handler.......... : onReceiveStart
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onReceiveStart (  )
--------------------------------------------------------------------------------
       hud.setComponentVisible ( hud.getComponent ( this.getUser ( ),"Network_Connection.Room" ),false )
    user.addAIModel ( this.getUser ( ),"AIPlayer" )
    user.sendEvent ( this.getUser ( ),"AIPlayer","onReceiveServerUserID",this.nServerUserID ( ) )

      user.sendEvent ( this.getUser ( ),"AIPlayer","onReceivePlayerName",this.sPlayerName ( ) )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
