--------------------------------------------------------------------------------
--  Function......... : commitBuyOrders
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.commitBuyOrders ( )
--------------------------------------------------------------------------------
	
    
    
    local nTaxOnCancel= hashtable.get(this.htCurrentBuyOrders ( ),"TaxOnCancel" )
    if(not nTaxOnCancel)then nTaxOnCancel = 0 end
    
    for nPlayerIndex =0, table.getSize ( this.tUsers ( ) )-1
    do
    
        local nUserID=table.getAt ( this.tUsers ( ),nPlayerIndex )
        if(nUserID)
        then
            
            
            local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

            for i=0,nCount-1
            do
                local sResourceID =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
                
                local bCancel=hashtable.get ( this.htRuntimeData ( ), "Game.Resources."..sResourceID ..".CancelBuy")
                
                local nAmount= hashtable.get ( this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                local nResPrice= hashtable.get(this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Price" )
                
              
                  local nResCount=this.getResourceCount (  sResourceID)
                if (nAmount)
                then
                    if(bCancel)
                    then
                        
                       
                      --  this.ChangePlayerResource (nPlayerIndex, sResourceID,-1*nAmount )
                  
                        if(nResCount~=-1)
                        then
                      
                            this.changeGameResourceCount ( sResourceID,nAmount )
                            
        
                        end
                  
                       
                        local nTotal=(nAmount*nResPrice) * (1-nTaxOnCancel)
                        this.changePlayerCash ( nPlayerIndex,nTotal )
                     --   this.changePlayerCash ( nPlayerIndex,nTotal )
                    
                    
                    else
                      --   local nTotal=-1*nAmount*nResPrice
                       -- this.changePlayerCash ( nPlayerIndex,nTotal )
                        this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                        
                        if(nResCount~=-1)
                        then
                            this.changeGameResourceCount ( sResourceID,-1*nAmount )
                        end
                    end
                    
                    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onCommitBuyOrder",sResourceID,nAmount,this.getPlayerResourceCount (nUserID, sResourceID ),bCancel)
                    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onGetMyCashCallback",this.getPlayerCash (  nUserID))
                    hashtable.remove ( this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                end
                
                
            end
        else
            log.error ( "AIMainMoneyBlock.onBuy UserID Error" )
        end   
    end  
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
