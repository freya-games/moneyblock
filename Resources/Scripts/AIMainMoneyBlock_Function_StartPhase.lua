--------------------------------------------------------------------------------
--  Function......... : StartPhase
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.StartPhase ( )
--------------------------------------------------------------------------------

    this.startPhaseTimer ( )

    if(this.nPhase ( )==1)
    then



        --Debug Market Card
        local nIndexMarketCard =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Debug.MarketCardIndex")

        if(not nIndexMarketCard or nIndexMarketCard==-1)
        then
            nIndexMarketCard=this.PickRandomMarketCard ( )
        end

        this.nCurrentMarketCard ( nIndexMarketCard)
        this.SendMarketCardIDToAllPlayers (nIndexMarketCard )
        this.ApplyMarketCardEffect (this.nCurrentMarketCard ( ),false)


        local nRefreshActionCard= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.RefreshActionCards")
        if (nRefreshActionCard>0)
        then
            this.removeAllActionCardPlayers ( )
        end

            --Action Cards--

        --Debug Action Cards
        local nIndexActionCard =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Debug.ActionCardIndex")

        local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Debug.ActionCards.ChildCount")
        if(nCount)
        then
            for i=0, nCount-1
            do
                local nID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Debug.ActionCards.Index."..i)
                 this.GiveActionCardToAllPlayers (nID )
            end

        else


            local nPickActionCards =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.NewTurnPickActionCardsCount")

            for j=0,nPickActionCards-1
            do
                this.GiveActionCardToAllPlayers ( nil )
            end

        end

        for i =0, this.nPlayerCount ( )-1
        do
          --TransactionCount
            hashtable.set( this.htRuntimeData ( ),"Player."..i..".TransactionCount", 0)





        end

    elseif(this.nPhase ( )==2)
    then



    elseif(this.nPhase ( )==3)
    then

        local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

        for i=0,nCount-1
        do
            if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
            then
                local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
            --this.updateCryptoPrice (sID  )

              this.updateTotalBookOrderToAllPlayers (sID )

            end

        end
    elseif(this.nPhase ( )==4)
    then

        local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

        for i=0,nCount-1
        do
            if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
            then
                local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
            --this.updateCryptoPrice (sID  )

                this.updateTotalMiningResourcesToAllPlayers ( sID)

            end

        end

    elseif(this.nPhase ( )==5)
    then
        this.ApplyAllActionCardsEffect (false )
        this.PhaseUpdatePrice ( )

        this.Mine ( )
        this.ApplyAllActionCardsEffect (true)
        this.ApplyMarketCardEffect (this.nCurrentMarketCard ( ),true )
        this.commitSellOrders ( )
        this.commitBuyOrders ( )

  --  elseif(this.nPhase ( )==6)
  --  then



    end

     this.sendScoresToAllPlayers ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
