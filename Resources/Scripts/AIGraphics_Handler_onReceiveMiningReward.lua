--------------------------------------------------------------------------------
--  Handler.......... : onReceiveMiningReward
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onReceiveMiningReward ( sCryptoID,nNewTotalCryptoCount,nReward )
--------------------------------------------------------------------------------
		
	if(hashtable.contains ( this.htResourceCount ( ),sCryptoID ))
    then
        hashtable.set ( this.htResourceCount ( ),sCryptoID, nNewTotalCryptoCount )
    else
        hashtable.add ( this.htResourceCount ( ),sCryptoID,nNewTotalCryptoCount )
    end
	
    this.UpdateHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
