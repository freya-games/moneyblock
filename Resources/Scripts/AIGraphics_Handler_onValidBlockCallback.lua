--------------------------------------------------------------------------------
--  Handler.......... : onValidBlockCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onValidBlockCallback ( sCryptoID,nCurrentBlock)
--------------------------------------------------------------------------------
	

if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sCryptoID ))
then
    hashtable.set ( this.htCryptoCurrentBlock  ( ),sCryptoID, nCurrentBlock )
else
    hashtable.add ( this.htCryptoCurrentBlock  ( ),sCryptoID,nCurrentBlock )
end

this.UpdateMarketHUD ( )
this.UpdateMiningHUD (  )
this.UpdateBlockChainModel (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
