--------------------------------------------------------------------------------
--  Handler.......... : onSellCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onSellCallback ( sResourceID,nAmount,nTotalCount )
--------------------------------------------------------------------------------


    if(this.GetResourceType ( sResourceID)=="CRYPTO")
    then
        if(hashtable.contains ( this.htPendingOrders ( ), sResourceID..".Sell" ))
        then
            hashtable.set ( this.htPendingOrders ( ) ,sResourceID..".Sell" ,nAmount)
        else
            hashtable.add ( this.htPendingOrders ( ), sResourceID..".Sell" ,nAmount)
        end
        
          if(hashtable.contains ( this.htPendingOrders ( ), sResourceID..".Buy" ))
        then
            hashtable.set ( this.htPendingOrders ( ) ,sResourceID..".Buy" ,0)
     
        end

    this.UpdateHUD (  )

	end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
