--------------------------------------------------------------------------------
--  Handler.......... : onGetCryptoCurrentBlockCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetCryptoCurrentBlockCallback (   sResID,nCurrentBlock,nBlockCount )
--------------------------------------------------------------------------------
	
	if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sResID ))
    then
        hashtable.set ( this.htCryptoCurrentBlock  ( ),sResID, nCurrentBlock )
    else
        hashtable.add ( this.htCryptoCurrentBlock  ( ),sResID,nCurrentBlock )
    end

    this.UpdateMarketHUD (  )
    this.UpdateMiningHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
