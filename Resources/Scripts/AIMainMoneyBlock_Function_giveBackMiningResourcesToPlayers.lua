--------------------------------------------------------------------------------
--  Function......... : giveBackMiningResourcesToPlayers
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.giveBackMiningResourcesToPlayers ( )
--------------------------------------------------------------------------------

 for nPlayerIndex=0, this.nPlayerCount ( )-1
 do

    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

    for iC=0, nCount-1
    do
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID")
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then

            for iR=0, nCount-1
            do
                 local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                if(this.getResourceType ( sResID)=="MINER")
                then
                     local nAmount =hashtable.get ( this.htMining ( ), "Player."..nPlayerIndex.."."..sCryptoID.."."..sResID )
                     if (nAmount)
                     then
                         this.ChangePlayerResource ( nPlayerIndex,sResID,nAmount )
                        --user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex) ),"AIPlayer","onReceiveMessage" ,"[SERVER] Get Back "..nAmount.." "..sResID.." on "..sCryptoID.." Mining")

                     end
                end
            end

        end
    end


end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
