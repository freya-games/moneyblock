--------------------------------------------------------------------------------
--  Function......... : ClearPriceLock
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ClearPriceLock ( )
--------------------------------------------------------------------------------

    local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

for i=0,nCount-1
do
  if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
    then
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        hashtable.set (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price.Lock" ,false)
    end
end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
