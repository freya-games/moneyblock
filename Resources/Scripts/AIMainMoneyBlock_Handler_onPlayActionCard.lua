--------------------------------------------------------------------------------
--  Handler.......... : onPlayActionCard
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onPlayActionCard ( nUserID,nCardID,sCryptoID ,sCallBack)
--------------------------------------------------------------------------------

--log.warning (  nUserID, " ", nCardID, " ", sCryptoID, " " ,sCallBack )

local bCanPlay=false
if(this.isActionCardValid ( nUserID,nCardID ))
then

    local nPlayerIndex=this.getPlayerIndex ( nUserID )
    if(hashtable.contains ( this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.ID" ))
    then
        hashtable.set(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.ID" ,nCardID)
    else
        hashtable.add(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.ID" ,nCardID)

    end


    if(this.bRandomWalk ( ))
    then
        sCryptoID=this.getRandomCrypto (  )
    end

    if(sCryptoID)
    then


        if(hashtable.contains ( this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.CryptoTarget" ))
        then
            hashtable.set(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.CryptoTarget" ,sCryptoID)
        else
            hashtable.add(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.CryptoTarget" ,sCryptoID)

        end

    end




    bCanPlay=true
    this.removeActionCard (nUserID,nCardID)
else
    bCanPlay=false
end


     user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", sCallBack,nCardID,sCryptoID ,bCanPlay)

    this.sendCardsPlayedToAllPlayers ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
