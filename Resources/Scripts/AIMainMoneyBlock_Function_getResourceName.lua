--------------------------------------------------------------------------------
--  Function......... : getResourceName
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getResourceName ( sResID)
--------------------------------------------------------------------------------
	
    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
	for i=0, nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        if(sID ==sResID)
        then
            return hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Name")
        end
    
    end
    return nil
	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
