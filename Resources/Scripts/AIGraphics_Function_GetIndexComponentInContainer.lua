--------------------------------------------------------------------------------
--  Function......... : GetIndexComponentInContainer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetIndexComponentInContainer ( sTagComponent )
--------------------------------------------------------------------------------
	
local nIndex = -1
local hC=hud.getComponent ( this.getUser ( ),sTagComponent )
if(hC)then
    local hContainer = hud.getComponentContainer ( hC )
    
    for i=0 , hud.getContainerChildCount ( hContainer ) -1
    do
        local hTemp = hud.getContainerChildAt ( hContainer, i )
        local sTempTag = hud.getComponentTag ( hTemp )
        if(sTempTag == sTagComponent)then 
            nIndex = i
            break
        end
    end    
end

return nIndex
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
