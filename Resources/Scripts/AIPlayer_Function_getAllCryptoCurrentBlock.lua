--------------------------------------------------------------------------------
--  Function......... : getAllCryptoCurrentBlock
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.getAllCryptoCurrentBlock ( )
--------------------------------------------------------------------------------
	
local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
for i=0, nCount-1
do
    local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")
    if(sType=="CRYPTO")
    then
        this.getCurrentBlock( sID)
    end
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
