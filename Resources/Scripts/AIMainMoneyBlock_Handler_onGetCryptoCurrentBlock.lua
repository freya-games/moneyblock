--------------------------------------------------------------------------------
--  Handler.......... : onGetCryptoCurrentBlock
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onGetCryptoCurrentBlock ( nUserID,sResID,sCallback  )
--------------------------------------------------------------------------------
	
    
      local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sResID..".CurrentBlock"  )
      local nBlockCount=hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sResID..".BlockCount"  )
    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", sCallback,sResID,nCB, nBlockCount)
	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
