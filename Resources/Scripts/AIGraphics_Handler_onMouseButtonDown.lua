--------------------------------------------------------------------------------
--  Handler.......... : onMouseButtonDown
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onMouseButtonDown ( nButton, nPointX, nPointY, nRayPntX, nRayPntY, nRayPntZ, nRayDirX, nRayDirY, nRayDirZ )
--------------------------------------------------------------------------------
	
-- local hC = hud.getUnderCursorComponent ( this.getUser ( ) )
-- if(hC)then
-- local sTag = hud.getComponentTag ( hC )
-- log.warning ( sTag )
-- end

-- local sKey = hashtable.getKeyAt ( this.htCryptoCurrentBlock ( ), 1 )
-- local nValue = hashtable.get ( this.htCryptoCurrentBlock ( ), sKey)
-- hashtable.set ( this.htCryptoCurrentBlock ( ), sKey, nValue +1 )
-- this.UpdateBlockChainModel (  )

    
--SELECT MINING MODEL
if(this.nPhase ( ) == 3 and this.bPhaseValided ( )== false)then
    local hScene = user.getScene ( this.getUser ( ) )
    
    local hC = hud.getUnderCursorComponent ( this.getUser ( ) )
     local sTag=""
    if(hC)
    then
        sTag = hud.getComponentTag ( hC )
    end
     
--     log.message ( sTag )
    
    if(hC and (not string.contains ( sTag,"ButtonAdd" ) and not string.contains ( sTag,"ButtonRemove" )))
    then
        this.sCurMiningCryptoID ( "")
        this.EnableMiningPanelHUD ( false )
        this.UpdateOrderBooksHUD (  )
       
       
    end
    
    if(not hC or sTag=="PlayerMainHUD.ContainerGlobal" or sTag=="MiningHUD.ContainerGlobal")
    then
        local hHitObj, nHitDist, nHitSensorID = scene.getFirstHitSensor ( hScene, nRayPntX, nRayPntY, nRayPntZ, nRayDirX, nRayDirY, nRayDirZ, 100 )
        if(hHitObj)then
            local sCryptoID = this.GetCrytoIDOfMiningModel ( hHitObj )

            --CRYPTO SELECTED
            if(sCryptoID ~= "")then
                local hUser = this.getUser ( )
                hud.stopSound ( hUser,1)
                hud.playSound ( hUser,1,127,false )
                this.sCurMiningCryptoID ( sCryptoID )
                this.EnableMiningPanelHUD ( true, hHitObj )
                this.UpdateMiningHUD(true)
            else
                 local hUser = this.getUser ( )
                hud.stopSound ( hUser,2)
                hud.playSound ( hUser,2,127,false )
            end
       else
            local hUser = this.getUser ( )
            hud.stopSound ( hUser,2)
            hud.playSound ( hUser,2,127,false )

   
            
        end
  

    end
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
