--------------------------------------------------------------------------------
--  Function......... : UpdatePendingHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdatePendingHUD ( )
--------------------------------------------------------------------------------
	
if(this.bUseDebugHUD ( ))then return end
    
    
local hUser = this.getUser ( )

--UPDATE PENGING VALUES
local sActionTag = "PlayerMainHUD.SetPendingOrders"
if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
hud.callAction ( hUser, sActionTag, this.GetTotalBuyAmount ( ).." "..this.GetCurrency ( ), this.GetTotalSellAmount ( ).." "..this.GetCurrency ( ) )


--UPDATE CRYPTO
for i=0 , hashtable.getSize ( this.htPrefixCryptoHUD() ) -1
do
	local sCryptoID = hashtable.getKeyAt ( this.htPrefixCryptoHUD(), i )
	local nValue = hashtable.get ( this.htHUDTradeValue(), sCryptoID )
    if(not nValue)then nValue = 0 end
    local sPrefix = hashtable.get ( this.htPrefixCryptoHUD ( ), sCryptoID )
    
    --GET COLOR AND TEXT
    local sPrefixValue = "_"
    local r = 65
    local g = 65
    local b = 65
    if(nValue>0)then
        sPrefixValue = "+"
        r = 0
        g = 127
        b = 0
    elseif(nValue<0)then
        sPrefixValue = "-"
        r = 127
        g = 0
        b = 0
    end
    if(math.abs ( nValue )<10)then sPrefixValue = sPrefixValue.."0"end
    
    local hC = hud.getComponent ( this.getUser ( ), sPrefix..".LabelTradeValue" )
    if(hC)then
        hud.setLabelText ( hC, sPrefixValue..math.abs ( nValue) )
        hud.setComponentForegroundColor ( hC, r, g, b, 255 )
    end
    
    
end

	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
