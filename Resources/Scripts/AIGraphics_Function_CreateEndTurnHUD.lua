--------------------------------------------------------------------------------
--  Function......... : CreateEndTurnHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateEndTurnHUD ( )
--------------------------------------------------------------------------------

    local nCryptoCount = hashtable.getSize ( this.htPrefixCryptoHUD ( ) )
    local nPlayerCount = table.getSize ( this.tUsers ( ) )
    local hUser = this.getUser ( )

    --CREATE END TURN HUD
    local sPrefixHUD = "EndTurnHUD"
    hud.newTemplateInstance ( hUser, sPrefixHUD, sPrefixHUD )
    this.sPrefixEndTurnHUD ( sPrefixHUD )

    local hCToken = hud.getComponent ( hUser, sPrefixHUD..".ContainerTokens" )
    local hCPlayer = hud.getComponent ( hUser, sPrefixHUD..".ContainerPlayers" )

    local nMarginTokenHUD = 2
    local nMarginPlayerHUD = 2
    local nMaxTokenDisplay = 6
    local nMaxPlayerDisplay = this.GetMaxPlayerCount ( )

    --CREATE TOKEN HUD
    if(hCToken)then
        for nIndexCrypto = 0, nCryptoCount-1
        do

            local sResID = hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ), nIndexCrypto )
            local r, g, b = this.GetResourceColor ( sResID )
            local sLogo = this.GetResourceLogo ( sResID )

            local sPrefixToken = "Token_"..nIndexCrypto
            hashtable.add ( this.htPrefixEndTurnTokenHUD ( ), sResID, sPrefixToken )
            hud.newTemplateInstance ( hUser, "LeaderboardToken", sPrefixToken)

            --SET COMPONENT TRS
            local hContainer = hud.getComponent ( hUser, sPrefixToken..".ContainerGlobal" )
            if(hContainer)then
                hud.setComponentContainer ( hContainer, hCToken )
                hud.setComponentSize ( hContainer, 100/nMaxTokenDisplay - nMarginTokenHUD, 100 )
                hud.setComponentPosition ( hContainer, (nIndexCrypto+0.5)/nMaxTokenDisplay*100, 50 )
            end

            --SET SPRITE
            local hSpriteCrypto = hud.getComponent ( hUser, sPrefixToken..".SpriteCrypto" )
            if(hSpriteCrypto)then
               hud.setComponentBackgroundImage ( hSpriteCrypto, sLogo )
            end

            --SET BAR COLOR
            local hProgress = hud.getComponent ( hUser, sPrefixToken..".ProgressBar" )
            if(hProgress)then
               hud.setComponentBackgroundColor ( hProgress, r, g, b, 255 )
            end

        end
    end

    --CREATE LEADERBOARD
    if(hCPlayer)then
       for nIndexPlayer=0 , nPlayerCount-1
       do
            local sPrefixLeaderboard = "Leaderboard_"..nIndexPlayer
            hud.newTemplateInstance ( hUser, "LeaderboardPlayer", sPrefixLeaderboard )
            hashtable.add ( this.htPrefixEndTurnPlayerHUD ( ), ""..nIndexPlayer, sPrefixLeaderboard )

             --SET COMPONENT TRS
            local hContainer = hud.getComponent ( hUser, sPrefixLeaderboard..".ContainerGlobal" )
            if(hContainer)then
                hud.setComponentContainer ( hContainer, hCPlayer )
                hud.setComponentSize ( hContainer, 100,100/nMaxPlayerDisplay - nMarginPlayerHUD )
                hud.setComponentPosition ( hContainer, 0, 100- (nIndexPlayer+0.5)/nMaxPlayerDisplay*100)
            end

       end
    end

    this.UpdateEndTurnHUD ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
