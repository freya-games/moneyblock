--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalMiningResourceCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetTotalMiningResourceCallback ( sCryptoID,sMiningResID,nAmount )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htTotalMining ( ),sCryptoID.."."..sMiningResID ))
    then
        hashtable.set ( this.htTotalMining ( ),sCryptoID.."."..sMiningResID, nAmount )
    else
        hashtable.add ( this.htTotalMining( ),sCryptoID.."."..sMiningResID, nAmount )
    end

user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetTotalMiningResourceCallback", sCryptoID,sMiningResID,nAmount )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
