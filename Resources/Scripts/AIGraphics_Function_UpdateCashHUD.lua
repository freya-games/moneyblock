--------------------------------------------------------------------------------
--  Function......... : UpdateCashHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateCashHUD ( )
--------------------------------------------------------------------------------
	
local vValue = this.nCash ( )
if(this.nPhase ( )==2 and (not this.bUseDebugHUD ( )) and this.bPhaseValided () == false)then vValue = vValue - this.GetTotalBuyAmount (  )end

local sTag = "PlayerMainHUD.LabelUserCash"
if(this.bUseDebugHUD ( ))then sTag = "PlayerHUD.Cash" end
    
    
--SET HUD
local hCash = hud.getComponent ( this.getUser ( ), sTag )
if(hCash)then
    hud.setLabelText ( hCash,""..vValue..this.GetCurrency ( ) )
end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
