--------------------------------------------------------------------------------
--  Handler.......... : onCreateServer
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onCreateServer (  )
--------------------------------------------------------------------------------

    --create server on default port
    network.createServer ( network.kDefaultServerPort )

    user.addAIModel ( this.getUser ( ),"AIMainMoneyBlock" )
    user.sendEvent ( this.getUser(),"AIMainMoneyBlock","onSetLocal",false )
    this.sServerIP ( "")

    this.WaitForplayers ( )
    --this.StartPlaying ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
