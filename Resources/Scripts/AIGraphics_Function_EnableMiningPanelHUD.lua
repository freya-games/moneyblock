--------------------------------------------------------------------------------
--  Function......... : EnableMiningPanelHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableMiningPanelHUD ( bEnable, hMiningBox )
--------------------------------------------------------------------------------

if(this.bUseDebugHUD ( ))then return end

local sAction = ".Hide"
if(bEnable)then
    sAction = ".Show"
end

-- SHOW/HIDE HUD
if(hud.isActionRunning ( this.getUser ( ), this.sPrefixMiningHUD ( )..sAction ))then hud.stopAction ( this.getUser ( ), this.sPrefixMiningHUD ( )..sAction )end
hud.callAction ( this.getUser ( ), this.sPrefixMiningHUD ( )..sAction )
	
--SET PANEL POSITION
if(bEnable and hMiningBox)then

    local hC = hud.getComponent ( this.getUser ( ), this.sPrefixMiningHUD ( )..".ContainerBlockLvl")
    local x, y = hud.getComponentPosition ( hC )
    local hLastObj = hashtable.getAt ( this.htMiningModel ( ), hashtable.getSize ( this.htMiningModel ( ) )-1  ) 
    if( object.isEqualTo ( hMiningBox , hLastObj))then
        
        hud.setComponentOrigin ( hC, hud.kOriginLeft )
        hud.setComponentPosition ( hC, 0, y  )
    else
        hud.setComponentOrigin ( hC, hud.kOriginRight )
        hud.setComponentPosition ( hC, 100, y  )
    end

    local nOffsetX = 0
    local nOffsetY = 5
    local hUser = this.getUser ( )
    local hCam = user.getActiveCamera ( this.getUser ( )  )
    local x, y, z = object.getTranslation ( hMiningBox, object.kGlobalSpace )
    local sx, sy, sz = object.getScale ( hMiningBox )
    y = y + sy/2
    
    x, y, z = camera.projectPoint ( hCam, x, y, z )
    x = (x + 1) * 50
    y = (y + 1) * 50
    
    hud.callAction ( this.getUser ( ), this.sPrefixMiningHUD ( )..".SetPosition", x + nOffsetX, y + nOffsetY )
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
