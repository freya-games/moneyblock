--------------------------------------------------------------------------------
--  Function......... : GetCardIndex
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetCardIndex ( sCardTag )
--------------------------------------------------------------------------------
	
	 --GET CARD INDEX and CardID
    for j= 0, table.getSize ( this.tPrefixActionCardHUD ( ) )-1 
    do
    	if(string.contains ( sCardTag, table.getAt (this.tPrefixActionCardHUD ( ), j )))then
            local nCardIndex = j
            return nCardIndex, table.getAt ( this.tActionCards ( ),j )
        end
    end
    
    return -1, -1
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
