--------------------------------------------------------------------------------
--  Handler.......... : onGetMyResourceCountCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetMyResourceCountCallback (sResID,nCount  )
--------------------------------------------------------------------------------
	
	if(hashtable.contains ( this.htResourceCount ( ),sResID ))
    then
        hashtable.set ( this.htResourceCount ( ),sResID, nCount )
    else
        hashtable.add ( this.htResourceCount ( ),sResID,nCount )
    end
	
    this.UpdateHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
