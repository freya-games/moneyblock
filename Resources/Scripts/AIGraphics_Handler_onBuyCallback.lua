--------------------------------------------------------------------------------
--  Handler.......... : onBuyCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onBuyCallback ( sResID,nAmount,nTotalCount )
--------------------------------------------------------------------------------
	

        if(this.GetResourceType ( sResID )=="CRYPTO")
        then
            if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Buy" ))
            then
                hashtable.set ( this.htPendingOrders ( ),sResID..".Buy" ,nAmount)
            else
                hashtable.add ( this.htPendingOrders ( ),sResID..".Buy" ,nAmount)
            end
            
            
             if(hashtable.contains ( this.htPendingOrders ( ), sResID..".Sell" ))
            then
                hashtable.set ( this.htPendingOrders ( ) ,sResID..".Sell" ,0)
            end
        else
            this.UpdateMiningResourceSliders ( )
        
        end
        
        this.UpdateHUD (  )

    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
