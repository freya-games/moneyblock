--------------------------------------------------------------------------------
--  Handler.......... : onValidPhaseCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onValidPhaseCallback (  )
--------------------------------------------------------------------------------
	
this.bPhaseValided ( true )
    
if(not this.bUseDebugHUD ( ))then

    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerEndTurn" )
    hud.setComponentVisible ( hComponent,false )
else	
	local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.ValidPhase" )
    hud.setComponentVisible ( hComponent,false )
end


if(not this.bUseDebugHUD ( ))then

 if(this.nPhase ( )==1)
    then
        return
    elseif(this.nPhase ( )==2)
    then
        this.EnableTradeHUD ( false, this.sCurTradeCryptoID ( ) )
        return
    elseif(this.nPhase ( )==3)
    then
        this.EnableMiningHUD ( false )
        return
     elseif(this.nPhase ( )==4)
    then
        
        return   
    elseif(this.nPhase ( )==5)
    then
        this.EnableActionCards ( false )
        return  
    elseif(this.nPhase ( )==6)
    then
        return 
    end
    
else



end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
