--------------------------------------------------------------------------------
--  Function......... : updateAnimations
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIAvatar.updateAnimations ( )
--------------------------------------------------------------------------------
	
    local hObject = this.getObject  ( )
	local hDynObj = this.hBody ( )
    
    if ( hDynObj and object.hasController ( hObject, object.kControllerTypeAnimation ) )
    then
        local dt         = application.getLastFrameTime ( )
        local dt2        = application.getLastFrameTime ( ) * 4
        local vx, vy, vz = dynamics.getLinearVelocity ( hDynObj, object.kGlobalSpace )
        local vLength    = math.vectorLength ( vx, 0, vz )
        local nMaxSpeed  = 3
        local nSSmoother = this.nAnimSpeedSmoother ( )
        local nSSFactor  = 10 if ( nSSmoother > vLength ) then nSSFactor = 3 end
        local nSpeed     = math.interpolate ( nSSmoother, vLength, nSSFactor * dt2 )
        local f          = math.clamp ( nSpeed / nMaxSpeed, 0, 1 )
        
        if ( nSpeed < 0.01 )
        then
            nSpeed = 0
        end
        
        this.nAnimSpeedSmoother ( nSpeed )
        
        local targetWait = math.max ( 0, 1 - 2 * f )
        
        local walkAndRunLoopSpeed   = nSpeed * animation.getClipKeyFrameRangeMax ( hObject, 1 )
        local walkLevel             = 0
        local runLevel              = 0
        
        if ( f > 0.3 )
        then
            runLevel                = ( f - 0.3 ) / 0.7
            walkLevel               = 1 - runLevel
            walkAndRunLoopSpeed     = walkAndRunLoopSpeed * ( 1 - 0.5 * runLevel )
        else
            walkLevel               = f / 0.3
        end
        
        local targetWalk = math.trunc ( walkLevel, 2 )
        local targetRun = math.trunc ( runLevel, 2 )
        
        if ( this.bSit ( ) and animation.getPlaybackCursor ( hObject, 3 ) == animation.getClipKeyFrameRangeMax ( hObject, 3 ) )
        then
            this.bSit ( false )
        end
        
        if ( this.bPunch ( ) and animation.getPlaybackCursor ( hObject, 4 ) == animation.getClipKeyFrameRangeMax ( hObject, 4 ) )
        then
            this.bPunch ( false )
        end
        
        if ( this.bPunch ( ) or this.bSit ( ) )
        then
            targetWait = 0
            targetWalk = 0
            targetRun = 0
        end
        
        local targetSit = this.bSit ( ) and 1 or 0
        local targetPunch = this.bPunch ( ) and 1 or 0
        
        local currentWait = animation.getPlaybackLevel ( hObject, 0 )
        local currentWalk = animation.getPlaybackLevel ( hObject, 1 )
        local currentRun = animation.getPlaybackLevel ( hObject, 2 )
        local currentSit = animation.getPlaybackLevel ( hObject, 3 )
        local currentPunch = animation.getPlaybackLevel ( hObject, 4 )
        
        local newWait = ( currentWait == targetWait ) and targetWait or ( ( currentWait < targetWait ) and math.min ( currentWait + dt2, targetWait ) or math.max ( currentWait - dt2, targetWait ) )
        local newWalk = ( currentWalk == targetWalk ) and targetWalk or ( ( currentWait < targetWait ) and math.min ( currentWalk + dt2, targetWalk ) or math.max ( currentWalk - dt2, targetWalk ) )
        local newRun = ( currentRun == targetRun ) and targetRun or ( ( currentRun < targetRun ) and math.min ( currentRun + dt2, targetRun ) or math.max ( currentRun - dt2, targetRun ) )
        local newSit = ( currentSit == targetSit ) and targetSit or ( ( currentSit < targetSit ) and math.min ( currentSit + dt2, targetSit ) or math.max ( currentSit - dt2, targetSit ) )
        local newPunch = ( currentPunch == targetPunch ) and targetPunch or ( ( currentPunch < targetPunch ) and math.min ( currentPunch + dt2, targetPunch ) or math.max ( currentPunch - dt2, targetPunch ) )
        
        animation.setPlaybackLevel ( hObject, 0, newWait )
        animation.setPlaybackLevel ( hObject, 1, newWalk )
        animation.setPlaybackSpeed ( hObject, 1, math.max ( 20, walkAndRunLoopSpeed ) )
        animation.setPlaybackLevel ( hObject, 2, newRun )
        animation.setPlaybackSpeed ( hObject, 2, math.max ( 20, walkAndRunLoopSpeed ) )
        animation.setPlaybackLevel ( hObject, 3, newSit )
        animation.setPlaybackLevel ( hObject, 4, newPunch )
        animation.matchPlaybackCursor ( hObject, 2, 1 )
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
