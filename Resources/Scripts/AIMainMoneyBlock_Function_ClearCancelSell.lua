--------------------------------------------------------------------------------
--  Function......... : ClearCancelSell
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ClearCancelSell ( )
--------------------------------------------------------------------------------


local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
for i=0, nCount-1
do

    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")
    if(sType=="CRYPTO")
    then
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")

        if(hashtable.contains ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".HodlCancelSell"))
        then
            hashtable.set (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".HodlCancelSell",false )
        end
    end

end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
