--------------------------------------------------------------------------------
--  Handler.......... : onReceiveScores
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onReceiveScores ( sAllScore, sDelimiter, sAllTokens, sAllMoney  )
--------------------------------------------------------------------------------

    local hUser = this.getUser ( )

     --PARSE SCORES
    local tTemp = table.newInstance ( )
    local tTempMoney = table.newInstance ( )
    local tTempTokens = table.newInstance ( )
    local tRank= table.newInstance ( )
    string.explode (  sAllScore, tTemp, sDelimiter )
    string.explode ( sAllTokens, tTempTokens, sDelimiter )
    string.explode ( sAllMoney, tTempMoney, sDelimiter )

    local bErasedOldValues = this.bHasUserSeenLastScores ( )
    this.bHasUserSeenLastScores ( false)

    --INIT TABLES
    if(table.getSize ( this.tPlayersCurScores ( ))==0 )then
        for i=0 , table.getSize ( tTemp ) -1
        do
            table.add ( this.tPlayersPrevScores ( ), 0 )
            table.add ( this.tPlayersCurScores ( ), string.toNumber ( table.getAt ( tTemp ,i) ))
            table.add ( this.tPlayersTotalMoney ( ), string.toNumber ( table.getAt ( tTempMoney ,i) ))
            table.add ( this.tPlayersPrevTotalMoney ( ), 0 )
            table.add ( this.tPlayersTotalTokens ( ),  string.toNumber ( table.getAt ( tTempTokens ,i) ))
            table.add ( this.tPlayersPrevTotalTokens ( ), 0 )
        end
        bErasedOldValues = true
    end


    --SET SCORES
    for i= 0, table.getSize ( tTemp ) -1
    do
        if(bErasedOldValues)then
            table.setAt ( this.tPlayersPrevTotalMoney ( ), i, table.getAt ( this.tPlayersTotalMoney ( ) ,i) )
            table.setAt ( this.tPlayersPrevTotalTokens ( ), i, table.getAt ( this.tPlayersTotalTokens ( ) ,i) )
            table.setAt ( this.tPlayersPrevScores ( ), i, table.getAt ( this.tPlayersCurScores ( ) ,i) )
        end

        table.setAt ( this.tPlayersTotalMoney ( ), i, string.toNumber ( table.getAt ( tTempMoney ,i) ))
        table.setAt ( this.tPlayersTotalTokens ( ), i, string.toNumber ( table.getAt ( tTempTokens ,i) ))
        table.setAt ( this.tPlayersCurScores ( ), i, string.toNumber ( table.getAt ( tTemp ,i) ))
        local nScore =  table.getAt ( this.tPlayersCurScores ( ), i )

        --UPDATE HUD SCORE
        if(table.getSize ( this.tPrefixPlayerScoreHUD ( ))> i )then
            local sPrefix = table.getAt ( this.tPrefixPlayerScoreHUD ( ), i )
            local sAction = sPrefix..".SetPlayerScore"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction, ""..nScore   )
        end


        --ORDER RANK
        local bInsert = false
        for j=0, table.getSize ( tRank )-1
        do
            local nUser = table.getAt ( tRank, j  )
            local nOldScore = table.getAt ( this.tPlayersCurScores ( ), nUser )

            if(nScore >nOldScore)then
                table.insertAt ( tRank, j, i )
                bInsert = true
                break
            end
        end
        if(not bInsert)then
            table.add ( tRank, i )
        end

    end

    --log.warning ( this.tPlayersCurScores ( ) )


    --UPDATE HUD RANK
    local nExecoCount = 0
    for i=0 , table.getSize ( tRank ) -1
    do
        local nUser = table.getAt ( tRank, i )


        --IS EXECO ?
        if(i>0)then
            local nPrevUser = table.getAt ( tRank, i-1 )
            local nOldScore = table.getAt ( this.tPlayersCurScores ( ), nPrevUser )
            local nScore = table.getAt ( this.tPlayersCurScores ( ), nUser )
            if(nOldScore == nScore)then nExecoCount = nExecoCount +1 end
        end


        if(table.getSize ( this.tPrefixPlayerScoreHUD ( ))> nUser )then

            --GET HUD
            local sPrefix = table.getAt ( this.tPrefixPlayerScoreHUD ( ), nUser )

            --GET RANK
            local nRank = i+1 - nExecoCount

            --GET SUFFIX
            local sSuffixRank = "th"
            if(nRank==1)then sSuffixRank = "st"
            elseif(nRank==2)then sSuffixRank = "nd"
            elseif(nRank==3)then sSuffixRank = "rd"end

            --GET NEW POS
            local nPosX =  (i+0.5)*100/ this.GetMaxPlayerCount (  )
            local nPosY =  50

            local sAction = sPrefix..".SetPlayerRank"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction, ""..nRank, sSuffixRank, nPosX, nPosY  )
        end
    end



    --UPDATE END TURN HUD IF OPEN
    if(this.IsHudVisible ( this.sPrefixEndTurnHUD ( )..".ContainerGlobal" ))then
       this.UpdateEndTurnHUD ( )
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
