--------------------------------------------------------------------------------
--  Function......... : CleanBuffers
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.CleanBuffers ( )
--------------------------------------------------------------------------------

    hashtable.empty ( this.htCurrentBuyOrders ( ) )
    hashtable.empty ( this.htCurrentSellOrders ( ) )
    hashtable.empty ( this.htMining ( ) )
    hashtable.empty ( this.htPlayerActionCards ( ) )
    table.empty ( this.tCurBlockMined ( ) )


    --TODO use global Table
    this.bAnonymous ( false)
    this.bRandomWalk ( false)
    this.bMarketCap ( false)


this.ClearCancelSell ( )
this.ClearPriceLock ( )


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
