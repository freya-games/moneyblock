--------------------------------------------------------------------------------
--  Handler.......... : onSetTargetTransform
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Camera.onSetTargetTransform ( nTime, endX, endY, endZ, endRX, endRY, endRZ )
--------------------------------------------------------------------------------
	
    this.nLerpCurTime ( 0 )
    this.nLerpTotalTime ( nTime )
	this.SetTarget ( endX, endY, endZ, endRX, endRY, endRZ )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
