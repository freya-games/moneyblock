--------------------------------------------------------------------------------
--  Function......... : updateCryptoPrice
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.updateCryptoPrice (sCryptoID,bApplyMarketCard )
--------------------------------------------------------------------------------
	

local nLock =  hashtable.get (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price.Lock" )
if(nLock)
then  
    return
else


    local nPrice=hashtable.get (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price" )
    local nInitPrice=nPrice
    local nBuy=this.getTotalBuyOrder (sCryptoID )
    local nSell=this.getTotalSellOrder (sCryptoID )
    local nIterationCount=   hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Market.PriceIterationCount")
    local nTotalActionRandom=0
    for i=0,nIterationCount-1
    do
        --local nR=math.random (-0.2,0.2  )
        local nM=0
        if(bApplyMarketCard)
        then
        
            nM=this.MarketNormalDistribution (  this.nCurrentMarketCard ( ))
            if(not nM)
            then
        
                nM=0
            end
            	
        end
       
        for nPlayerIndex=0, this.nPlayerCount ( )-1
        do  
            
            local  nCardID=this.getActionOnCryptoByPlayer ( nPlayerIndex,sCryptoID )
            if( nCardID)
            then
                local nAR=this.ActionNormalDistribution ( nCardID )
                if(nAR)
                then
                    -- log.message ("Action Random "..nPlayerIndex.." "..sCryptoID.." "..nAR  )
                    nTotalActionRandom=nTotalActionRandom+nAR
                end
            end
            
        end
       
       -- log.message ("Total Action Random "..nTotalActionRandom  )
       
        local nInput = (math.random ( -1*nSell,nBuy )+ nBuy-nSell)/10
        local nDelta=this.Volume (  )+this.Alea (  )+nM+nInput+nTotalActionRandom
        nPrice=nPrice+ nDelta
        
        if(nPrice<=0)
        then
            nPrice=nPrice-nDelta
        end
    end
    
    this.changeGameResourcePrice ( sCryptoID,math.roundToNearestInteger (  nPrice-nInitPrice) )  
    
  
end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
