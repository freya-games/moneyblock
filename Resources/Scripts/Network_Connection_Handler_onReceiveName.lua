--------------------------------------------------------------------------------
--  Handler.......... : onReceiveName
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onReceiveName ( nUserID, sName )
--------------------------------------------------------------------------------


    --handle message only for distant user
    if ( nUserID ~= user.getID ( this.getUser ( ) ) )
    then
        if ( hashtable.contains ( this.aPlayerName ( ), ""..nUserID ) )
        then
            hashtable.set  ( this.aPlayerName ( ), ""..nUserID,  sName ) --only modify an existing entry
        else
            hashtable.add  ( this.aPlayerName ( ), ""..nUserID,  sName )
        end

    end

    this.updatePlayersList ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
