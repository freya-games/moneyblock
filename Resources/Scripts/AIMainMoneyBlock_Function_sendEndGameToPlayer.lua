--------------------------------------------------------------------------------
--  Function......... : sendEndGameToPlayer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendEndGameToPlayer (nPlayerIndex,sTitle,sLeaderBoard )
--------------------------------------------------------------------------------
	
    local nUserID=table.getAt ( this.tUsers ( ),nPlayerIndex)
    local hUser=application.getUser ( nUserID )


    user.sendEvent ( hUser,"AIPlayer","onEndGame" ,sTitle,sLeaderBoard )
   
 
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
