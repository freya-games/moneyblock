--------------------------------------------------------------------------------
--  Handler.......... : onValidBlock
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onValidBlock ( sCryptoID,nCurrentBlock,bValid )
--------------------------------------------------------------------------------
	local sMessage="Block "..nCurrentBlock.." "..sCryptoID
    if(bValid)
    then
    
        if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sCryptoID ))
        then
            hashtable.set ( this.htCryptoCurrentBlock  ( ),sCryptoID, nCurrentBlock )
        else
            hashtable.add ( this.htCryptoCurrentBlock  ( ),sCryptoID,nCurrentBlock )
        end
        
        user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onValidBlockCallback", sCryptoID,nCurrentBlock)
            
        sMessage=sMessage.." is mined"  
    else
    
        sMessage=sMessage.." is not mined"  
    end
	
    this.openDialogBox ( sMessage )
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
