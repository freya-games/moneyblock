--------------------------------------------------------------------------------
--  Function......... : ChangePlayerResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ChangePlayerResource (nPlayerIndex,sResID,nValueToAdd )
--------------------------------------------------------------------------------


                
        
    if(hashtable.contains (this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID..".Count" ))
    then
        local nV=hashtable.get (this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID..".Count" )
        local nNewValue=nV+nValueToAdd
        if(nNewValue<0)
        then
            nNewValue=0
        end
        
        hashtable.set ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID..".Count",nNewValue )
        
        local hUser=application.getUser (  table.getAt ( this.tUsers ( ),nPlayerIndex ))
        local nCount=this.getPlayerResourceCount (user.getID ( hUser ) ,sResID )
        user.sendEvent ( hUser,"AIPlayer", "onGetMyResourceCountCallback",sResID,nCount)

        
    else
    
    
        --hashtable.add ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID ,nValueToAdd )
        log.error ( "AIMainMoneyBlock.ChangePlayerResource  ".."Player."..nPlayerIndex.."."..sResID..".Count does not exist")
    end
    return
           
      
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
