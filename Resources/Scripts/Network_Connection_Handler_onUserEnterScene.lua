--------------------------------------------------------------------------------
--  Handler.......... : onUserEnterScene
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onUserEnterScene ( nUserID )
--------------------------------------------------------------------------------

    --build a temporary name for the new player
    if ( not hashtable.contains ( this.aPlayerName ( ), ""..nUserID ) )
    then
        this.sPlayerName ( "Player "..nUserID)
        hashtable.add   ( this.aPlayerName ( ), ""..nUserID,  "Player "..nUserID )
    end

    --Send our local user name to the new distant user
    user.sendEvent ( application.getUser ( nUserID ), "Network_Connection", "onReceiveName", user.getID ( this.getUser ( )),  this.sPlayerName ())

    --inform Main AI about the new user
    user.sendEvent ( this.getUser ( ), "AIMainMoneyBlock", "onAddPlayer", nUserID,  this.sPlayerName() )

    log.message ( "User Enter Scene : ", nUserID )

    --add new user to the voip diffusion list ( if the diffusion is not started, this will have no impact )
    microphone.addUserToDiffusionList ( nUserID )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
