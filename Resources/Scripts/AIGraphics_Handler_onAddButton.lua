--------------------------------------------------------------------------------
--  Handler.......... : onAddButton
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onAddButton ( sTag, nAdd )
--------------------------------------------------------------------------------
	
if(not this.bUseDebugHUD ( ))then

    local sID = ""
    local sPrefixHUD = ""
    
    --GET RES ID
    for i=0 , hashtable.getSize ( this.htPrefixCryptoHUD ( ) ) -1
    do
    	if( string.contains ( sTag, hashtable.getAt ( this.htPrefixCryptoHUD ( ), i  )))then
            sID = hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ), i )
            sPrefixHUD = hashtable.getAt ( this.htPrefixCryptoHUD ( ), i )
            break
        end
    end
    if(sID == "")then return end
    
    --GET RES AMOUNT
    local nCurAmount = hashtable.get ( this.htHUDTradeValue ( ), sID)
    if(not nCurAmount)then 
        hashtable.add ( this.htHUDTradeValue ( ), sID, 0)
        nCurAmount = 0 
    end
    
    --IS ABLE TO TRADE
    local bTransactionValid = true
    local bCantSell = false
    local bCantBuy = false
    local nMaxOrder = hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MaxOrderCryptoCount")
    local bMaxOrderReached = false
    local nNewAmount= nCurAmount + nAdd
    if(this.GetTotalBuyAmount(  ) + this.EvaluateTransactionValue ( sID, nAdd )>this.nCash ( ) )then bCantBuy = true end
    if(hashtable.get ( this.htHUDTradeValue ( ),sID) + nAdd < -this.GetMaxSellAmount ( sID ))then bCantSell = true end
    if(this.GetTotalOrderCount ( )- math.abs ( nCurAmount) + math.abs ( nNewAmount) > nMaxOrder  )then bMaxOrderReached = true end
    
    if(bMaxOrderReached)then
        log.message ( "max order reached" )
        bTransactionValid = false
    elseif(nAdd > 0 and bCantBuy and nNewAmount >0 )then 
        log.message ( "too much expensive" )
        bTransactionValid = false
    elseif( nAdd < 0 and bCantSell and nNewAmount <0)then 
        log.message ( "cant sell more" )
        bTransactionValid = false
    end
    
   
    --VALID FEEDBACK
    if(bTransactionValid and nAdd>0)then
        local sAction = sPrefixHUD..".ClicBuy"
        if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction )end
        hud.callAction ( this.getUser ( ), sAction  )  
    elseif(bTransactionValid and nAdd<0)then
        local sAction = sPrefixHUD..".ClicSell"
         if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction )end
        hud.callAction ( this.getUser ( ), sAction  )  
    else
        --INVALID RETURN
        return
    end
    
    
    --SAVE VALUE
    hashtable.set ( this.htHUDTradeValue ( ),sID, nNewAmount )
    
    -- DISPLAY NEW VALUE
    local hC=hud.getComponent ( this.getUser ( ), hashtable.get (  this.htPrefixCryptoHUD ( ), sID )..".LabelCurCount" )
    if(hC)then
        local sPrefix = "None"
        --local sSuffix = "/"..nMaxOrder
        local sSuffix = ""
        local vValue = math.abs(nNewAmount)
        local r = 65
        local g = 65
        local b = 65
        
        if(nNewAmount < 0)then 
            sPrefix = "Sell "
            r = 127
            g = 127
            b = 127
        elseif(nNewAmount > 0)then 
            sPrefix = "Buy "
            r = 0
            g = 127
            b = 0
        else 
            vValue = "" 
            sSuffix = ""
        end
        hud.setLabelText ( hC, sPrefix..vValue..sSuffix)
        hud.setComponentForegroundColor ( hC, r, g, b, 255 )
    end

    this.UpdatePendingHUD (  )
    this.UpdateCashHUD ( )

else
    ---------------------------------------------------------------------------------------------------------------------
	local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sTag )
    local nV=0
    if(hashtable.contains ( this.htHUDVars ( ),sTag ))
    then
  
        hashtable.set ( this.htHUDVars ( ),sTag,math.clamp ( hashtable.get ( this.htHUDVars ( ),sTag)+nAdd,0,hashtable.get ( this.htHUDVars ( ),sTag)+nAdd))
    else
        hashtable.add ( this.htHUDVars ( ),sTag,math.clamp ( nAdd,0,nAdd) )
    end
    
    hud.setLabelText ( hC,""..hashtable.get ( this.htHUDVars ( ),sTag)  )
    
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
