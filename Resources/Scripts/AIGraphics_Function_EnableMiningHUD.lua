--------------------------------------------------------------------------------
--  Function......... : EnableMiningHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableMiningHUD ( bEnable )
--------------------------------------------------------------------------------
	
if(this.bUseDebugHUD ( ))then return end
    
    

if(bEnable == false)then
    --DISABLE PANEL
    this.EnableMiningPanelHUD ( false)
end
    
    
-- SHOW/HIDE ORDER BOOK 
local hC = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerOrderBooks" )
if(hC)then hud.setComponentVisible ( hC, bEnable  )end


-- ENABLE 3D OBJ
for i=0 , hashtable.getSize ( this.htMiningModel ( ) ) -1
do
    local hObj = hashtable.getAt ( this.htMiningModel ( ), i )
    if(hObj)then
        object.setVisible ( hObj, bEnable )
    end
end

local hScene = user.getScene ( this.getUser ( ) )
local hObj = scene.getTaggedObject ( hScene, "Surround_Mining" )
object.setVisible ( hObj, bEnable )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
