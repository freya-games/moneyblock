--------------------------------------------------------------------------------
--  Function......... : changePlayerCashRatio
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.changePlayerCashRatio ( nPlayerIndex,nRatio)
--------------------------------------------------------------------------------
	

                
        
    if(hashtable.contains (this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash"))
    then
        local nV=hashtable.get (this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash" )
        
           local nNewValue=math.roundToNearestInteger ( nV*(nRatio))
        if(nNewValue<0)
        then
            nNewValue=0
        end
        hashtable.set ( this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash",nNewValue)
        
        local hUser=application.getUser (  table.getAt ( this.tUsers ( ),nPlayerIndex ))
        user.sendEvent ( hUser,"AIPlayer", "onGetMyCashCallback",this.getPlayerCash (user.getID ( hUser )  ))

    else
    
    
        --hashtable.add ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID ,nValueToAdd )
        log.error ( "AIMainMoneyBlock.changePlayerCash  ".."Player."..nPlayerIndex..".Cash does not exist")
    end
    return
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
