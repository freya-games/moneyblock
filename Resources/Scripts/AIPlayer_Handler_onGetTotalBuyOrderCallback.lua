--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalBuyOrderCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetTotalBuyOrderCallback (sCryptoID ,nBuyAmount  )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htBuyOrder ( ),sCryptoID ))
    then
        hashtable.set ( this.htBuyOrder ( ),sCryptoID, nBuyAmount )
    else
        hashtable.add ( this.htBuyOrder ( ),sCryptoID,nBuyAmount )
    end

    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetTotalBuyOrderCallback", sCryptoID ,nBuyAmount)
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
