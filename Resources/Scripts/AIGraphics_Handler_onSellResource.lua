--------------------------------------------------------------------------------
--  Handler.......... : onSellResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onSellResource( sType, sListTag,sTag )
--------------------------------------------------------------------------------
	
    
if(sType=="MINER")
then

    local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
    if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
    then
        user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onSell" , table.getAt ( this.tMinerHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
    
    end
elseif(sType=="ENERGY")
then
    local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
    if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
    then
        user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onSell" , table.getAt ( this.tEnergyHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
    
    end
elseif(sType=="CRYPTO")
then
        local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
    if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
    then
       user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onSell" , table.getAt ( this.tCryptoHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
    
    end
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
