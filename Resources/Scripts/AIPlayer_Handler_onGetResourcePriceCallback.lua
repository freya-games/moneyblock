--------------------------------------------------------------------------------
--  Handler.......... : onGetResourcePriceCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetResourcePriceCallback ( sResID,nPrice  )
--------------------------------------------------------------------------------
	
	if(hashtable.contains ( this.htResourcePrice ( ),sResID ))
    then
        hashtable.set ( this.htResourcePrice ( ),sResID, nPrice )
    else
        hashtable.add ( this.htResourcePrice ( ),sResID,nPrice )
    end
	
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetResourcePriceCallback", sResID,nPrice )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
