--------------------------------------------------------------------------------
--  Handler.......... : onKeyboardKeyDown
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Chat.onKeyboardKeyDown ( kKeyCode )
--------------------------------------------------------------------------------
	
    local hEditChat = hud.getComponent ( this.getUser ( ), "Network_Chat.EditChat" )
    if ( hEditChat )
    then
        if ( kKeyCode == input.kKeyReturn )
        then
            if ( hud.getFocusedComponent ( this.getUser ( ) ) ~= hEditChat  )
            then
                hud.setFocus ( hEditChat )
            end
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
