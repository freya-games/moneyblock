--------------------------------------------------------------------------------
--  Handler.......... : onReturnCardToContainer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onReturnCardToContainer ( sTagCard, nPosX, nPosY )
--------------------------------------------------------------------------------
	
	local hContainerActionCards= hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerActionCards" )
    local hCard = hud.getComponent ( this.getUser ( ), sTagCard )
    local nSpaceCard = -10
    local hUser = this.getUser ( )
    
    local t = table.newInstance ( )
    string.explode (sTagCard, t, "."  )
    local sPrefix = table.getFirst ( t )
    
   
    
    if(hContainerActionCards and hCard)then
        
        hud.setComponentContainer ( hCard, hContainerActionCards )
        hud.setComponentActive ( hCard, true  )
        
        local nSizeX =  100 / table.getSize (this.tActionCards ( )) - nSpaceCard
        local nSizeY =  nSizeX / this.nRatioCardHUD ( )  
        
        local sAction = sPrefix..".SetPosition"
        if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
        hud.callAction ( hUser, sAction , nPosX, nPosY )
        
          
        sAction = sPrefix..".SetSize"
        if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
        hud.callAction ( hUser, sAction ,  nSizeX, nSizeY  )
    
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
