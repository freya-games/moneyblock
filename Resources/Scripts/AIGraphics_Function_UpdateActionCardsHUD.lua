--------------------------------------------------------------------------------
--  Function......... : UpdateActionCardsHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateActionCardsHUD ( )
--------------------------------------------------------------------------------


if(not this.bUseDebugHUD ( ))then

local hComponent= hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerActionCards" )
local hContainerCards= hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerCards" )
local hUser = this.getUser ( )
local nSpaceCard = -10
local nContainerPosX, nContainerPosY = hud.getComponentPosition ( hContainerCards )
local nContainerSizeX, nContainerSizeY = hud.getComponentSize ( hContainerCards )

--CREATE CARDS HUD
if(application.isResourceReferenced ( "Card", application.kResourceTypeHUD ))then
    if(hComponent)then
        while (table.getSize (this.tPrefixActionCardHUD ( )) < table.getSize (this.tActionCards ( )) )
        do
            
            local sPrefix = "Card_"..table.getSize (this.tPrefixActionCardHUD ( ))
            if(hud.newTemplateInstance ( hUser, "Card", sPrefix ))then
            
                table.add ( this.tPrefixActionCardHUD ( ), sPrefix )
                hud.callAction ( hUser, sPrefix..".SetActionCard" )
                 local hCard = hud.getComponent ( hUser, sPrefix..".ContainerGlobal" )
                if(hCard)then
                    hud.setComponentContainer ( hCard, hComponent )
                end
            else
                break
            end
        end
    end
else
    log.warning ( "Card HUD not found" )
end


--UPDATE CARD TRANSFORM
for i=0,table.getSize ( this.tActionCards ( ) )-1
do

    local nCardID=table.getAt ( this.tActionCards ( ),i )
    
    
    if(table.getSize ( this.tPrefixActionCardHUD ( ) )>i)then
    
        local sPrefix=table.getAt ( this.tPrefixActionCardHUD ( ),i )
        local bReturnToContainer = false
        
         --RETURN TO CONTAINER IF NOT IN USE
         if(this.sTagCurUsedCard ( ) ~= sPrefix..".ContainerGlobal")then
            local hCard = hud.getComponent ( hUser, sPrefix..".ContainerGlobal" )
            
            --IS INSIDE THE CONTAINER ?
            if(hCard)then   
                local hContainer = hud.getComponentContainer ( hCard)
                
                if(hContainer)then
                    local sTagContainer = hud.getComponentTag ( hContainer )
                    
                    if( sTagContainer ~= "PlayerMainHUD.ContainerActionCards" )then
                        hud.setComponentContainer ( hCard, hComponent  )
                        bReturnToContainer = false
                        
                        --FADE IN
                        sAction = sPrefix..".FadeIn"
                        if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
                        hud.callAction ( hUser, sAction )
                        
                   end
                else
                    bReturnToContainer = true
                end
            end
            
            
            --SET TRANSFORM
            local nPosX =  (i+0.5) / table.getSize (this.tActionCards ( )) * 100
            local nSizeX =  100 / table.getSize (this.tActionCards ( )) - nSpaceCard
            local nSizeY =  nSizeX / this.nRatioCardHUD ( )  
            
            local sAction = sPrefix..".SetInfos"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction , this.GetActionCardInfos ( nCardID, "Name" ), this.GetActionCardInfos ( nCardID, "Description" ), this.GetActionCardInfos ( nCardID, "Img" ) )
          
            
            if(bReturnToContainer)then
                
                local nReturnSizeX = 10
                
                sAction = sPrefix..".ReturnToContainer"
                if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
                hud.callAction ( hUser, sAction , nContainerPosX, nContainerPosY - nContainerSizeY/4, nPosX, 50 )
                
                sAction = sPrefix..".SetSize"
                if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
                hud.callAction ( hUser, sAction , nReturnSizeX, nReturnSizeX / this.nRatioCardHUD ( )    )
            
            else
                sAction = sPrefix..".SetPosition"
                if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
                hud.callAction ( hUser, sAction , nPosX, 50 )
                
                  
                sAction = sPrefix..".SetSize"
                if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
                hud.callAction ( hUser, sAction ,  nSizeX, nSizeY  )
                
            end
           
        end
    end

end



------------------ DEBUG HUD ----------
else
    local hComponent= hud.getComponent ( this.getUser ( ), "PlayerHUD.ActionCards" )
    hud.removeListAllItems (hComponent)

    table.empty ( this.tPrefixActionCardHUD ( ) )	
    for i=0,table.getSize ( this.tActionCards ( ) )-1
    do
        local nCardID=table.getAt ( this.tActionCards ( ),i )
        local nIndex=hud.addListItem ( hComponent, this.GetActionCardInfos ( nCardID, "Name") )
           
        table.add (  this.tPrefixActionCardHUD ( ), nCardID)

    end
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
