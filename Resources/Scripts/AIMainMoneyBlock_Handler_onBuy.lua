--------------------------------------------------------------------------------
--  Handler.......... : onBuy
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onBuy (nUserID,sResourceID,nAmount,sCallback  )
--------------------------------------------------------------------------------
	  local bCanBuy=false
    if(nUserID)
    then
        local nPlayerIndex=this.getPlayerIndex ( nUserID )
      
        
          
            local nPlayerCash=this.getPlayerCash ( nUserID) 
            if(nPlayerCash)
            then
                local nResPrice=this.getResourcePrice ( sResourceID )
                
                
                
                local nResCount=this.getResourceCount (  sResourceID)
                
             
                    
                if(nResCount==-1)
                then
                    local nTotal =nAmount*nResPrice
                    if(nTotal>nPlayerCash)
                    then
                         bCanBuy=false
                    else
                       
                       
                    --    this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                     --   this.changePlayerCash ( nPlayerIndex,-1*nTotal )
                        if(this.getResourceType ( sResourceID )=="CRYPTO")
                        then
                               bCanBuy=this.RegisterPlayerBuyOrder ( nUserID,sResourceID,nAmount )
                               if(bCanBuy)
                               then
                               
                                this.changePlayerCash ( nPlayerIndex,-1*nTotal )
                            end
                        else
                                bCanBuy=true
                                this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                                this.changePlayerCash ( nPlayerIndex,-1*nTotal )
                        end
                     
                    end
                    
                else
                
                    if(nResCount<nAmount)
                    then
                        if(nResCount>0)
                        then
                            nAmount=nResCount
                           
                        else
                            bCanBuy=false
                        end
                    else
                        
                        local nTotal =nAmount*nResPrice
                        if(nTotal>nPlayerCash)
                        then
                           bCanBuy=false
                            
                            
                        else
                           
                              local nPlayerIndex=this.getPlayerIndex ( nUserID )
                      --      this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                           
                            
                       --     this.changePlayerCash ( nPlayerIndex,-1*nTotal )
                            if(this.getResourceType ( sResourceID )=="CRYPTO")
                            then
                                if(this.RegisterPlayerBuyOrder ( nUserID,sResourceID,nAmount ))
                                then
                                    this.changeGameResourceCount ( sResourceID,-1*nAmount )
                                    bCanBuy=true
                                end
                            else
                                 this.changeGameResourceCount ( sResourceID,-1*nAmount )
                                  this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                                  this.changePlayerCash ( nPlayerIndex,-1*nTotal )
                                 bCanBuy=true
                            end
                        
                            
                        end
                        
                        
                        
                    end
                    
                end
                
                user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", sCallback,sResourceID,nAmount,this.getPlayerResourceCount (nUserID, sResourceID ),bCanBuy)
                
              --  user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onGetMyCashCallback",this.getPlayerCash (  nUserID))
            else
        
                log.error ( "AIMainMoneyBlock.onBuy PlayerCash Error" )
            end
  
  
    else
         log.error ( "AIMainMoneyBlock.onBuy UserID Error" )
     end   
    

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
