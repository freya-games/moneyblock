--------------------------------------------------------------------------------
--  Handler.......... : onBroadcastChat
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onBroadcastChat ( sChatText )
--------------------------------------------------------------------------------
	    
    local hScene  = application.getCurrentUserScene ( )
    if ( hScene ) 
    then
        local nLocalUserID = user.getID ( this.getUser ( ) )   
        --send the text to all users in the scene (including the local user)
        scene.sendEventToAllUsers ( hScene, "Network_Connection", "onReceiveChat", nLocalUserID, sChatText )
	end
    
    
--------------------------------------------------------------------------------
end 
--------------------------------------------------------------------------------
