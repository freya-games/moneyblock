--------------------------------------------------------------------------------
--  State............ : CheckTimer
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.CheckTimer_onLoop ( )
--------------------------------------------------------------------------------

    local dt=application.getLastFrameTime ( )

    if(this.bPhaseTimer ( ))
    then

        local dt=application.getLastFrameTime ( )
        this.nPhaseTimer (this.nPhaseTimer()+dt)
        if(this.bPhaseTimerLimit ( ))
        then
            if(this.CheckTimerCurrentPhase ( ))
            then
                this.NextPhase (  )
            else
                --SYNCHRO PHASE TIMER
                if(this.nSynchroTimer ( )>5)then
                    this.nSynchroTimer (0)
                    this.sendPhaseTime ( this.nPhaseTimer ( ) )
                else
                    this.nSynchroTimer (this.nSynchroTimer () + dt )
                end
            end
        end
    end

    if(this.bGlobalTimer ( ))
    then

        this.nGlobalTimer (this.nGlobalTimer()+dt)
        if(this.bGlobalTimerLimit ( ))
        then

            if(this.checkGlobalTimer ( ))
            then
                this.EndGame ( )
            end
        end
    end



    --DEBUG TEST
    --local dt=application.getLastFrameTime ( )
    --this.nGlobalTimer (this.nGlobalTimer()+dt)

        --if(this.nGlobalTimer()>5)
        --then
            --this.EndGame ( )
            --this.Idle ( )

        --end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
