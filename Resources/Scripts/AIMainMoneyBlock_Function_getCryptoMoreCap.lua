--------------------------------------------------------------------------------
--  Function......... : getCryptoMoreCap
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getCryptoMoreCap ( )
--------------------------------------------------------------------------------


local nMax=0
local sReturnCrypto
local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

for j=0,nCount-1
do
    local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
   
    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".Type")
    if(sType=="CRYPTO")
    then
        local nTotal=0
        for i =0, this.nPlayerCount ( )-1
        do
           
            local nCryptoCount=this.getPlayerResourceCount ( table.getAt ( this.tUsers ( ),i ), sCryptoID)
            local nCryptoPrice=this.getResourcePrice (sCryptoID )
            if(nCryptoCount)
            then
                 nTotal=nTotal+nCryptoCount*nCryptoPrice
            end
          
        
        end
      
        if(nTotal==nMax)
        then
            if(math.roundToNearestInteger (  math.random ( 0,1 ))==0)
            then
                sReturnCrypto=sCryptoID
                nMax=nTotal   
            end
        end
        if(nTotal>nMax)
        then
            sReturnCrypto=sCryptoID
            nMax=nTotal
        end
    end
end
return sReturnCrypto
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
