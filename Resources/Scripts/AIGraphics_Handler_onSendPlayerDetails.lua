--------------------------------------------------------------------------------
--  Handler.......... : onSendPlayerDetails
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onSendPlayerDetails (  )
--------------------------------------------------------------------------------

    local sName = "Name Error"

    local hC=hud.getComponent ( this.getUser ( ),"PlayerDetails.Name" )
    if(hC)then sName = hud.getEditText ( hC ) end

    user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ) , "onSendPlayerDetails" , this.nPictureID ( ), sName )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
