--------------------------------------------------------------------------------
--  Function......... : UpdateTotalPortfolio
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateTotalPortfolio ( nTotalCryptoCount, nTotalCryptoValue )
--------------------------------------------------------------------------------
	
	 local hUser = this.getUser ( )
    
    local sActionTag = "PlayerMainHUD.SetTotalCrypto"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nTotalCryptoCount, ""..nTotalCryptoValue..this.GetCurrency (  ) )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
