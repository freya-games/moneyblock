--------------------------------------------------------------------------------
--  Function......... : CreateOrderBooksHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateOrderBooksHUD ( )
--------------------------------------------------------------------------------
	
    --GET MAIN HUD
    local _nCryptoCount = hashtable.getSize ( this.htPrefixOrderBookHUD ( ) )
    local hUser = this.getUser ( )
    local hContainerCrypto = hud.getComponent ( hUser, "PlayerMainHUD"..".ContainerOrderBooks" )
    
    
    local nSpaceOrderBook = 1
    local nPosY = 90
    
    --CREATE ALL ORDER BOOKS
    if(hContainerCrypto)then
    
        for i =0 , _nCryptoCount-1
        do
               
            --CREATE HUD 
            local sPrefixOrderBookHUD = "OrderBook_"..i
            hashtable.set ( this.htPrefixOrderBookHUD ( ), hashtable.getKeyAt ( this.htPrefixOrderBookHUD ( ), i ), sPrefixOrderBookHUD )
            hud.newTemplateInstance ( hUser, "OrderBook", sPrefixOrderBookHUD)
            local hChild = hud.getComponent ( hUser, sPrefixOrderBookHUD..".ContainerGlobal" )
            
            --SET HUD TRANS
            local nWidth = 100/_nCryptoCount -  nSpaceOrderBook
            hud.setComponentContainer ( hChild, hContainerCrypto)
            hud.setComponentPosition ( hChild, (i+0.5)*100/(_nCryptoCount), nPosY )
            hud.setComponentSize ( hChild, nWidth , nWidth*1.5 )
            
            hud.callAction ( hUser, sPrefixOrderBookHUD..".Hide" )
            
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
