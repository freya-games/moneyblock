--------------------------------------------------------------------------------
--  Function......... : UpdateCryptoInfos
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateCryptoInfos ( sResID, nCount, nPrice )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    
	local sActionTag = hashtable.get ( this.htPrefixCryptoHUD ( ), sResID )..".SetInfosCrypto"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nPrice, ""..nCount, ""..nPrice*nCount )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
