--------------------------------------------------------------------------------
--  Handler.......... : onReceiveChat
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onReceiveChat ( nUserID, sText )
--------------------------------------------------------------------------------
	
    local sName = hashtable.get ( this.aPlayerName ( ), ""..nUserID )
    if ( sName ) 
    then
        user.sendEvent ( this.getUser ( ), "Network_Chat", "onReceiveText", nUserID, sName, sText )
    else
        log.warning ( "Receive Chat from unknown player : ", sText )
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
