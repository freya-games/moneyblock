--------------------------------------------------------------------------------
--  Function......... : OpenDialogBox
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.OpenDialogBox (sText )
--------------------------------------------------------------------------------
	
    
    if(sText)
    then
        table.add ( this.tDialogMessageQueue ( ),sText )
        
        if(not this.bUseDebugHUD ( ))then
        
            local hUser = this.getUser ( )
            local hComponent = hud.getComponent ( hUser, "PopUpHUD.LabelPopUpMessage" )
            if(hComponent)then
                local hContainer = hud.getComponent ( hUser, "PopUpHUD.ContainerGlobal" )
                
                if(hud.isComponentVisible ( hContainer ) == true or hud.isActionRunning ( hUser, "PopUpHUD.Show" ))then
                
                    local sOldText = hud.getLabelText ( hComponent )
                    hud.setLabelText ( hComponent, sOldText.."\n"..sText  )
                else
                    hud.callAction ( this.getUser ( ), "PopUpHUD.Show")
                    hud.setLabelText ( hComponent, sText  )
                end
            end
        
           
        ---------------------DEBUG HUD --------------------------------
        else
             local hD=hud.getComponent ( this.getUser ( ),"PlayerHUD.Dialog" )
            hud.setLabelText ( hud.getComponent ( this.getUser ( ),"PlayerHUD.DialogText" ),sText )
            hud.setComponentVisible ( hD,true )
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
