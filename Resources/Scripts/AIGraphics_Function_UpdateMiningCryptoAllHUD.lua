--------------------------------------------------------------------------------
--  Function......... : UpdateMiningCryptoAllHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningCryptoAllHUD ( )
--------------------------------------------------------------------------------
	
if(not this.bUseDebugHUD ( ))then return end


    
local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningCryptoAll" )
hud.removeListAllItems ( hComponent )


for i=0,hashtable.getSize ( this.htMining ( ) )-1
do

    local sS=hashtable.getKeyAt ( this.htMining ( ),i )
    local t=table.newInstance ( )
    string.explode ( sS,t,"." )
    
    local sCryptoID=table.getAt ( t,0 )
    local sResID=table.getAt ( t,1 )
    local nAmount = hashtable.getAt ( this.htMining ( ),i )
    if(nAmount>0)
    then
    
        hud.addListItem ( hComponent,this.GetResourceName (   sCryptoID).." "..this.GetResourceName ( sResID).." "..nAmount )
    end
    
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
