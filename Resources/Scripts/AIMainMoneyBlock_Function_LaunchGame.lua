--------------------------------------------------------------------------------
--  Function......... : LaunchGame
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.LaunchGame ( )
--------------------------------------------------------------------------------

this.CheckTimer ( )
  
--Action Cards
local nInitActionCards =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.InitActionCardsCount")


for j=0,nInitActionCards-1
do
    this.GiveActionCardToAllPlayers ( )
end

    
this.NextPhase ( )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
