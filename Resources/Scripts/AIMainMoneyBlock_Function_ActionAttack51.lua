--------------------------------------------------------------------------------
--  Function......... : ActionAttack51
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionAttack51( nCardIndex, sCryptoID)
--------------------------------------------------------------------------------


    --log.warning ( this.tCurBlockMined ( ) )


if(table.contains ( this.tCurBlockMined ( ), sCryptoID ))then



    local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end


    --GET CARD PARAMS
    local sMinerID =hashtable.get (this.htActionCards ( ),sTag..".Effect.Miner.ID")
    local nMinerRatio=hashtable.get (this.htActionCards ( ),sTag..".Effect.Miner.Ratio")
    local nCryptoRatio=hashtable.get (this.htActionCards ( ),sTag..".Effect.CryptoRatio")



    local nTotalMiners = 0

    --GET ALL MINERS
    for nPlayerIndex =0, this.nPlayerCount ( )-1
    do
        local nMinerAmount= hashtable.get ( this.htMining ( ), "Player."..nPlayerIndex.."."..sCryptoID.."."..sMinerID )
        if(nMinerAmount)
        then

            nTotalMiners = nTotalMiners + nMinerAmount
        end

    end


   --COMPARE PLAYER MINERS and ALL MINERS
    local bSucces = false
    for nPlayerIndex =0, this.nPlayerCount ( )-1
    do
        local nMinerAmount= hashtable.get ( this.htMining ( ), "Player."..nPlayerIndex.."."..sCryptoID.."."..sMinerID )
        if(nMinerAmount)
        then
            --log.error ( nMinerAmount/nTotalMiners.." on Crypto "..sCryptoID )

            if (nMinerAmount/nTotalMiners>nMinerRatio)
            then
                    bSucces = true
                log.message ( "Attack51 sucess from Player "..nPlayerIndex.." on sCryptoID with "..nMinerAmount.."/"..nTotalMiners.." GPU" )
                this.changeGameResourcePriceRatio ( sCryptoID, nCryptoRatio )

            end
        end
    end



else
     log.message ( "Attack51 fail, block was not mined" )

end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
