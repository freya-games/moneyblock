--------------------------------------------------------------------------------
--  Function......... : CreateMiningModel
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateMiningModel ( )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
    
    
    -- GET MINING BOX
        for i=0 , hashtable.getSize ( this.htMiningModel ( ) )-1
        do
            local sCryptoID = hashtable.getKeyAt ( this.htMiningModel ( ), i )
            
            --GET OBJ
            local hObj = scene.getTaggedObject ( hScene, "Rig_"..(i+1 ))
--             if(hObj == nil )then
--                 hObj= scene.createRuntimeObject ( hScene, "P_RigMining" )
--             end
            
            hashtable.set ( this.htMiningModel ( ), sCryptoID, hObj )
            object.setVisible ( hObj, false)
            
            --SET MATERIALS
            for nIndexChild=1 , object.getChildCount ( hObj )-1 
            do
            	local hChild = object.getChildAt ( hObj, nIndexChild )
                object.setVisible ( hChild, false )
                
                if(string.contains ( shape.getMeshName ( hChild ), "Cell"))then
                    shape.setMeshSubsetMaterial ( hChild, 1, "M_Cell_"..i )
                end
            end
            
            local hFirstChild = object.getChildAt ( hObj, 0 )
            --0 : base   1: bay ??  2 : panel
            shape.setMeshSubsetMaterial ( hFirstChild, 2, "M_PanelRig_"..i )

    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
