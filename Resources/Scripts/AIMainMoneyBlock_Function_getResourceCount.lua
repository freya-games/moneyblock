--------------------------------------------------------------------------------
--  Function......... : getResourceCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getResourceCount (sResourceID )
--------------------------------------------------------------------------------
	
if(hashtable.contains (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Count" ))
then
    return hashtable.get (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Count" )
else
    log.error (  )
    return nil
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
