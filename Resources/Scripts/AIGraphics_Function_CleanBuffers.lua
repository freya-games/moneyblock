--------------------------------------------------------------------------------
--  Function......... : CleanBuffers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CleanBuffers ( )
--------------------------------------------------------------------------------
	
hashtable.empty (this.htMining ( )  )
hashtable.empty ( this.htTotalMining ( ) )
hashtable.empty ( this.htPendingOrders ( ) )
hashtable.empty ( this.htBuyOrder ( ) )
hashtable.empty ( this.htSellOrder ( ) )
hashtable.empty ( this.htCardPlayed ( ) )
hashtable.empty ( this.htHUDTradeValue ( ) )
hashtable.empty ( this.htCurActiveCards ( ) )	
hashtable.empty ( this.htSlotPreviewCardID ( ) )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
