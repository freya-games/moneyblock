--------------------------------------------------------------------------------
--  Handler.......... : onReceiveMiningReward
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onReceiveMiningReward ( sCryptoID,nNewTotalCryptoCount,nReward )
--------------------------------------------------------------------------------
	this.openDialogBox ( "Receive "..nReward.." "..sCryptoID.." for mining")
		
	if(hashtable.contains ( this.htResourceCount ( ),sCryptoID ))
    then
        hashtable.set ( this.htResourceCount ( ),sCryptoID, nNewTotalCryptoCount )
    else
        hashtable.add ( this.htResourceCount ( ),sCryptoID,nNewTotalCryptoCount )
    end
	
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onReceiveMiningReward", sCryptoID,nNewTotalCryptoCount,nReward )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
