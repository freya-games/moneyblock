--------------------------------------------------------------------------------
--  Function......... : Volume
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.Volume ( )
--------------------------------------------------------------------------------
    local nExpectation=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Market.Volume.Expectation")
    local nDeviation=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Market.Volume.Deviation")
	return math.gaussianRandom ( nExpectation,nDeviation )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
