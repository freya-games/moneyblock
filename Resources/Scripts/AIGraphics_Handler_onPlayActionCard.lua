--------------------------------------------------------------------------------
--  Handler.......... : onPlayActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onPlayActionCard (  )
--------------------------------------------------------------------------------
	
    local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ActionCards" )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
    
    local nCardID=table.getAt ( this.tPrefixActionCardHUD ( ),nItem )
    
    
    hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ActionCrypto" )

    nItem=hud.getListSelectedItemAt ( hC,0 )
    if(nItem>-1)
    then
        local sCryptoID=table.getAt ( this.tActionCryptoHUD ( ),nItem )
        user.sendEvent ( this.getUser ( ) , this.sAIPlayer ( ), "onPlayActionCard", nCardID, sCryptoID  )
    else
        this.OpenDialogBox ( "Select a crypto to apply Action Effect ")
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
