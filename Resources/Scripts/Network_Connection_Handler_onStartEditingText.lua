--------------------------------------------------------------------------------
--  Handler.......... : onStartEditingText
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onStartEditingText ( sTag )
--------------------------------------------------------------------------------

    local bValid = false
    local hC = hud.getComponent ( this.getUser ( ), sTag )
    if(hC)then
        if(hud.getComponentType ( hC ) == hud.kComponentTypeEdit)then bValid=true end
    end

    if(bValid)then

        local sText = hud.getEditText ( hC )
        local nPosition = string.getLength ( sText )
        hud.setEditCursorPosition ( hC, nPosition )

    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
