--------------------------------------------------------------------------------
--  State............ : DownloadingXMLConfigFiles
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.DownloadingXMLConfigFiles_onLoop ( )
--------------------------------------------------------------------------------

    local nS=xml.getReceiveStatus ( this.xMoneyBlockConfig ( ) )
    local nSM=xml.getReceiveStatus ( this.xMarketCards ( ) )
    local nSA=xml.getReceiveStatus ( this.xActionCards ( ) )
    if(nS==1 and nSM ==1 and nSA==1)
    then

        this.ParseActionCards ( )
        this.ParseMarketCards ( )
        this.ParseMoneyBlockConfig ( )
        this.ParseTranslation ( )



        --Sound Design
        hud.setSoundBank ( this.getUser ( ),"Interface" )

        --DEFAULT NAME AND PICTURE
        hud.newTemplateInstance ( this.getUser ( ),"PlayerDetails","PlayerDetails" )
        local hC=hud.getComponent ( this.getUser ( ),"PlayerDetails.Picture" )

        hud.setComponentBackgroundImage ( hC,"Picture"..this.nPictureID ( ) )
        hC=hud.getComponent ( this.getUser ( ),"PlayerDetails.Name" )

        if(string.isEmpty (  this.sPlayerName ( )))
        then

                  hud.setEditText( hC,table.getAt ( this.tRandomName ( ),this.nPictureID ( ) ) )
        else
        hud.setEditText( hC,this.sPlayerName ( ) )
        end


        --
        this.Idle ( )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
