--------------------------------------------------------------------------------
--  Handler.......... : onNextDialogMessage
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onNextDialogMessage (  )
--------------------------------------------------------------------------------
	
    table.removeLast ( this.tDialogMessageQueue ( ) )
    local sText=table.getLast ( this.tDialogMessageQueue ( ) )
     local hD=hud.getComponent ( this.getUser ( ),"PlayerHUD.Dialog" )
    if(sText)
    then
    
       
        
       
        hud.setLabelText ( hud.getComponent ( this.getUser ( ),"PlayerHUD.DialogText" ),sText )
        hud.setComponentVisible ( hD,true )
    else
         hud.setLabelText ( hud.getComponent ( this.getUser ( ),"PlayerHUD.DialogText" ),"No More Message" )
         hud.setComponentVisible ( hD,false)
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
