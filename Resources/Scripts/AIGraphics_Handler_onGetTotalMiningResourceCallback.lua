--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalMiningResourceCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetTotalMiningResourceCallback( sCryptoID,sMiningResID,nAmount )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htTotalMining ( ),sCryptoID.."."..sMiningResID ))
    then
        hashtable.set ( this.htTotalMining ( ),sCryptoID.."."..sMiningResID, nAmount )
    else
        hashtable.add ( this.htTotalMining( ),sCryptoID.."."..sMiningResID, nAmount )
    end

    this.UpdateMarketHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
