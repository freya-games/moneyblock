--------------------------------------------------------------------------------
--  Function......... : GetBodyPosition
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetBodyPosition ( )
--------------------------------------------------------------------------------
	
    local hScene = user.getScene ( this.getUser ( ) )
    local nUserID = user.getID ( this.getUser ( ) )
    
	local hObj = scene.getTaggedObject ( hScene, "Body."..nUserID )
    
    if(hObj)then
        return object.getTranslation ( hObj, object.kGlobalSpace )
	end
    
    return 0,0,0
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
