--------------------------------------------------------------------------------
--  Function......... : UnselectAllBuy
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UnselectAllBuy ( )
--------------------------------------------------------------------------------

if(this.bUseDebugHUD ( ))then

    local hList = hud.getComponent ( this.getUser ( ), "PlayerHUD.Crypto" )
    hud.selectListAllItems ( hList, false) 


    hList = hud.getComponent ( this.getUser ( ), "PlayerHUD.Miner" )
    hud.selectListAllItems ( hList, false) 


    hList = hud.getComponent ( this.getUser ( ), "PlayerHUD.Energy" )
    hud.selectListAllItems ( hList, false) 

    hud.callAction ( this.getUser ( ),"PlayerHUD.HideAllBuy" )
  
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
