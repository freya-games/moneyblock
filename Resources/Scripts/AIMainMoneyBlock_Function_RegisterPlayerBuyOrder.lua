--------------------------------------------------------------------------------
--  Function......... : RegisterPlayerBuyOrder
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.RegisterPlayerBuyOrder (nUserID,sResourceID,nAmount )
--------------------------------------------------------------------------------
local bReturn =false
    
if(this.checkTransationCount ( nPlayerIndex ))
then

    
    local nPlayerIndex=this.getPlayerIndex (nUserID  )
    
     if(hashtable.contains ( this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" ))
    then
        hashtable.set ( this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" ,0)
    end
    
    if(this.checkOrderCryptoCount (nPlayerIndex,sResourceID,nAmount ))
    then
        if(hashtable.contains ( this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" ))
        then
             hashtable.set( this.htCurrentBuyOrders( ),"Player."..nPlayerIndex.."."..sResourceID..".Count",nAmount )
        else
             hashtable.add( this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count",nAmount )
        end
        
        if(hashtable.contains ( this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Price" ))
        then
             hashtable.set( this.htCurrentBuyOrders( ),"Player."..nPlayerIndex.."."..sResourceID..".Price",this.getResourcePrice ( sResourceID ) )
        else
             hashtable.add( this.htCurrentBuyOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Price",this.getResourcePrice ( sResourceID )  )
        end



        if(hashtable.contains ( this.htCurrentBuyOrdersGlobal ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" ))
        then
             hashtable.set( this.htCurrentBuyOrdersGlobal( ),"Player."..nPlayerIndex.."."..sResourceID..".Count",nAmount )
        else
             hashtable.add( this.htCurrentBuyOrdersGlobal ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count",nAmount )
        end
        
        local nTransactionCount=hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".TransactionCount")
        hashtable.set ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".TransactionCount",nTransactionCount+1)
        bReturn=true
    else
         user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onReceiveMessage","[Server] Max Total Crypto Count Order Reached")
    end

else
    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onReceiveMessage","[Server] Max Transaction Count Reached")
end

return bReturn
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
