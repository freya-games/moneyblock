--------------------------------------------------------------------------------
--  Handler.......... : onJoypadButtonDown
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onJoypadButtonDown ( nJoypad, nButton )
--------------------------------------------------------------------------------
local bContain=false
for i=0,table.getSize ( this.tJoypadMapping ( ) )-1
do
    local nPad=table.getAt ( this.tJoypadMapping ( ),i )
    if(nPad==nJoypad)
    then
    
        bContain=true
        
    end

end

    if(bContain)
    then
        local idx=0
        for i=0,table.getSize ( this.tJoypadMapping ( ) )-1
        do
            if(table.getAt ( this.tJoypadMapping ( ),i )==nJoypad)
            then
                idx=i
                break
            end
        end
        
        if(this.nTurn ( )>0 and nButton==1)
        then
              
            local nUserID=table.getAt ( this.tUsers ( ),idx)
            local hUser=application.getUser ( nUserID )
            user.sendEvent ( hUser,"AIPlayer","onValidPhase" )
        else
           
         
            
            local nX,nY=this.computePlayerViewportToGlobalViewport (table.getAt ( this.tMouseX ( ),idx ),table.getAt ( this.tMouseY ( ),idx ),idx  )
            input.setVirtualMousePosition ( this.getUser ( ),nX,nY )
       
            input.setVirtualMouseButtonDown (this.getUser ( ), 0, true )
            
        
        end
       




    else
    
        table.add ( this.tMouseX ( ),0 )
        table.add ( this.tMouseY ( ),0 )
        table.add ( this.tMouseDeltaX ( ),0 )
        table.add ( this.tMouseDeltaY ( ),0 )
        
        this.bGameStarted ( true)
        
           
        local nUserID=table.getAt ( this.tUsers ( ),table.getSize (  this.tJoypadMapping ( )))
        local hUser=application.getUser ( nUserID )
        user.sendEvent ( hUser,"AIPlayer","onValidInputController" )
        table.add ( this.tJoypadMapping ( ),nJoypad )
        
    end
    
	
	
   
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
