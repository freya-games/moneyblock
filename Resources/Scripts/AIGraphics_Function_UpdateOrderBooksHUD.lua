--------------------------------------------------------------------------------
--  Function......... : UpdateOrderBooksHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateOrderBooksHUD ( )
--------------------------------------------------------------------------------

if(this.bUseDebugHUD ( ))then return end


local nMaxDisplay = 20
local hUser = this.getUser ( )

for i=0 , hashtable.getSize ( this.htPrefixOrderBookHUD ( ) )-1
do
    local sCryptoID = hashtable.getKeyAt ( this.htPrefixOrderBookHUD ( ), i )
    local sPrefixOrderBookHUD = hashtable.getAt ( this.htPrefixOrderBookHUD ( ), i )
    local hC = hud.getComponent ( hUser, sPrefixOrderBookHUD..".ContainerGlobal" )

    if(this.sCurMiningCryptoID ( ) == sCryptoID)then

        hud.setComponentVisible ( hC, false )
        sPrefixOrderBookHUD = "OrderBook"
    else
        hud.setComponentVisible ( hC, true )
    end

    local nTotalBuy = hashtable.get ( this.htBuyOrder ( ),sCryptoID)
    local nTotalSell = hashtable.get ( this.htSellOrder ( ),sCryptoID)

    if(not nTotalBuy)then nTotalBuy = 0 end
    if(not nTotalSell)then nTotalSell = 0 end

    --SET PROGRESS BAR


    local sActionTag = sPrefixOrderBookHUD..".SetProgressBuy"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nTotalBuy, math.clamp(nTotalBuy/nMaxDisplay,0,1) * 255 )

    sActionTag = sPrefixOrderBookHUD..".SetProgressSell"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nTotalSell, math.clamp(nTotalSell/nMaxDisplay,0,1) * 255 )

end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
