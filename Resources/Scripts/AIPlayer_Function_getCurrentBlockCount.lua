--------------------------------------------------------------------------------
--  Function......... : getCurrentBlockCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.getCurrentBlockCount (sCryptoID )
--------------------------------------------------------------------------------
	
    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
	for iR=0, nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
        if(sID ==sCryptoID)
        then
            
       
            return hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".BlockChain.ChildCount")
        end
    end
   
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
