--------------------------------------------------------------------------------
--  Function......... : EnableMarketCardHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableMarketCardHUD ( bEnable)
--------------------------------------------------------------------------------
	
    if(this.bUseDebugHUD ( ))then return end
    
	if(bEnable)then
    
        hud.callAction ( this.getUser ( ), this.sPrefixMarketCardHUD ( ).."Holder.Show" )
	
    else
        hud.callAction ( this.getUser ( ), this.sPrefixMarketCardHUD ( ).."Holder.Hide" )
    
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
