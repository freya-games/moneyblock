--------------------------------------------------------------------------------
--  Function......... : GetMaxSellAmount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetMaxSellAmount ( sResID )
--------------------------------------------------------------------------------
	
for i=0, hashtable.getSize ( this.htResourceCount ( ) )-1
do
    local sID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
    if(sID == sResID)
    then

        return hashtable.get (this.htResourceCount ( ), sResID )

    end
end

return 0
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
