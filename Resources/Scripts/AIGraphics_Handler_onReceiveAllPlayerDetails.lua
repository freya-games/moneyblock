--------------------------------------------------------------------------------
--  Handler.......... : onReceiveAllPlayerDetails
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onReceiveAllPlayerDetails ( sAllNames, sAllPictures, sDelimiter )
--------------------------------------------------------------------------------
	
	string.explode ( sAllNames, this.tPlayersNames ( ), sDelimiter )
	string.explode ( sAllPictures, this.tPlayersPictureID ( ), sDelimiter )
    
    this.CreatePlayerDetailsHUD ( )
    
    --SET USER PICTURE
    if(not this.bUseDebugHUD ( ))then
        local nUserID = user.getID ( this.getUser ( ) )
        local nPicture = table.getAt ( this.tPlayersPictureID ( ), this.GetUserIndex(nUserID) )
        local hC = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.SpriteUser" )
        if(hC)then
            hud.setComponentBackgroundImage ( hC, "Picture"..nPicture )
        end
    end
    --LOG
--     for i=0 , table.getSize ( this.tPlayersNames ( )) -1
--     do
--     	log.message ( " AI Player : Details Player "..i.." received "..table.getAt ( this.tPlayersNames ( ) ,i).." and "..table.getAt ( this.tPlayersPictureID ( ) ,i) )
--     end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
