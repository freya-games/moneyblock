--------------------------------------------------------------------------------
--  Function......... : PickRandomMarketCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.PickRandomMarketCard ( )
--------------------------------------------------------------------------------

local nPickIndex = -1

if(table.getSize ( this.tMarketCardDrawIndex ( ) )<=0)
then
     this.NewMarketDrawCard ( )
end

if(table.getSize ( this.tMarketCardDrawIndex ( ) )>0)
then

    local nIndex=math.roundToNearestInteger (  math.random (0, table.getSize ( this.tMarketCardDrawIndex ( ) )-1 ))
    nPickIndex=table.getAt(this.tMarketCardDrawIndex ( ),nIndex)
	
    table.removeAt ( this.tMarketCardDrawIndex ( ),nIndex )
end
    
return nPickIndex
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
