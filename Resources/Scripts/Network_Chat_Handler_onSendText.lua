--------------------------------------------------------------------------------
--  Handler.......... : onSendText
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Chat.onSendText ( sText )
--------------------------------------------------------------------------------
    
    if ( string.isEmpty ( sText ) )
    then
        local hEditChat = hud.getComponent ( this.getUser ( ), "Network_Chat.EditChat" )
        if ( hEditChat )
        then
            sText = hud.getEditText ( hEditChat )
            hud.setEditText ( hEditChat, "" )
        end
    end
    
    if ( not string.isEmpty ( sText ) )
    then  
        --send to other players
        user.sendEvent ( this.getUser ( ), "Network_Connection", "onBroadcastChat", sText )
	end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
