--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalBuyOrderCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetTotalBuyOrderCallback (sCryptoID ,nBuyAmount  )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htBuyOrder ( ),sCryptoID ))
    then
        hashtable.set ( this.htBuyOrder ( ),sCryptoID, nBuyAmount )
    else
        hashtable.add ( this.htBuyOrder ( ),sCryptoID,nBuyAmount )
    end

    this.UpdateMarketHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
