--------------------------------------------------------------------------------
--  Function......... : sendInitToAllPlayers
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendInitToAllPlayers ( )
--------------------------------------------------------------------------------

    this.WaitingPlayerInitialized ( )

    local sUsers = ""
    local sDelimiter = "_"
    for i=0 , table.getSize ( this.tUsers ( ) )-1
    do
        sUsers = sUsers..table.getAt ( this.tUsers ( ), i )..sDelimiter
    end


    for i=0, table.getSize ( this.tUsers ( ) )-1
    do

        local nUserID=table.getAt ( this.tUsers ( ),i )
        local hUser=application.getUser ( nUserID )
        user.sendEvent ( hUser,"AIPlayer","onStartGame", sUsers, sDelimiter ,i+1)
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
