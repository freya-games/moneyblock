--------------------------------------------------------------------------------
--  Function......... : Set3DAnchorPosition
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.Set3DAnchorPosition ( nMinScreenX, nMinScreenY, nMaxScreenX, nMaxScreenY )
--------------------------------------------------------------------------------
	
		
    local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
    local hCam = user.getActiveCamera ( hUser )
    local nCamX, nCamy, nCamZ = object.getTranslation ( hCam, object.kGlobalSpace )

    

    --GET SPAWN POSITION
    local hMinDummy = scene.getTaggedObject ( hScene, "Min" )
    local hMaxDummy = scene.getTaggedObject ( hScene, "Max" )
    
    if(not hMinDummy )then
        hMinDummy = scene.createRuntimeObject ( hScene, "" )
        scene.setObjectTag ( hScene,hMinDummy, "Min" )
    end
    if(not hMaxDummy )then
        hMaxDummy = scene.createRuntimeObject ( hScene, "" )
        scene.setObjectTag ( hScene,hMaxDummy, "Max" )
    end
    
    
    local nMinX, nMinY, nMinZ = camera.unprojectPoint ( hCam, nMinScreenX, nMinScreenY, 1 )
    local nMaxX, nMaxY, nMaxZ = camera.unprojectPoint ( hCam, nMaxScreenX, nMaxScreenY, 1 )
    nMinX, nMinY, nMinZ = math.vectorNormalize ( nMinX, nMinY, nMinZ )
    nMaxX, nMaxY, nMaxZ = math.vectorNormalize ( nMaxX, nMaxY, nMaxZ )
    
    
    local hHitObject, nHitDist = scene.getFirstHitCollider ( hScene, nCamX, nCamy, nCamZ, nMinX, nMinY, nMinZ, 100 )
    local hHitObject2, nHitDist2 = scene.getFirstHitCollider ( hScene, nCamX, nCamy, nCamZ, nMaxX, nMaxY, nMaxZ, 100 )
    
    
    if(hHitObject)then
        nMinX, nMinY, nMinZ = math.vectorSetLength ( nMinX, nMinY, nMinZ, nHitDist  )
        nMinX, nMinY, nMinZ = math.vectorAdd ( nMinX, nMinY, nMinZ, nCamX, nCamy, nCamZ  )
    end
    if(hHitObject2)then
        nMaxX, nMaxY, nMaxZ = math.vectorSetLength ( nMaxX, nMaxY, nMaxZ, nHitDist2  )
        nMaxX, nMaxY, nMaxZ = math.vectorAdd ( nMaxX, nMaxY, nMaxZ, nCamX, nCamy, nCamZ  )
    end
    
    object.setTranslation ( hMinDummy, nMinX, nMinY, nMinZ, object.kGlobalSpace )
    object.setTranslation ( hMaxDummy, nMaxX, nMaxY, nMaxZ, object.kGlobalSpace )
    
    return nMinX, nMinY, nMinZ, nMaxX, nMaxY, nMaxZ
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
