--------------------------------------------------------------------------------
--  State............ : DownloadingXMLConfigFiles
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.DownloadingXMLConfigFiles_onEnter ( )
--------------------------------------------------------------------------------
local sPath=application.getPackDirectory ( )
    if(system.getClientType ( )==system.kClientTypeEditor)
    then
        sPath="."
    end
    log.message ( "file://"..sPath.."./MoneyBlockConfig.xml"  )
    xml.receive ( this.xMoneyBlockConfig ( ),"file://"..sPath.."/MoneyBlockConfig.xml" )
    xml.receive ( this.xMarketCards ( ),"file://"..sPath.."/MarketCards.xml" )
    xml.receive ( this.xActionCards ( ),"file://"..sPath.."/ActionCards.xml" )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
