--------------------------------------------------------------------------------
--  Function......... : ParseMoneyBlockConfig
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.ParseMoneyBlockConfig ( )
--------------------------------------------------------------------------------

 if(this.xMoneyBlockConfig ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xMoneyBlockConfig ( ))

    if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.RecurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htGameConfig ( ),ht)

    end


end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
