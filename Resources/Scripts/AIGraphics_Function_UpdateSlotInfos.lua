--------------------------------------------------------------------------------
--  Function......... : UpdateSlotInfos
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateSlotInfos ( )
--------------------------------------------------------------------------------

if(this.bUseDebugHUD ( ))then return end


local hUser = this.getUser ( )
local nMaxDisplay = 20

--UPDATE ORDER BOOKS
for i=0 , hashtable.getSize ( this.htPrefixSlotOrderBookHUD ( ) )-1
do
    local sCryptoID = hashtable.getKeyAt ( this.htPrefixSlotOrderBookHUD ( ), i )
    local sPrefixOrderBookHUD = hashtable.getAt ( this.htPrefixSlotOrderBookHUD ( ), i )


    local nTotalBuy = hashtable.get ( this.htBuyOrder ( ),sCryptoID)
    local nTotalSell = hashtable.get ( this.htSellOrder ( ),sCryptoID)

    if(not nTotalBuy)then nTotalBuy = 0 end
    if(not nTotalSell)then nTotalSell = 0 end

    --SET PROGRESS BAR
    local sActionTag = sPrefixOrderBookHUD..".SetProgressBuy"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nTotalBuy, math.clamp(nTotalBuy/nMaxDisplay,0,1) * 255 )

    sActionTag = sPrefixOrderBookHUD..".SetProgressSell"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nTotalSell, math.clamp(nTotalSell/nMaxDisplay,0,1) * 255 )

end

--UPDATE MINING RESOURCE
for i=0 , hashtable.getSize ( this.htPrefixSlotMiningResourceHUD ( ) ) -1
do
    local t = table.newInstance ( )
    local sKey = hashtable.getKeyAt ( this.htPrefixSlotMiningResourceHUD ( ), i )
    local sPrefix = hashtable.getAt ( this.htPrefixSlotMiningResourceHUD ( ), i )
    string.explode ( sKey, t, "." )

    local sCryptoID = table.getFirst ( t )
    local sResID = table.getAt ( t, 1 )

    local nMax = this.GetRequiredResourceToMine ( sResID, sCryptoID )
    --local nCur = hashtable.get ( this.htMining ( ), sKey )
    local nCur = hashtable.get ( this.htTotalMining ( ), sKey )

    if(not nCur)then nCur = 0 end

    local sActionTag = sPrefix..".SetResourceInfos"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nCur, "/"..nMax )

end



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
