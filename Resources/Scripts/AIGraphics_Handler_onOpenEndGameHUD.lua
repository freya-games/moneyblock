--------------------------------------------------------------------------------
--  Handler.......... : onOpenEndGameHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onOpenEndGameHUD ( sTitle,sLeaderBoard)
--------------------------------------------------------------------------------

if(not this.bUseDebugHUD ( ))
then

    this.UpdateEndTurnHUD ( )
    this.EnableEndTurnHUD ( true, sTitle )

    local hC = hud.getComponent ( this.getUser ( ), this.sPrefixEndTurnHUD ( )..".ContainerRestart")
    if(hC)then hud.setComponentVisible ( hC, true ) end


--------------------------------------DEBUG HUD------------------------------------
else
    if(sTitle)
    then

         local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ContainerResources" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ContainerResources" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.MarketCardContainer" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.Mining" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.OpenMining" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.PhaseContainer" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ValidPhase" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ContainerAction" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.Dialog" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ContainerCash" )
        hud.setComponentVisible ( hC,false )
        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.ContainerTotal" )
        hud.setComponentVisible ( hC,false )

        hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.Market" )
        hud.setComponentVisible ( hC,false )


        hud.setLabelText ( hud.getComponent ( this.getUser ( ),"PlayerHUD.EndGameTitle" ),sTitle )
        hud.setLabelText ( hud.getComponent ( this.getUser ( ),"PlayerHUD.LeaderBoard" ),sLeaderBoard )
        hud.setComponentVisible (  hud.getComponent ( this.getUser ( ),"PlayerHUD.EndGameContainer" ),true )

    end
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
