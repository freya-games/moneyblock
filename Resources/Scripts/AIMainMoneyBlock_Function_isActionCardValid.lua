--------------------------------------------------------------------------------
--  Function......... : isActionCardValid
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.isActionCardValid (nUserID,nCardID )
--------------------------------------------------------------------------------
    local nPlayerIndex=this.getPlayerIndex ( nUserID )

if(nPlayerIndex)then
  local nCardCount=hashtable.get ( this.htRuntimeData ( ),"Player.".. nPlayerIndex..".ActionCards.Count")

    for i=0,nCardCount-1
    do
        if(hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards."..i)==nCardID)
        then
            return true
        end


    end
end

    return false

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
