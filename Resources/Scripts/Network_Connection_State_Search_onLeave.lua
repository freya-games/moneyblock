--------------------------------------------------------------------------------
--  State............ : Search
--  Author........... : 
--  Description...... : Choose the fisrt server found on LAN
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.Search_onLeave ( )
--------------------------------------------------------------------------------
	        
        if ( network.getServerCount ( ) == 0 )
        then
            log.message ( "no server found" )
            
        else 
            log.message ( network.getServerCount ( ) .. " server(s) found.")
            
            --override the default server by the first LAN server
            this.sServerIP ( network.getServerNameAt ( 0 ) )               
            
            --and connect to it immedatly
            this.Connection ( )
        end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
