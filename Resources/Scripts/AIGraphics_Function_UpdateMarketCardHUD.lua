--------------------------------------------------------------------------------
--  Function......... : UpdateMarketCardHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMarketCardHUD ( )
--------------------------------------------------------------------------------
	
local hComponent= hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerMarketCards" )
local hUser = this.getUser ( )
local nbMarketCard = 1

--CREATE CARDS HUD
if(application.isResourceReferenced ( "Card", application.kResourceTypeHUD ))then
    if(hComponent)then
        while (table.getSize (this.tPrefixMarketCardHUD ( )) < nbMarketCard )
        do
            
            local sPrefix = "MarketCard_"..table.getSize (this.tPrefixMarketCardHUD ( ))
            if(hud.newTemplateInstance ( hUser, "Card", sPrefix ))then
            
                table.add ( this.tPrefixMarketCardHUD ( ), sPrefix )
                local hCard = hud.getComponent ( hUser, sPrefix..".ContainerGlobal" )
                if(hCard)then
                    hud.setComponentContainer ( hCard, hComponent )
                end
            else
                break
            end
        end
    end
else
    log.warning ( "Card HUD not found" )
end


--UPDATE CARD TRANSFORM
for i=0, nbMarketCard-1
do

    local nCardID= this.nMarketCardID ( )
    
    
    if(table.getSize ( this.tPrefixMarketCardHUD ( ) )>i)then
    
        local sPrefix=table.getAt ( this.tPrefixMarketCardHUD ( ),i )
            
            
            --SET TRANSFORM
            local nSizeX =  100 / 2 
            local nSizeY =  nSizeX / this.nRatioCardHUD ( )  
            
            local sAction = sPrefix..".SetInfos"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction ,  this.GetMarketCardInfo (nCardID,"Name"), this.GetMarketCardInfo (nCardID,"Description"), this.GetMarketCardInfo (nCardID,"Img") )
            
            local sAction = sPrefix..".SetPosition"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction , 50, 50 )
            
            local sAction = sPrefix..".SetSize"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction ,  nSizeX, nSizeY  )
        end
end


	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
