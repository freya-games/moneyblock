--------------------------------------------------------------------------------
--  Handler.......... : onDisplayPlayedCardDetail
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onDisplayPlayedCardDetail ( sTag )
--------------------------------------------------------------------------------
	
    for i=0 ,  hashtable.getSize ( this.htSlotPreviewCardID ( ))-1
    do
    	local sPrefix = hashtable.getKeyAt ( this.htSlotPreviewCardID ( ), i )
        local sTempTag = sPrefix..".PreviewCard_ContainerGlobal"
        
        if(sTempTag == sTag)then
        
            local nCardID = hashtable.get ( this.htSlotPreviewCardID ( ), sPrefix )
            if(nCardID)then
                this.EnableCardDetailHUD ( nCardID, false  )
            end
        end
    end
    
    
    
    
   
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
