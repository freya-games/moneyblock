--------------------------------------------------------------------------------
--  Function......... : changeGameResourcePrice
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.changeGameResourcePrice (sResID,nValueToAdd)
--------------------------------------------------------------------------------
	
    if(hashtable.contains (this.htRuntimeData ( ),"Game.Resources."..sResID..".Price" ))
    then
        local nV=  hashtable.get ( this.htRuntimeData ( ),"Game.Resources."..sResID..".Price")
        local nVMini=  hashtable.get ( this.htRuntimeData ( ),"Game.Resources."..sResID..".PriceMini")
        if(not nVMini)then nVMini = 0 end
        
        local nNewValue=nV+nValueToAdd
        if(nNewValue<nVMini)
        then
            nNewValue=nVMini
        end
        hashtable.set ( this.htRuntimeData ( ),"Game.Resources."..sResID..".Price",nNewValue)
        
        for i =0, this.nPlayerCount ( )-1
        do
      
            user.sendEvent ( application.getUser (  table.getAt ( this.tUsers ( ),i )),"AIPlayer", "onGetResourcePriceCallback",sResID,this.getResourcePrice ( sResID))

        
        end
        
    else
        log.error ( "AIMainMoneyBlock.changeGameResourcePrice "..sResID.." does not exist" )
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
