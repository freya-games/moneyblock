--------------------------------------------------------------------------------
--  Function......... : ApplyAllActionCardsEffect
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ApplyAllActionCardsEffect (bEndTurn )
--------------------------------------------------------------------------------

    for nPlayerIndex=0,this.nPlayerCount ( )-1
    do

        local nCardID= hashtable.get(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.ID" )
        local sCryptoID =  hashtable.get(this.htPlayerActionCards ( ),"Player."..nPlayerIndex..".ActionCard.CryptoTarget" )

        if(nCardID and sCryptoID)then

            this.ApplyActionCardEffect (nCardID,sCryptoID,nPlayerIndex,bEndTurn )
        end
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
