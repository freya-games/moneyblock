--------------------------------------------------------------------------------
--  Handler.......... : onGetResourcePriceCallback
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetResourcePriceCallback ( sResID,nPrice  )
--------------------------------------------------------------------------------

    if(hashtable.contains ( this.htResourcePrice ( ),sResID ))
    then
        --STORE OLD VALUE
        local oldValue = hashtable.get ( this.htResourcePrice ( ),sResID)
        hashtable.set ( this.htPrevResourcePrice ( ),sResID, oldValue )

        hashtable.set ( this.htResourcePrice ( ),sResID, nPrice )
    else
        hashtable.add ( this.htResourcePrice ( ),sResID,nPrice )
        hashtable.add ( this.htPrevResourcePrice ( ),sResID,nPrice )

        --INIT HUD VALUES
        this.UpdateEndTurnHUD ( )
    end

    this.UpdateHUD (  )
    this.UpdateMarketHUD (  )


    if(this.IsHudVisible ( this.sPrefixEndTurnHUD ( )..".ContainerGlobal" ))then
       this.UpdateEndTurnHUD ( )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
