--------------------------------------------------------------------------------
--  Handler.......... : onBroadcastGoto
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onBroadcastGoto ( x, y, z )
--------------------------------------------------------------------------------
	
    local hScene  = application.getCurrentUserScene ( )
    if ( hScene ) 
    then
        local nLocalUserID = user.getID ( this.getUser ( ) )   
        --send the position to all users in the scene (including the local user)
        scene.sendEventToAllUsers ( hScene, "Network_Connection", "onReceiveGoto", nLocalUserID, x, y, z )
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
