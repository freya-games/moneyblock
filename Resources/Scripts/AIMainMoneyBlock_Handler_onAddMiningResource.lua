--------------------------------------------------------------------------------
--  Handler.......... : onAddMiningResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onAddMiningResource (nUserID,sCryptoID,sResourceID,nAmount,sCallback  ) 
--------------------------------------------------------------------------------
	
    
if(nUserID)
then


    local bCanAdd=false
     
    local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )
    
    local bValid=this.isMiningResourceValid ( sCryptoID,sResourceID,nCB )
    if(bValid)
    then
        local nPlayerResourceCount=this.getPlayerResourceCount (  nUserID, sResourceID)
        if(nPlayerResourceCount<nAmount)
        then
            bCanAdd=false
        else
        
            local nPlayerIndex=this.getPlayerIndex ( nUserID )
         
            this.changePlayerMiningResource (nPlayerIndex, sCryptoID,sResourceID,nAmount )
            if(this.getResourceType ( sResourceID )=="ENERGY")
            then
                this.ChangePlayerResource ( nPlayerIndex,sResourceID,-1*nAmount )
            end
             if(this.getResourceType ( sResourceID )=="MINER")
            then
                this.ChangePlayerResource ( nPlayerIndex,sResourceID,-1*nAmount )
            end
            
            bCanAdd=true
        end

         
    
       
    end
    
    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", sCallback,sCryptoID,sResourceID,nAmount,bCanAdd)
else
     log.error ( "AIMainMoneyBlock.onBuy UserID Error" )
end   
    
  
    
    
    
    

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
