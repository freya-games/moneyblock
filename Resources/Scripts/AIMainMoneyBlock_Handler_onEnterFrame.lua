--------------------------------------------------------------------------------
--  Handler.......... : onEnterFrame
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onEnterFrame (  )
--------------------------------------------------------------------------------
	if(this.bGameStarted ( ))
    then
    
    
    local dt =application.getAverageFrameTime ( )
    for i=0,table.getSize ( this.tJoypadMapping ( ) )-1
    do      
        local nPad=table.getAt ( this.tJoypadMapping ( ),i )
        table.setAt (this.tMouseX ( ) ,i,math.clamp ( table.getAt (this.tMouseX ( ),i)+dt*table.getAt (this.tMouseDeltaX ( ),i),-1,1) )
        table.setAt (this.tMouseY ( ) ,i,math.clamp ( table.getAt (this.tMouseY ( ),i)+dt*table.getAt (this.tMouseDeltaY ( ),i),-1,1) )
        
        local nUserID=table.getAt ( this.tUsers ( ),i )
        local hUser=application.getUser ( nUserID )
        user.sendEvent ( hUser,"AIPlayer","onMoveVirtualMouse" ,table.getAt ( this.tMouseX ( ),i ),table.getAt ( this.tMouseY ( ),i ))
       

      --  log.message (table.getAt (this.tMouseX ( ),i) )
    end
 end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
