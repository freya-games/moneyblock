--------------------------------------------------------------------------------
--  Function......... : GetResourceColor
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetResourceColor ( sResID, bEmissiveColor )
--------------------------------------------------------------------------------

    local sColor = nil

    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    for i=0, nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        if(sID ==sResID)
        then
            if(bEmissiveColor)then
                sColor = hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ColorEmissive")
            else
                sColor = hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Color")

            end
            break
        end

    end


    --PARSE STRING
    if(sColor)then
        local tColor = table.newInstance ( )
        if(string.explode ( sColor, tColor, "_") )then

            local r = string.toNumber ( table.getAt ( tColor, 0 ) )
            local g = string.toNumber ( table.getAt ( tColor, 1 ) )
            local b = string.toNumber ( table.getAt ( tColor, 2 ) )

            return r, g, b
        end
    end

    --DEFAULT VALUE
    return 80,80,80

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
