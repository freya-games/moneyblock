--------------------------------------------------------------------------------
--  Function......... : computePlayerViewportToGlobalViewport
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.computePlayerViewportToGlobalViewport ( nX,nY,nPlayerIndex)
--------------------------------------------------------------------------------
	



if(this.nPlayerCount ( )==1)
then
      

        return nX,nY
 

else

    local nScreenLeft, nScreenBottom, nScreenWidth, nScreenHeight = user.getViewport ( this.getUser ( ) ) --user 0 has the full viewport     
            
    local nLeft=hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".L" ) 
    local nBottom=  hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".B"  ) 
    local nWidth=hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".W"  ) 
    local nHeight=hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".H"  )  
    
    
    local nGlobalX=( ((nX+1)/2*nWidth+nLeft)/nScreenWidth+nScreenLeft)*2-1
    local nGlobalY=( ((nY+1)/2*nHeight+nBottom)/nScreenHeight+nScreenBottom)*2-1
                        
    return nGlobalX,nGlobalY
end
    
  
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
