--------------------------------------------------------------------------------
--  Function......... : changeGameResourceCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.changeGameResourceCount (sResID,nValueToAdd)
--------------------------------------------------------------------------------
	
    if(hashtable.contains (this.htRuntimeData ( ),"Game.Resources."..sResID..".Count" ))
    then
        local nV=  hashtable.get ( this.htRuntimeData ( ),"Game.Resources."..sResID..".Count")
        local nNewValue=nV+nValueToAdd
        if(nNewValue<0)
        then
            nNewValue=0
        end
        
        hashtable.set ( this.htRuntimeData ( ),"Game.Resources."..sResID..".Count", nNewValue)
  
    else
        log.error ( "AIMainMoneyBlock.changeGameResourceCount "..sResID.." does not exist" )
    end
   
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
