--------------------------------------------------------------------------------
--  Function......... : Alea
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.Alea ( )
--------------------------------------------------------------------------------
    local nExpectation=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Market.Alea.Expectation")
    local nDeviation=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Market.Alea.Deviation")
    return math.gaussianRandom ( nExpectation,nDeviation )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
