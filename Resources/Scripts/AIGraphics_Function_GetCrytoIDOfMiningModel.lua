--------------------------------------------------------------------------------
--  Function......... : GetCrytoIDOfMiningModel
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetCrytoIDOfMiningModel ( hMiningModel )
--------------------------------------------------------------------------------
	
if(hMiningModel)then    

   
    
    for i=0 , hashtable.getSize ( this.htMiningModel ( ) ) -1
    do
        local hObj = hashtable.getAt ( this.htMiningModel ( ), i )
        if(hObj)then
            if(object.isEqualTo ( hObj, hMiningModel ))then
                return hashtable.getKeyAt ( this.htMiningModel ( ), i )
            end
        end
    end
end

return ""
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
