--------------------------------------------------------------------------------
--  Function......... : ParseActionCards
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ParseActionCards ( )
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

if(this.xActionCards ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xActionCards ( ) )


     local sVersion=    xml.getAttributeValue ( xml.getElementAttributeWithName ( hRootElement,"version") )

    if(sVersion~= application.getCurrentUserEnvironmentVariable ( "version" ))
    then
        log.message ( "ActionCardsXML Bad Version Waiting for version "..application.getCurrentUserEnvironmentVariable ( "version" ).." found "..sVersion)
        application.quit ( )
    end

     if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.recurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htActionCards ( ),ht)

    end


    hashtable.add ( this.htRuntimeData ( ),"TotalActionCards", hashtable.get ( this.htActionCards ( ),"ActionCards.ChildCount"))

this.NewActionDrawCard ( )

end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
