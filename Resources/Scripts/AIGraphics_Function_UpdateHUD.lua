--------------------------------------------------------------------------------
--  Function......... : UpdateHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateHUD ( )
--------------------------------------------------------------------------------


if(not this.bUseDebugHUD ( ))then


-- UPDATE RESOURCE AMOUNT AND PRICE
local nTotalCryptoValue = 0
local nTotalCryptoCount = 0

for i=0, hashtable.getSize ( this.htResourcePrice ( ) ) -1
do
    local sID=hashtable.getKeyAt ( this.htResourcePrice ( ),i )
    local nCount=hashtable.getAt ( this.htResourceCount ( ),i )
    if(not nCount)then nCount = 0 end
	local nPrice=hashtable.get( this.htResourcePrice ( ),sID )
    
    local sType=this.GetResourceType ( sID)
    if(sType=="CRYPTO" )then
        this.UpdateCryptoInfos ( sID, nCount, nPrice )
        nTotalCryptoCount = nTotalCryptoCount + nCount
        nTotalCryptoValue = nTotalCryptoValue + nCount * nPrice
        
    else
        this.UpdateMiningResourceInfos ( sID, nCount, nPrice )
    end
end
this.UpdateTotalPortfolio ( nTotalCryptoCount, nTotalCryptoValue)

this.UpdatePendingHUD()

---------------------------------DEBUG HUD ---------------------------------
else

    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.Crypto" )
    hud.removeListAllItems ( hComponent )
    table.empty ( this.tCryptoHUD ( ) )	
        
    local hComponentMiner = hud.getComponent ( this.getUser ( ), "PlayerHUD.Miner" )
    hud.removeListAllItems ( hComponentMiner )
    table.empty ( this.tMinerHUD ( ) )

    local hComponentEnergy = hud.getComponent ( this.getUser ( ), "PlayerHUD.Energy" )
    hud.removeListAllItems ( hComponentEnergy )
    table.empty ( this.tEnergyHUD ( ) )


        
    for i=0,hashtable.getSize ( this.htResourceCount ( ) )-1
    do

        local sID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
        local nCount=hashtable.getAt ( this.htResourceCount ( ),i )
        if(not nCount)
        then
            nCount=0
        end
            local sType=this.GetResourceType ( sID)
            if(sType=="CRYPTO" )
            then
                local nIndex=hud.addListItem ( hComponent,this.GetResourceName (  sID) )
                hud.setListItemTextAt ( hComponent, nIndex, 1,""..nCount )
            
                table.add (  this.tCryptoHUD ( ),sID)
                local nPrice=hashtable.get( this.htResourcePrice ( ),sID )
        
                if(nPrice)
                then
                      hud.setListItemTextAt ( hComponent, nIndex, 2,""..nPrice..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")  )
                end
                
                if(nPrice)
                then
                      hud.setListItemTextAt ( hComponent, nIndex, 3,""..(nPrice*nCount)..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")  )
       
                end
                
                local nB=0
                local nS=0
                
                local nV=hashtable.get ( this.htPendingOrders ( ),sID..".Buy" )
                if(nV)
                then
                    nB=nV
                end
                
                nV=hashtable.get ( this.htPendingOrders ( ),sID..".Sell" )
                if(nV)
                then
                    nS=nV
                end
                hud.setListItemTextAt ( hComponent, nIndex, 3,"B "..nB.."\n".."S "..nS  )
            end
            
          
            if(sType=="MINER")
            then
                local nIndex=hud.addListItem ( hComponentMiner,this.GetResourceName (  sID) )
                
                
                local nTotalMining=0
                for i=0,hashtable.getSize ( this.htMining ( ) )-1
                do

                    local sS=hashtable.getKeyAt ( this.htMining ( ),i )
                    local t=table.newInstance ( )
                    string.explode ( sS,t,"." )
                    
                    local sMiningCryptoID=table.getAt ( t,0 )
                    local sMiningResourceID=table.getAt ( t,1 )
                    local nMiningResourceCount= hashtable.getAt ( this.htMining ( ),i )
                    if(sMiningResourceID==sID)
                    then
                        nTotalMining=nTotalMining+nMiningResourceCount
                    end
                    
                end
                hud.setListItemTextAt ( hComponentMiner, nIndex, 1,""..nCount-nTotalMining )
                table.add (  this.tMinerHUD ( ),sID)
                  local nPrice=hashtable.get( this.htResourcePrice ( ),sID )
        
                if(nPrice)
                then
                      hud.setListItemTextAt (  hComponentMiner, nIndex, 2,""..nPrice..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")  )
            
                end
                
            end
            
             if(sType=="ENERGY")
            then
                local nIndex=hud.addListItem ( hComponentEnergy,this.GetResourceName (  sID) )
                
                
                  local nTotalMining=0
                for i=0,hashtable.getSize ( this.htMining ( ) )-1
                do

                    local sS=hashtable.getKeyAt ( this.htMining ( ),i )
                    local t=table.newInstance ( )
                    string.explode ( sS,t,"." )
                    
                    local sMiningCryptoID=table.getAt ( t,0 )
                    local sMiningResourceID=table.getAt ( t,1 )
                    local nMiningResourceCount= hashtable.getAt ( this.htMining ( ),i )
                    if(sMiningResourceID==sID)
                    then
                        nTotalMining=nTotalMining+nMiningResourceCount
                    end
                    
                end
                
                hud.setListItemTextAt ( hComponentEnergy, nIndex, 1,""..nCount-nTotalMining )
                table.add (  this.tEnergyHUD ( ),sID)
                 local nPrice=hashtable.get( this.htResourcePrice ( ),sID )
        
                if(nPrice)
                then
                      hud.setListItemTextAt (  hComponentEnergy, nIndex, 2,""..nPrice..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")  )
            
                end
            end
        
    end

    this.UpdateMiningHUD ( )
    this.UpdateTotal ( )
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
