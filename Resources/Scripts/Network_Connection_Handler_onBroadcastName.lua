--------------------------------------------------------------------------------
--  Handler.......... : onBroadcastName
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onBroadcastName (  )
--------------------------------------------------------------------------------
	
    local hScene  = application.getCurrentUserScene ( )
    if ( hScene ) 
    then
        local nLocalUserID = user.getID ( this.getUser ( ) )   
        --send the position to all users in the scene (including the local user)
        scene.sendEventToAllUsers ( hScene, "Network_Connection", "onReceiveName", nLocalUserID, this.sPlayerName ( ) )
    end
   
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
