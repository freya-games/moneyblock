--------------------------------------------------------------------------------
--  Function......... : AddMiningResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.AddMiningResource ( sCryptoID,sResID,nAmount)
--------------------------------------------------------------------------------

local nBlock=hashtable.get ( this.htCryptoCurrentBlock ( ),sCryptoID )
if(nBlock)
then
    local currentResCount=hashtable.get ( this.htResourceCount ( ),sResID)
        if(nAmount> currentResCount)
        then
               --this.openDialogBox ("Cannot Add, not enough resources : "..nAmount.." "..this.getName ( sResID ).." to mine "..this.getName ( sCryptoID ) )
        else
        if(hashtable.contains (this.htMining ( ),sCryptoID.."."..sResID  ))
        then

            hashtable.set (this.htMining ( ),sCryptoID.."."..sResID,nAmount  )
        else
            hashtable.add (this.htMining ( ),sCryptoID.."."..sResID ,nAmount )

        end
        
            --this.openDialogBox (nAmount.." "..this.getName ( sResID ).." added to mine "..this.getName ( sCryptoID ) )
            
            user.sendEvent(this.getUser ( ), this.sAIGraphics ( ), "onAddMiningResourceCallback", sCryptoID,sResID,nAmount )
                   
        end
end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
