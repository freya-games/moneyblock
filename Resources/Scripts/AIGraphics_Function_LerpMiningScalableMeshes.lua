--------------------------------------------------------------------------------
--  Function......... : LerpMiningScalableMeshes
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.LerpMiningScalableMeshes ( )
--------------------------------------------------------------------------------
	
	local nTime = application.getTotalFrameTime ( )
    local nLerpTime = 1
    local nScaleMin = 0.4
    local nScaleMax = 0.55
    
    --GET VALUE
    local nCurTime = math.mod ( nTime, nLerpTime*2 )
    if(nCurTime> nLerpTime)then
        nCurTime = nLerpTime*2 - nCurTime
    end
    local nLerpValue = math.interpolate ( nScaleMin, nScaleMax, nCurTime )
    
    --SET VALUE
    for i=0 , hashtable.getSize ( this.htMiningScalableMesh ( ) ) -1
    do
    	local hObj = hashtable.getAt ( this.htMiningScalableMesh ( ), i )
        
        if(hObj)then
            object.setUniformScale ( hObj, nLerpValue )
        end
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
