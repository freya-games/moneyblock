--------------------------------------------------------------------------------
--  Function......... : sendPhaseToPlayers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendPhaseToPlayers ( )
--------------------------------------------------------------------------------
	
	for i=0, table.getSize ( this.tUsers ( ) )-1
    do
        
        local nUserID=table.getAt ( this.tUsers ( ),i )
        local hUser=application.getUser ( nUserID )
        user.sendEvent ( hUser,"AIPlayer","onReceivePhase" ,this.nTurn ( ),this.nPhase ( ))
       
        if(not  hashtable.get ( this.htValidPhase ( ),""..nUserID ))
        then
        
              user.sendEvent ( hUser,"AIPlayer", "onReceiveMessage","[Server] Time Limit for Current Phase Reached")
        end
     
    end
    for j=0,hashtable.getSize ( this.htValidPhase ( ))-1
    do 
       
        hashtable.set ( this.htValidPhase ( ), hashtable.getKeyAt (this.htValidPhase ( ),j  ) ,false)
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
