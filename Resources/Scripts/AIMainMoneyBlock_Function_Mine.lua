--------------------------------------------------------------------------------
--  Function......... : Mine
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.Mine ( )
--------------------------------------------------------------------------------
    local ht=hashtable.newInstance ( )
    local htBlockCondition=hashtable.newInstance ( )

    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    for iC=0, nCount-1
    do
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID")
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then
           for iR=0, nCount-1
            do
                local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                local sType=this.getResourceType ( sResID)
                if(sType=="MINER" or sType=="ENERGY")
                then
                    hashtable.add ( ht,sCryptoID.."."..sResID,0 )
                    hashtable.add ( htBlockCondition,sCryptoID.."."..sResID,0 )
                end
            end
        end
    end

    for nPlayerIndex=0, this.nPlayerCount ( )-1
    do


        for iC=0, nCount-1
        do
            local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID")
            if(this.getResourceType ( sCryptoID)=="CRYPTO")
            then
                for iR=0, nCount-1
                do
                    local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                    local sType=this.getResourceType ( sResID)
                    if(sType=="MINER" or sType=="ENERGY")
                    then
                        local nAmount= hashtable.get ( this.htMining ( ), "Player."..nPlayerIndex.."."..sCryptoID.."."..sResID )
                        if(nAmount)
                        then
                            hashtable.set (ht, sCryptoID.."."..sResID,hashtable.get ( ht,sCryptoID.."."..sResID )+nAmount )
                        end


                    end
                end
            end
        end

    end

--Action Cards
     for iC=0, nCount-1
        do
            local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID")
            if(this.getResourceType ( sCryptoID)=="CRYPTO")
            then
                for iR=0, nCount-1
                do
                    local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                    local sType=this.getResourceType ( sResID)
                    if(sType=="MINER" or sType=="ENERGY")
                    then
                        local nAmount= hashtable.get ( this.htMining ( ), sCryptoID.."."..sResID )
                        if(nAmount)
                        then
                            hashtable.set (ht, sCryptoID.."."..sResID,hashtable.get ( ht,sCryptoID.."."..sResID )+nAmount )
                        end


                    end
                end
            end
        end






    for iR=0, nCount-1
    do
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")

        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then
            local nResourceIndex=iR

             local nBlockChainCount =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".BlockCount"  )
             if(nBlockChainCount)
             then
                for i=0,nBlockChainCount-1
                do
                    local nOrder=i

                    local nCryptoCurrentBlock =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )
                    if(nOrder==nCryptoCurrentBlock)
                    then
                        local sValidation

                         local sTag
                        if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.ChildCount")>1)
                        then
                              sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block."..i
                        else
                              sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block"

                        end

                        local nResCount=hashtable.get ( this.htGameConfig ( ),sTag..".ChildCount")
                        if(nResCount>1)
                        then
                            for j=0,nResCount-1
                            do
                                local sResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".ID")
                                local nAmount=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".Amount")
                                hashtable.set ( htBlockCondition,sCryptoID.."."..sResID,nAmount )

                            end
                        end
                        if(nResCount==1)
                        then
                            local sResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.ID")
                            local nAmount=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.Amount")
                             hashtable.set (htBlockCondition, sCryptoID.."."..sResID,nAmount )
                        end

                    end

                end
            end
        end

    end

    local htCryptoValid=hashtable.newInstance ( )

    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    for iC=0, nCount-1
    do
        local sCryptoID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID")
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then
             hashtable.add (htCryptoValid,sCryptoID,true )
        end



    end

    for i=0,hashtable.getSize ( ht )-1
    do
        local nTotal=hashtable.getAt (ht,i  )
        local sKey=hashtable.getKeyAt (ht,i  )

        local nCond=hashtable.get (htBlockCondition, sKey )
        if(nCond>0)
        then
            if(nTotal<nCond)
            then
                local tR=table.newInstance ( )
                string.explode ( sKey,tR,"." )
                local sCryptoID = table.getAt ( tR,0 )

                 hashtable.set (htCryptoValid,sCryptoID,false )
            end
        end
    end

    table.empty ( this.tCurBlockMined ( ) )
    for i=0,hashtable.getSize ( htCryptoValid )-1
    do

            this.validBlock (hashtable.getKeyAt ( htCryptoValid,i ),hashtable.getAt ( htCryptoValid,i ) )




    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
