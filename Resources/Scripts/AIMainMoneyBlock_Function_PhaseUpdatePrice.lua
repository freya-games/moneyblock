--------------------------------------------------------------------------------
--  Function......... : PhaseUpdatePrice
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.PhaseUpdatePrice ( )
--------------------------------------------------------------------------------
	
 local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
   local sCryptoMarketCap=""
 if(this.bMarketCap ( ))
 then
 
    sCryptoMarketCap=this.getCryptoMoreCap ( )
 end

for i=0,nCount-1
do
  if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
    then
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        local bApplyMarketCard=(sCryptoMarketCap==sID or not this.bMarketCap ( ))
        this.updateCryptoPrice (sID,bApplyMarketCard  )
        if(this.bMarketCap ( ) and sCryptoMarketCap==sID)
        then
            this.sendMessageToAllUsers ( "[MARKET CARD] Market Cap Effect Applied To "..this.getResourceName (sCryptoMarketCap ))
        end
    end
end
  

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
