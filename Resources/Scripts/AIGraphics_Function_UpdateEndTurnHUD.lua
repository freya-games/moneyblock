--------------------------------------------------------------------------------
--  Function......... : UpdateEndTurnHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateEndTurnHUD ( )
--------------------------------------------------------------------------------

    local hUser = this.getUser ( )
    local nMaxDisplayPrice = 300

    --UPDATE GLOBAL TOKEN DISPLAY
    for nIndexCrypto=0, hashtable.getSize ( this.htPrefixEndTurnTokenHUD() ) -1
    do
        local sCryptoID = hashtable.getKeyAt ( this.htPrefixEndTurnTokenHUD(), nIndexCrypto  )
        local sPrefixHUD = hashtable.getAt ( this.htPrefixEndTurnTokenHUD(), nIndexCrypto  )

        local hPrice = hud.getComponent ( hUser, sPrefixHUD..".LabelTokenPrice" )
        local nPrice= hashtable.get( this.htResourcePrice ( ), sCryptoID )
        local nOldPrice= hashtable.get( this.htPrevResourcePrice ( ), sCryptoID )

        if(nPrice)then

            --UPDATE BAR
            local nValue = math.clamp ( nPrice/nMaxDisplayPrice,0,1) * 255
            local nLerpTime = 4000
            local sAction = sPrefixHUD..".SetProgressBar"
            if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
            hud.callAction ( hUser, sAction, nValue, nLerpTime )




            if(hPrice)then
               hud.setLabelText ( hPrice, ""..nPrice )

                --GET PRICE COLOR
                local r = 50
                local g = 50
                local b = 50

                if(nPrice>nOldPrice)then
                    r = 0
                    g = 127
                    b = 0
                elseif(nPrice<nOldPrice)then
                    r = 127
                    g = 0
                    b = 0
                end
                hud.setComponentForegroundColor ( hPrice, r, g, b, 255 )
            end
        end
    end





    --UPDATE SCORES
    local tRank= table.newInstance ( )

    for i= 0, hashtable.getSize ( this.htPrefixEndTurnPlayerHUD ( ) ) -1
    do

        local sPrefix = hashtable.getAt ( this.htPrefixEndTurnPlayerHUD ( ), i )

        --UPDATE PICTURE
        local sPicture = table.getAt ( this.tPlayersPictureID ( ), i )
        local hPicture = hud.getComponent ( hUser, sPrefix..".SpritePlayer" )
        if(hPicture and sPicture)then
            hud.setComponentBackgroundImage ( hPicture, "Picture"..sPicture )
        end
        local sName = table.getAt ( this.tPlayersNames ( ), i )
        local hName = hud.getComponent ( hUser, sPrefix..".LabelPlayerName" )
        if(sName and hName)then
            hud.setLabelText ( hName, sName )
        end


        local nScore = 0
        --local _nCash = 0
        --local nTokens = 0

        --if(i<table.getSize ( this.tPlayersTotalMoney ( ) ))then
            --_nCash = table.getAt ( this.tPlayersTotalMoney ( ), i )
        --end
        --if(i<table.getSize ( this.tPlayersTotalTokens ( ) ))then
            --nTokens = table.getAt ( this.tPlayersTotalTokens ( ), i )
        --end
        if(i<table.getSize ( this.tPlayersCurScores ( ) ))then
            nScore = table.getAt ( this.tPlayersCurScores ( ), i )
        end


        --UPDATE HUD SCORE
        -- reset time
        if(not this.bHasUserSeenLastScores ( ))then
            this.nTimeLerpEndTurnValues ( this.nTimeTotalLerpEndTurnValues ( ) )
        end

        --local sAction = sPrefix..".SetPlayerScore"
        --if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
        --hud.callAction ( hUser, sAction, "".._nCash, ""..nTokens    )



        --ORDER RANK
        local bInsert = false
        for j=0, table.getSize ( tRank )-1
        do
            local nUser = table.getAt ( tRank, j  )
            local nOldScore = 0
            if(j<table.getSize ( this.tPlayersCurScores ( ) ))then
            nOldScore = table.getAt ( this.tPlayersCurScores ( ), j )
            end

            if(nScore >nOldScore)then
                table.insertAt ( tRank, j, i )
                bInsert = true
                break
            end
        end
        if(not bInsert)then
            table.add ( tRank, i )
        end

    end


    --UPDATE HUD RANK
    local nExecoCount = 0
    for i=0 , table.getSize ( tRank ) -1
    do
        local nUser = table.getAt ( tRank, i )


        --IS EXECO ?
        local nScore = table.getAt ( this.tPlayersCurScores ( ), nUser )
        if(i>0)then
            local nPrevUser = table.getAt ( tRank, i-1 )
            local nOldScore = table.getAt ( this.tPlayersCurScores ( ), nPrevUser )
            if(nOldScore == nScore)then nExecoCount = nExecoCount +1 end
        end

        --GET HUD PREFIX
        local sPrefix = hashtable.getAt ( this.htPrefixEndTurnPlayerHUD ( ), nUser )

        --GET RANK
        local nRank = i+1 - nExecoCount

        --GET SUFFIX
        local sSuffixRank = "th"
        if(nRank==1)then sSuffixRank = "st"
        elseif(nRank==2)then sSuffixRank = "nd"
        elseif(nRank==3)then sSuffixRank = "rd"end

        --IS STAR VISIBLE
        local hStar = hud.getComponent ( hUser, sPrefix..".SpriteStar" )
        if(hStar)then
            if(nRank==1)then hud.setComponentVisible ( hStar, true )
            else hud.setComponentVisible ( hStar, false ) end
        end

        --GET NEW POS
        local nLerpTime =  1200
        local nPosY =  100-(i+0.5)/ this.GetMaxPlayerCount (  ) *100

        --log.warning ( sPrefix.." "..nScore.." "..nRank )
        local sAction = sPrefix..".SetPlayerRank"
        if(hud.isActionRunning ( hUser, sAction ))then hud.stopAction ( hUser, sAction ) end
        hud.callAction ( hUser, sAction, ""..nRank, sSuffixRank, nLerpTime, nPosY  )

    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
