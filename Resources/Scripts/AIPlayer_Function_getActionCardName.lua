--------------------------------------------------------------------------------
--  Function......... : getActionCardName
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.getActionCardName (nCardID )
--------------------------------------------------------------------------------
	
local nTotalActionCard =  hashtable.get ( this.htActionCards ( ),"ActionCards.ChildCount")

    local sTag
    if(nTotalActionCard>1)
    then
        sTag="ActionCards.ActionCard."..nCardID
    else
      
        sTag="ActionCards.ActionCard"
    end	
    
    
    local sName=hashtable.get ( this.htActionCards ( ),sTag..".Name")
    return sName

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
