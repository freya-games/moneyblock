--------------------------------------------------------------------------------
--  Function......... : StartPlaying
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.StartPlaying ( )
--------------------------------------------------------------------------------

    --Get player name
    --
    this.sPlayerName ( hud.getEditText ( hud.getComponent ( this.getUser ( ), "Network_Connection.Connection_Edit_Name" ) ) )

    --build one if none
    if ( string.isEmpty ( this.sPlayerName ( ) ) or this.sPlayerName ( )== " " )
    then
        this.sPlayerName ( "Player "..user.getID ( this.getUser ( ) ) )
    end

    --Add our player name in the names table
    hashtable.add ( this.aPlayerName ( ), ""..user.getID ( this.getUser ( ) ), this.sPlayerName (  ))

    if (  hud.getCheckState ( hud.getComponent ( this.getUser ( ), "Network_Connection.VoIP" ) ) )
    then
        microphone.setRate        ( 11025 )
        microphone.enable         ( true  )
        microphone.startDiffusion ( )
    end

    --Inform the main AI we are ready to start playing
    user.sendEvent ( this.getUser ( ), "Network_Main", "onStartPlaying", this.sPlayerName ( ) )


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
