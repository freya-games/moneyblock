--------------------------------------------------------------------------------
--  Function......... : updateTotalBookOrderToAllPlayers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.updateTotalBookOrderToAllPlayers (sCryptoID )
--------------------------------------------------------------------------------
	if(this.bAnonymous ( ))
    then
    
        log.message ( "Anonymous mode : Order Book not sent" )
    else
    
    
        for i =0, this.nPlayerCount ( )-1
        do
  
  
            user.sendEvent ( application.getUser (  table.getAt ( this.tUsers ( ),i )),"AIPlayer","onGetTotalBuyOrderCallback",sCryptoID,this.getTotalBuyOrder ( sCryptoID ))

            user.sendEvent ( application.getUser (  table.getAt ( this.tUsers ( ),i )),"AIPlayer","onGetTotalSellOrderCallback",sCryptoID,this.getTotalSellOrder ( sCryptoID ))

  
    
        end
	end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
