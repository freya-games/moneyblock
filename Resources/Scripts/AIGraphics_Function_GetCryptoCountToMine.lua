--------------------------------------------------------------------------------
--  Function......... : GetCryptoCountToMine
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetCryptoCountToMine ( sResID )
--------------------------------------------------------------------------------
	
    local bIsSelling = false
    
	if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Buy" ))
    then
        
        return hashtable.get ( this.htPendingOrders ( ),sResID..".Buy"), bIsSelling

    elseif(hashtable.contains ( this.htPendingOrders ( ), sResID..".Sell" ))
    then
        bIsSelling = true
        return hashtable.get ( this.htPendingOrders ( ) ,sResID..".Sell"), bIsSelling
    else
        return 0, bIsSelling
 
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
