--------------------------------------------------------------------------------
--  Function......... : OpenContainerCardsHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.OpenContainerCardsHUD ( )
--------------------------------------------------------------------------------
	
	 local sAction = "PlayerMainHUD.OpenContainerCards"
    if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction ) end
    hud.callAction ( this.getUser ( ), sAction )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
