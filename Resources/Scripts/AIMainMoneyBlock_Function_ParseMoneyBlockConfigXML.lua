--------------------------------------------------------------------------------
--  Function......... : ParseMoneyBlockConfigXML
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ParseMoneyBlockConfigXML ( )
--------------------------------------------------------------------------------
	
 if(this.xMoneyBlockConfig ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xMoneyBlockConfig ( ))
    local sVersion=    xml.getAttributeValue ( xml.getElementAttributeWithName ( hRootElement,"version") )
 
    if(sVersion~= application.getCurrentUserEnvironmentVariable ( "version" ))
    then
        log.message ( "MoneyBlockConfigXML Bad Version Waiting for version "..application.getCurrentUserEnvironmentVariable ( "version" ).." found "..sVersion)
        application.quit ( )
    end

    if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.recurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htGameConfig ( ),ht)
        
    end

  
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
