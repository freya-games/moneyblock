--------------------------------------------------------------------------------
--  Function......... : getResourceType
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getResourceType (sResourceID )
--------------------------------------------------------------------------------
	

if(hashtable.contains (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Type" ))
then
    return hashtable.get (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Type" )
else
    log.error (  )
    return nil
end
 
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
