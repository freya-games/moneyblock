--------------------------------------------------------------------------------
--  Function......... : ActionNoChangeCryptoCount
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionNoChangeCryptoCount (nCardIndex,sCryptoID,nPlayerIndex )
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end




  if(hashtable.contains (this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sCryptoID..".Count.Lock" ))
    then
    hashtable.set (this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sCryptoID..".Count.Lock",true )
    else
    hashtable.add (this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sCryptoID..".Count.Lock",true )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
