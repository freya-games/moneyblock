--------------------------------------------------------------------------------
--  Handler.......... : onCommitBuyOrder
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onCommitBuyOrder ( sResID,nAmount,nTotalCount,bCancel )
--------------------------------------------------------------------------------
	
	
 
   
      local sMessage
        if(bCancel)
        then
            sMessage="Buy order Canceled "..nAmount.." "..sResID
            if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Buy" ))
            then
                hashtable.set ( this.htPendingOrders ( ),sResID..".Buy" ,0)
            else
                hashtable.add ( this.htPendingOrders ( ),sResID..".Buy" ,0)
            end
            
        else
            sMessage="Buy Order Commited "..nAmount.." "..sResID
            
            if(hashtable.contains ( this.htResourceCount ( ),sResID ))
            then
                hashtable.set ( this.htResourceCount ( ),sResID, nTotalCount)
            else
                hashtable.add ( this.htResourceCount ( ),sResID,nTotalCount )
            end
	
           
            
        end
        
        
        this.UpdateHUD (  )
   
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
