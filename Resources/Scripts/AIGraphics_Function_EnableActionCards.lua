--------------------------------------------------------------------------------
--  Function......... : EnableActionCards
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableActionCards ( bEnable )
--------------------------------------------------------------------------------
	
    if(this.bUseDebugHUD ( ))then return end
    
    
	local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerActionCards" )
    if(hComponent)then 
        hud.setComponentActive ( hComponent, bEnable )
    end


    hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerSlotCards" )
    if(hComponent)then 
        hud.setComponentActive ( hComponent, bEnable )
        hud.setComponentVisible ( hComponent, bEnable )
    end
    
    --RESET PREVIEW CARDS
    if(bEnable)then
    
        for i=0 , hashtable.getSize ( this.htPrefixSlotCardHUD ( ) ) -1
        do
            local sPrefix = hashtable.getAt ( this.htPrefixSlotCardHUD ( ), i )
        
            local hC = hud.getComponent ( this.getUser ( ), sPrefix..".PreviewCard_ContainerGlobal"  )
            
            if( hC )then
                hud.setComponentActive ( hC, false )
                hud.setComponentVisible ( hC, false )
            end
            
            hC = hud.getComponent ( this.getUser ( ), sPrefix..".ContainerGlobal"  )
            
            if( hC )then
                hud.setComponentActive ( hC, true )
                hud.setComponentVisible ( hC, true )
                hud.setComponentOpacity ( hC, 255 )
            end
            
        end
    
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
