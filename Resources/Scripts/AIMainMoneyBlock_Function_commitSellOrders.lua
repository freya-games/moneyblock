--------------------------------------------------------------------------------
--  Function......... : commitSellOrders
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.commitSellOrders ( )
--------------------------------------------------------------------------------
	
	    


    for nPlayerIndex =0, table.getSize ( this.tUsers ( ) )-1
    do
    
        local nUserID=table.getAt ( this.tUsers ( ),nPlayerIndex )
        if(nUserID)
        then
            
            
            local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

            for i=0,nCount-1
            do
                local sResourceID =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
                
                local bCancelHodl=hashtable.get ( this.htRuntimeData ( ), "Game.Resources."..sResourceID ..".HodlCancelSell")
                if(not bCancelHodl)
                then
                    bCancelHodl=false
                end
                local bCancel=(hashtable.get ( this.htRuntimeData ( ), "Game.Resources."..sResourceID ..".CancelSell") or bCancelHodl)
                
                local nAmount=hashtable.get ( this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                local nResPrice=      hashtable.get(this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Price" )
              
                local nResCount=this.getResourceCount (  sResourceID) 
                if (nAmount)
                then
                    if(bCancel)
                    then
                      
                       
                        --this.ChangePlayerResource (nPlayerIndex, sResourceID,nAmount )
                  
                        if(nResCount~=-1)
                        then
                      
                           
                           --this.changeGameResourceCount ( sResourceID,-1*nAmount )
                            
        
                        end
--                         if(this.getResourceType ( sResourceID )~="CRYPTO")
--                         then
--                             local nTotal=nAmount*nResPrice
--                             this.changePlayerCash ( nPlayerIndex,-1*nTotal )
--                         end
                       
                    
                    
                    else

                        if(this.getResourceType ( sResourceID )=="CRYPTO")
                        then
                            local nTotal=nAmount*nResPrice
                            this.changePlayerCash ( nPlayerIndex,nTotal )
                            this.ChangePlayerResource (nPlayerIndex, sResourceID,-1*nAmount )
                        end
                    end
                    
                    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onCommitSellOrder",sResourceID,nAmount,this.getPlayerResourceCount (nUserID, sResourceID ),bCancel)
                    user.sendEvent ( application.getUser ( nUserID ),"AIPlayer", "onGetMyCashCallback",this.getPlayerCash (  nUserID))
                    hashtable.remove ( this.htCurrentSellOrders ( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                end
                
                
            end
        else
            log.error ( "AIMainMoneyBlock.onBuy UserID Error" )
        end   
    end  

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
