--------------------------------------------------------------------------------
--  Function......... : updateDynamics
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIAvatar.updateDynamics ( )
--------------------------------------------------------------------------------
	
    local hObject = this.getObject ( )
    local hDynObj = this.hBody ( )

    if(this.hBody ( )and this.hCamera ( ))
    then
        
        local cam = this.hCamera ( )
        
        if ( not ( this.bPunch ( ) or this.bSit ( ) ) and ( this.nMoveX ( ) ~= 0 or this.nMoveY ( ) ~= 0 ) )
        then
            --Body move
            local Xx, Xy, Xz = object.getXAxis ( cam, object.kGlobalSpace )        
            Xx, Xy, Xz = math.vectorScale ( Xx, Xy, Xz, this.nMoveX ( ) * 200 )
            
            local Zx, Zy, Zz = object.getDirection ( cam, object.kGlobalSpace )        
            Zx, Zy, Zz = math.vectorScale ( Zx, Zy, Zz, this.nMoveY ( ) * 200 )
            
            local fx, fy, fz = math.vectorAdd ( Xx, 0, Xz, Zx, 0, Zz )
            dynamics.addForce ( hDynObj, fx, 0, fz, object.kGlobalSpace )

            local x, y, z = object.getTranslation ( hObject, object.kGlobalSpace )
            object.lookAt ( hObject, x + fx, y , z + fz, object.kGlobalSpace, 0.1 )
            
            dynamics.setLinearDamping ( hDynObj, 5 )
        end
        object.matchTranslation ( hObject, hDynObj, object.kGlobalSpace )
        object.translate ( hObject, 0, -0.35, 0, object.kGlobalSpace )
    end
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
