--------------------------------------------------------------------------------
--  Function......... : changePlayerCash
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.changePlayerCash ( nPlayerIndex,nAmount)
--------------------------------------------------------------------------------
	

                
        
    if(hashtable.contains (this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash"))
    then
        local nV=hashtable.get (this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash" )
        local nNewValue=nV+nAmount
        if(nNewValue<0)
        then
            nNewValue=0
        end
        hashtable.set ( this.htRuntimeData ( ), "Player."..nPlayerIndex..".Cash", nNewValue )
        
        local hUser=application.getUser (  table.getAt ( this.tUsers ( ),nPlayerIndex ))
        user.sendEvent ( hUser,"AIPlayer", "onGetMyCashCallback",this.getPlayerCash (user.getID ( hUser )  ))

    else
    
    
        --hashtable.add ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sResID ,nValueToAdd )
        log.error ( "AIMainMoneyBlock.changePlayerCash  ".."Player."..nPlayerIndex..".Cash does not exist")
    end
    return
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
