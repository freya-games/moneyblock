--------------------------------------------------------------------------------
--  Function......... : getAllResourcesCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.getAllResourcesCount ( )
--------------------------------------------------------------------------------
	
local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
for i=0, nCount-1
do
    local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
    this.GetMyResourceCount ( sID)
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
