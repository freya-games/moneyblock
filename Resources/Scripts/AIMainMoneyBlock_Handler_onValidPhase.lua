--------------------------------------------------------------------------------
--  Handler.......... : onValidPhase
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onValidPhase (nUserID  )
--------------------------------------------------------------------------------
	
	hashtable.set ( this.htValidPhase ( ),""..nUserID,true )
    
    --NOTIFY PLAYERS
    for i=0, table.getSize ( this.tUsers ( ) )-1
    do
        user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),i ) ),"AIPlayer","onPlayerReady", nUserID  )
    end
    
    -- NEXT PHASE IF ALL PLAYERS READY
   if( this.checkValidPhase (  ))
   then

        this.NextPhase (  )
    end
    
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
