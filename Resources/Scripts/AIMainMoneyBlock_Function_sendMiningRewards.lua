--------------------------------------------------------------------------------
--  Function......... : sendMiningRewards
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendMiningRewards ( sCryptoID)
--------------------------------------------------------------------------------
    local htMean=hashtable.newInstance ( )
    local htCoefTotal=hashtable.newInstance ( )
    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    
    if(this.getResourceType ( sCryptoID)=="CRYPTO")
    then
      
        hashtable.add (htCoefTotal,sCryptoID,0 )
       
    end

    for iC=0, nCount-1
    do
        if(sCryptoID==hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID"))
        then
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
            then
                   
                local nCryptoCurrentBlock =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )
                if(nCryptoCurrentBlock)
                then
                 
                    local sTag
                      
                        
                    if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.ChildCount")>1)
                    then
               
                        sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block."..nCryptoCurrentBlock
                         
                    else
                        sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block"
                    
                    end
                    
                    local nResCount=hashtable.get ( this.htGameConfig ( ),sTag..".ChildCount")
                    if(nResCount>1)
                    then
                        for j=0,nResCount-1
                        do
                            local sResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".ID")
        
                            local sType=this.getResourceType ( sResID)
                            if(sType=="MINER" or sType=="ENERGY")
                            then
                            
                                local nCoef=hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sResID..".MiningRewardCoef")
                                if(nCoef)
                                then
                                    hashtable.set ( htCoefTotal,sCryptoID,hashtable.get ( htCoefTotal,sCryptoID )+nCoef )
                                end
                                
                             end 
                        end
                    end
                    if(nResCount==1)
                    then
                        local sResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.ID")
                        local sType=this.getResourceType ( sResID)
                        if(sType=="MINER" or sType=="ENERGY")
                        then
                        
                            local nCoef=hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sResID..".MiningRewardCoef")
                            hashtable.set ( htCoefTotal,sCryptoID,hashtable.get ( htCoefTotal,sCryptoID )+nCoef )
                        end
                    end
                    
                    
                end
            end
        end
    end
    
    
   local htTotalMean=hashtable.newInstance ( )
   local htLastRewards=hashtable.newInstance ( )
   local nTotalReward = 0
   for nPlayerIndex=0, this.nPlayerCount ( )-1
    do
       
           
            hashtable.add ( htMean,nPlayerIndex.."."..sCryptoID,0 )
     
       
    end
    
    for iC=0, nCount-1
    do
        hashtable.add ( htTotalMean,sCryptoID,0 )
        hashtable.add ( htLastRewards,sCryptoID,0 )
    end
  
    for nPlayerIndex=0, this.nPlayerCount ( )-1
    do
    
       
        for iC=0, nCount-1
        do
            if(sCryptoID==hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID"))
            then
            
          
                if(this.getResourceType ( sCryptoID)=="CRYPTO")
                then
                    for iR=0, nCount-1
                    do
                        local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                        local sType=this.getResourceType ( sResID)
                        if(sType=="MINER" or sType=="ENERGY")
                        then
                            local nAmount= hashtable.get ( this.htMining ( ), "Player."..nPlayerIndex.."."..sCryptoID.."."..sResID )
                            local nCoef=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".MiningRewardCoef")
                            
                            if(nCoef and nAmount and nAmount>0)
                            then
                                local nTotal=hashtable.get ( htCoefTotal,sCryptoID )
                            
                          
                               
                                hashtable.set ( htMean,nPlayerIndex.."."..sCryptoID,hashtable.get (htMean, nPlayerIndex.."."..sCryptoID )+ nAmount*nCoef/nTotal)
                            end
                            
                        end
                    end
                    
                    --GET TOTAL REWARD
                    local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )               
                    local sTag       
                    if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.ChildCount")>1)
                    then
                        sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block."..(nCB)
                         
                    else
                         sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block"
                    
                    end  
                    nTotalReward=hashtable.get ( this.htGameConfig ( ),sTag..".Attributes.reward")
                end
            end
        end
    end
    
   
    for nPlayerIndex=0, this.nPlayerCount ( )-1
    do
     
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then 
            hashtable.set ( htTotalMean,sCryptoID, hashtable.get (htTotalMean,sCryptoID)+hashtable.get (htMean,nPlayerIndex.."."..sCryptoID))
        end
    end
    hashtable.set ( htLastRewards, sCryptoID, nTotalReward )
    
    
    --COUNT LAST REWARDS 
    for nPlayerIndex=0, this.nPlayerCount ( )-1
    do
     
        local mean=hashtable.get ( htMean,nPlayerIndex.."."..sCryptoID )
        local totalmean=hashtable.get ( htTotalMean,sCryptoID )
        
        local nReward =math.floor ((mean/totalmean)*nTotalReward)
        hashtable.set ( htLastRewards, sCryptoID, hashtable.get ( htLastRewards, sCryptoID)-nReward )
    end
    
    
    --GIVE ALL REWARDS REMINING TO BEST MINING PLAYER
    local nRewardsToSplit = hashtable.get ( htLastRewards, sCryptoID)
    if(nRewardsToSplit>0)then
        
        local nIndexPlayerRewarded = -1
        local nRequiredAmmount = 0
        
        for nPlayerIndex=0, this.nPlayerCount ( )-1
        do
            local nCurPlayerAmmount = hashtable.get ( htMean,nPlayerIndex.."."..sCryptoID )
            
            if(nCurPlayerAmmount> nRequiredAmmount)then
                nRequiredAmmount = nCurPlayerAmmount
                nIndexPlayerRewarded = nPlayerIndex
                
            elseif(nCurPlayerAmmount==nRequiredAmmount)then
                nRequiredAmmount = nCurPlayerAmmount
                nIndexPlayerRewarded = -1
            end
        end
        
        if(nIndexPlayerRewarded>0)then
            --log.warning ( "Player "..nIndexPlayerRewarded)
            hashtable.add ( htLastRewards, ""..nIndexPlayerRewarded, nRewardsToSplit )
        end
    end
    
    
    
    
    for nPlayerIndex=0, this.nPlayerCount ( )-1
    do
        for iC=0, nCount-1
        do
            if(sCryptoID==hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".ID"))
            then
                if(this.getResourceType ( sCryptoID)=="CRYPTO")
                then
                
                
                    local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )               
                    local sTag       
                    if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.ChildCount")>1)
                    then
                        sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block."..(nCB)
                         
                    else
                         sTag="MoneyBlockConfig.Game.Resources.Resource."..iC..".BlockChain.Block"
                    
                    end  
                    local nReward= hashtable.get ( this.htGameConfig ( ),sTag..".Attributes.reward")
                    
                    
                    if(nReward)
                    then
                        local mean=hashtable.get ( htMean,nPlayerIndex.."."..sCryptoID )
                        local totalmean=hashtable.get ( htTotalMean,sCryptoID )
                        nReward=math.floor ((mean/totalmean)*nReward)
                        
                        --GET EXTRA REWARDS
                        local nExtraReward = hashtable.get ( htLastRewards, ""..nPlayerIndex)
                        if(nExtraReward)then nReward = nReward + nExtraReward end
                                            
                        if(nReward >0)
                        then
                            local nResCount=this.getResourceCount (  sCryptoID)
                            if(nResCount==-1)
                            then
                                this.ChangePlayerResource (nPlayerIndex,sCryptoID, nReward )
                                user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex ) ),"AIPlayer","onReceiveMiningReward" ,sCryptoID, this.getPlayerResourceCount ( table.getAt ( this.tUsers ( ),nPlayerIndex ), sCryptoID),nReward )
                                                      
                                                   
                            else
                                if(nResCount< nReward)
                                then
                                    if(nResCount>0)
                                    then
                                        nReward=nResCount
                                        this.ChangePlayerResource (nPlayerIndex,sCryptoID, nReward )
                                        user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex ) ),"AIPlayer","onReceiveMiningReward" ,sCryptoID,this.getPlayerResourceCount ( table.getAt ( this.tUsers ( ),nPlayerIndex ), sCryptoID),nReward)
                                    else
                                        user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex) ),"AIPlayer","onReceiveMessage" ,"[Server] Cannot Give Rewards, no more token "..sCryptoID.." available")
                                    end
                                  
                                else
                                                    
                                    this.ChangePlayerResource (nPlayerIndex,sCryptoID, nReward )
                                    user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex ) ),"AIPlayer","onReceiveMiningReward" ,sCryptoID, this.getPlayerResourceCount ( table.getAt ( this.tUsers ( ),nPlayerIndex ), sCryptoID),nReward )
                                    if(nResCount~=-1)
                                    then
                            
                          
                                        this.changeGameResourceCount ( sCryptoID,-1*nReward )

                                    end
                                                        
                                end
                                                    
                            end
                        end
                    end
                end
            end
        end        
        
    end
	
    

    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
