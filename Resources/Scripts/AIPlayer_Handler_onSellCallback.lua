--------------------------------------------------------------------------------
--  Handler.......... : onSellCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onSellCallback ( sResourceID,nAmount,nTotalCount,bCanSell )
--------------------------------------------------------------------------------


    local sMes
    if(bCanSell)
    then
    
        if(this.getType ( sResourceID)=="CRYPTO")
        then
            if(hashtable.contains ( this.htPendingOrders ( ), sResourceID..".Sell" ))
            then
                hashtable.set ( this.htPendingOrders ( ) ,sResourceID..".Sell" ,nAmount)
            else
                hashtable.add ( this.htPendingOrders ( ), sResourceID..".Sell" ,nAmount)
            end
            
              if(hashtable.contains ( this.htPendingOrders ( ), sResourceID..".Buy" ))
            then
                hashtable.set ( this.htPendingOrders ( ) ,sResourceID..".Buy" ,0)
         
            end
            sMes="Pending Sell "
        else
             sMes="Sold "
        end
        
        user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onSellCallback", sResourceID,nAmount,nTotalCount,bCanSell  )
        this.openDialogBox (sMes..nAmount.." "..sResourceID )
    else
      
        this.openDialogBox ( "Cannot sell "..nAmount.." "..sResourceID)
    end
	
    

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
