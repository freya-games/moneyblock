--------------------------------------------------------------------------------
--  Handler.......... : onOpenMining
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onOpenMining (  )
--------------------------------------------------------------------------------
	
local hComponentResource = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResource" )
hud.removeListAllItems ( hComponentResource )

local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResourceValue" )
hud.removeListAllItems ( hComponent )
table.empty (  this.tMiningResourceHUD  ( ) )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
