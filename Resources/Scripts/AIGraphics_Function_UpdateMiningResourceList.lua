--------------------------------------------------------------------------------
--  Function......... : UpdateMiningResourceList
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningResourceList ( )
--------------------------------------------------------------------------------
	
local hComponentResource = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResource" )
hud.removeListAllItems ( hComponentResource )

local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResourceValue" )
hud.removeListAllItems ( hComponent )
table.empty (  this.tMiningResourceHUD  ( ) )

    


    local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.MiningCrypto" )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
  
    local sResID=table.getAt ( this.tMiningCryptoHUD ( ),nItem )
    
    local nBlock = 0
    if(hashtable.contains ( this.htCryptoCurrentBlock ( ), sResID))then 
        nBlock=hashtable.get ( this.htCryptoCurrentBlock ( ),sResID )
    end
    
    if(nBlock)
    then
    
     
        local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
        for iR=0, nCount-1
        do
            local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
            if(sID ==sResID)
            then
                
                local nResourceIndex=iR
               
                 local nBlockChainCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.ChildCount")
                  for i=0,nBlockChainCount-1
                  do
                    local nOrder=i
                    if(nOrder==nBlock)
                    then
                        local sValidation
                        local sTag
                        if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.ChildCount")>1)
                        then
                              sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block."..i
                        else
                              sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block"
                        
                        end
                        local nResCount=hashtable.get ( this.htGameConfig ( ),sTag..".ChildCount")
                        if(nResCount>1)
                        then
                            for j=0,nResCount-1
                            do
                                local sID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".ID")
                                local nAmount=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".Amount")
                                
                                
                                 
                                    
                                 local nCount=hashtable.get ( this.htResourceCount ( ),sID )


                                if(nCount)
                                then

                                        local sType=this.GetResourceType ( sID)

                                  
                                    if(sType=="MINER" or sType=="ENERGY")
                                    then
                                        local nIndex=hud.addListItem ( hComponentResource,this.GetResourceName (  sID) )
                                        
                                        local nTotalMining=0
                                        for i=0,hashtable.getSize ( this.htMining ( ) )-1
                                        do

                                            local sS=hashtable.getKeyAt ( this.htMining ( ),i )
                                            local t=table.newInstance ( )
                                            string.explode ( sS,t,"." )
                                            
                                            local sMiningCryptoID=table.getAt ( t,0 )
                                            local sMiningResourceID=table.getAt ( t,1 )
                                            local nMiningResourceCount= hashtable.getAt ( this.htMining ( ),i )
                                            if(sMiningResourceID==sID)
                                            then
                                                nTotalMining=nTotalMining+nMiningResourceCount
                                            end
                                            
                                        end
                                                                    
                                        hud.setListItemTextAt ( hComponentResource, nIndex, 1,""..nCount-nTotalMining )
                                        table.add (  this.tMiningResourceHUD ( ),sID)

                                        
                                    end
                                    
                                   
                                end
                                
                            end
                        end
                        if(nResCount==1)
                        then
                            local sID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.ID")
                            local nAmount=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.Amount")
                            
        
                             local nCount=hashtable.get ( this.htResourceCount ( ),sID )


                            if(nCount)
                            then

                                    local sType=this.GetResourceType ( sID)

                              
                                if(sType=="MINER" or sType=="ENERGY")
                                then
                                    local nIndex=hud.addListItem ( hComponentResource,this.GetResourceName (  sID) )
                                    hud.setListItemTextAt ( hComponentResource, nIndex, 1,""..nCount )
                                    table.add (  this.tMiningResourceHUD ( ),sID)

                                    
                                end
                                
                               
                            end
                        end
                        
                    end
                    
                  end
                
            end
        
        end

end
	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
