--------------------------------------------------------------------------------
--  Handler.......... : onPlayerReady
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onPlayerReady ( nUserID )
--------------------------------------------------------------------------------
	
    if(hashtable.contains ( this.htPlayerReady ( ), ""..nUserID ))then
         hashtable.set( this.htPlayerReady ( ), ""..nUserID, true )
    else
        hashtable.add ( this.htPlayerReady ( ), ""..nUserID, true )
    end
    
	local nIndex = this.GetUserIndex ( nUserID )
    if(nIndex)then
    
        this.SetPlayerHUDReady ( nIndex, true )
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
