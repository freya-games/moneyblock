--------------------------------------------------------------------------------
--  Function......... : UpdateBlockChainModel
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateBlockChainModel ( )
--------------------------------------------------------------------------------


    local hScene = user.getScene ( this.getUser ( ) )

    local nCrypto = hashtable.getSize ( this.htBlockChainCount ( ) )
    for i=0 , nCrypto-1
    do
        local sCryptoID = hashtable.getKeyAt ( this.htBlockChainCount ( ), i)
        local nBlockCount = hashtable.getAt ( this.htBlockChainCount ( ), i)
        local nBlockMined = 0
        local r, g, b = this.GetResourceColor ( sCryptoID, true )


        --GET BLOCK MINED
        if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sCryptoID ))
        then
            nBlockMined = hashtable.get ( this.htCryptoCurrentBlock  ( ),sCryptoID )
        end

        --GET ID CUR BLOCK
        local nCurBlockID = nBlockMined+1


        for j=1 , nBlockCount
        do
            if(nCurBlockID>=j)
            then

                local bIsCurBlock = false
                if(nCurBlockID==j)then
                    bIsCurBlock = true
                end

                local hBlock = scene.getTaggedObject ( hScene, sCryptoID.."."..j )
                if(hBlock)then
                    object.setUniformScale ( hBlock, 0.4 )

                    if(not bIsCurBlock)then

                        --MINED BEHAVIOUR
                        for nIndexSubObj=0 ,  object.getChildCount ( hBlock )-2
                        do
                            object.setVisible ( object.getChildAt ( hBlock, nIndexSubObj ), true )
                        end
                    else
                        --NEXT TO BE MINED
                        object.setVisible ( object.getChildAt ( hBlock, 0 ), true)
                    end

                    --DISPLAY TEXTURE OF CUR LEVEL
                    local hDecal = object.getChildAt ( hBlock, object.getChildCount ( hBlock )-1)
                    object.setVisible ( hDecal, bIsCurBlock)
                    shape.overrideMeshMaterialAmbient ( hDecal, r, g, b, 1 )

                    if(application.isResourceReferenced ( "FB_BlockSelect"..j,application.kResourceTypeTexture))then

                        shape.overrideMeshMaterialEffectMap0 ( hDecal, "FB_BlockSelect"..j )
                    end


                end
            end
        end
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
