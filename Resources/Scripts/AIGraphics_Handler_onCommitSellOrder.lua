--------------------------------------------------------------------------------
--  Handler.......... : onCommitSellOrder
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onCommitSellOrder ( sResID,nAmount,nTotalCount,bCancel )
--------------------------------------------------------------------------------
	
	
     
        local sMessage
        if(bCancel)
        then
            sMessage="Sell order Canceled "..nAmount.." "..sResID
            
            if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Sell" ))
            then
                hashtable.set ( this.htPendingOrders ( ),sResID..".Sell" ,0)
            else
                hashtable.add ( this.htPendingOrders ( ),sResID..".Sell" ,0)
            end
            
        else
            sMessage="Sell order Commited "..nAmount.." "..sResID
            if(hashtable.contains ( this.htResourceCount ( ),sResID ))
            then
                hashtable.set ( this.htResourceCount ( ),sResID, nTotalCount)
            else
                hashtable.add ( this.htResourceCount ( ),sResID,nTotalCount )
            end
        
            
   
        end
        
        this.UpdateHUD (  )
 
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
