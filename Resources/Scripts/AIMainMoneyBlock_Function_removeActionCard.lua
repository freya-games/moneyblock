--------------------------------------------------------------------------------
--  Function......... : removeActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.removeActionCard (nUserID,nCardID )
--------------------------------------------------------------------------------
    local nPlayerIndex=this.getPlayerIndex ( nUserID )
  local nCardCount=hashtable.get ( this.htRuntimeData ( ),"Player.".. nPlayerIndex..".ActionCards.Count")
  
    for i=0,nCardCount-1
    do
        if(hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards."..i)==nCardID)
        then
            hashtable.remove ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards."..i)
            return true
        end
     
        
    end
    
    return false
	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
