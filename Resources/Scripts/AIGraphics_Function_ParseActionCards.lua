--------------------------------------------------------------------------------
--  Function......... : ParseActionCards
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.ParseActionCards ( )
--------------------------------------------------------------------------------

if(this.xActionCards ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xActionCards ( ) )

     if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.RecurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htActionCards ( ),ht)

    end

end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
