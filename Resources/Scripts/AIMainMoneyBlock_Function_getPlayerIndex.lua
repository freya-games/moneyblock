--------------------------------------------------------------------------------
--  Function......... : getPlayerIndex
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getPlayerIndex  ( nUserID)
--------------------------------------------------------------------------------

    --log.warning (  this.tUsers ( ) )

   for i =0, table.getSize ( this.tUsers ( ) )-1
    do
        local nID = table.getAt ( this.tUsers ( ),i )

        --if(hashtable.get ( this.htRuntimeData ( ),"Player."..i..".UserID" )==nUserID)
        if(nID == nUserID)
        then
            return i
        end
    end
        log.error ( "fail to find player index ", nUserID )
    return nil

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
