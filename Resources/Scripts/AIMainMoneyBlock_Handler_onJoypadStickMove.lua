--------------------------------------------------------------------------------
--  Handler.......... : onJoypadStickMove
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.onJoypadStickMove ( nJoypad, nStick, nAxisX, nAxisY )
--------------------------------------------------------------------------------


local idx=-1
for i=0,table.getSize ( this.tJoypadMapping ( ) )-1
do
    if(table.getAt ( this.tJoypadMapping ( ),i )==nJoypad)
    then
        idx=i
        break
    end
end
if(idx==-1)
then
    return
end
  if(math.abs (  nAxisX)>this.nPadTreshold ( ) )
    then
         local nWidth=hashtable.get ( this.htPlayerViewport ( ),idx..".W"  ) 
            local nHeight=hashtable.get ( this.htPlayerViewport ( ),idx..".H"  )  
            table.setAt ( this.tMouseDeltaX ( ),idx,nAxisX*(nHeight/nWidth)*this.nJoypadFactor ( ) )
  
    else
       table.setAt ( this.tMouseDeltaX ( ),idx,0 )
     

    end
    if(math.abs (  nAxisY)>this.nPadTreshold ( ) )
    then
          --local nLeft=hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".L" ) 
            --local nBottom=  hashtable.get ( this.htPlayerViewport ( ),nPlayerIndex..".B"  ) 
            local nWidth=hashtable.get ( this.htPlayerViewport ( ),idx..".W"  ) 
            local nHeight=hashtable.get ( this.htPlayerViewport ( ),idx..".H"  )  
            table.setAt ( this.tMouseDeltaY ( ),idx,nAxisY*(nWidth/nHeight)*this.nJoypadFactor ( ) )
    else
             table.setAt ( this.tMouseDeltaY ( ),idx,0 )

    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
