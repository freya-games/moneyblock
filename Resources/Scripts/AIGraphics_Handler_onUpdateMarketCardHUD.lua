--------------------------------------------------------------------------------
--  Handler.......... : onUpdateMarketCardHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onUpdateMarketCardHUD (_nMarketCardID )
--------------------------------------------------------------------------------

    local sText=this.GetMarketCardInfo (_nMarketCardID,"Name")
    if(sText)
    then
        local sTag = this.sPrefixMarketCardHUD ( )..".LabelTitle"
        if(this.bUseDebugHUD ( ))then sTag = "PlayerHUD.MarketCard" end
        
    	local hMC=hud.getComponent ( this.getUser ( ),sTag )
         hud.setLabelText ( hMC,sText )
    end
   
   
    sText=this.GetMarketCardInfo (_nMarketCardID,"Description" )
    if(sText)
    then
        local sTag = this.sPrefixMarketCardHUD ( )..".LabelDescription"
        if(this.bUseDebugHUD ( ))then sTag = "PlayerHUD.MarketCardDescription" end
        
    	local hMCDesc=hud.getComponent ( this.getUser ( ), sTag )
         hud.setLabelText ( hMCDesc,sText )
    end
    
    
    if(not this.bUseDebugHUD ( ))then
        local sImg=this.GetMarketCardInfo (_nMarketCardID,"Img" )
        if(sImg)
        then
            local sTag = this.sPrefixMarketCardHUD ( )..".BackGround"
            
            local hBG=hud.getComponent ( this.getUser ( ), sTag )
             hud.setComponentBackgroundImage ( hBG,sImg )
        end
    end
    
    
    this.nMarketCardID ( _nMarketCardID )
    this.UpdateMarketCardHUD (  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
