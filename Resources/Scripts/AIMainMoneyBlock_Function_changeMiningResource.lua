--------------------------------------------------------------------------------
--  Function......... : changeMiningResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.changeMiningResource (sCryptoID,sResID,nAmount )
--------------------------------------------------------------------------------
        
    local sKey = sCryptoID.."."..sResID
        
    if(hashtable.contains (this.htMining ( ), sKey ))
    then
        local nCurValue = hashtable.get ( this.htMining ( ), sKey)
        hashtable.set ( this.htMining ( ), sKey , nAmount + nCurValue )
    else

        hashtable.add ( this.htMining ( ),sKey ,nAmount )
        
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
