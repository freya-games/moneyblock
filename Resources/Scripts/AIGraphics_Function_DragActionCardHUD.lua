--------------------------------------------------------------------------------
--  Function......... : DragActionCardHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.DragActionCardHUD ( )
--------------------------------------------------------------------------------
	
if(this.bUseDebugHUD ( ))then return end
    
    
local hUser = this.getUser ( )

if(this.sTagDraggedCard ( ) ~= "")then
    
    local hComponent = hud.getComponent ( hUser , this.sTagDraggedCard ( ) )
    if(hComponent)then
        
        local nPosX, nPosY = hud.getCursorPosition ( hUser )
        hud.setComponentPosition ( hComponent, nPosX, nPosY   )
    end

end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
