--------------------------------------------------------------------------------
--  Function......... : initMainMenu
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.initMainMenu ( )
--------------------------------------------------------------------------------


    hud.newTemplateInstance ( this.getUser ( ),"MainMenu","MainMenu" )


local hComponent = hud.getComponent ( this.getUser ( ), "MainMenu.PlayerCountList" )

hud.setListItemsHeight ( hComponent, 16.66 )
hud.setListTextHeight ( hComponent, 50 )
hud.setListColumnWidthAt ( hComponent, 0, 100 )
    local nMinPlayerCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MinPlayersCount")

    local nMaxPlayerCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MaxPlayersCount")

    for i=nMinPlayerCount, nMaxPlayerCount
    do
        hud.addListItem ( hComponent,""..i )
    end

    --Version Control
    hud.setLabelText (hud.getComponent ( this.getUser ( ), "MainMenu.Version"),application.getCurrentUserEnvironmentVariable ( "prefixVersion" ).." "..application.getCurrentUserEnvironmentVariable ( "version" ) )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
