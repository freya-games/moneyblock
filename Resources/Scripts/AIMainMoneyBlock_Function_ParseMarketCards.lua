--------------------------------------------------------------------------------
--  Function......... : ParseMarketCards
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ParseMarketCards ( )
--------------------------------------------------------------------------------

if(this.xMarketCards ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xMarketCards ( ) )
        log.message ( xml.toString ( hRootElement ) )
 local sVersion=    xml.getAttributeValue ( xml.getElementAttributeWithName ( hRootElement,"version") )

    if(sVersion~= application.getCurrentUserEnvironmentVariable ( "version" ))
    then
        log.message ( "MarketCardsXML Bad Version Waiting for version "..application.getCurrentUserEnvironmentVariable ( "version" ).." found "..sVersion)
        application.quit ( )
    end

    if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.recurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htMarketCards ( ),ht)

    end

    hashtable.add ( this.htRuntimeData ( ),"TotalMarketCards", hashtable.get ( this.htMarketCards ( ),"MarketCards.ChildCount"))
    this.NewMarketDrawCard ( )

end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
