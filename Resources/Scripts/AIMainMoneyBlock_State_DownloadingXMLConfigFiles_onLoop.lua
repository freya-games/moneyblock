--------------------------------------------------------------------------------
--  State............ : DownloadingXMLConfigFiles
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.DownloadingXMLConfigFiles_onLoop ( )
--------------------------------------------------------------------------------

    local nS=xml.getReceiveStatus ( this.xMoneyBlockConfig ( ) )
    local nSM=xml.getReceiveStatus ( this.xMarketCards ( ) )
    local nSA=xml.getReceiveStatus ( this.xActionCards ( ) )
    if(nS==1 and nSM ==1 and nSA==1)
    then

        this.ParseMoneyBlockConfigXML ( )
        --this.initMainMenu ( )
        this.Idle ( )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
