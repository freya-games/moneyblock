--------------------------------------------------------------------------------
--  Function......... : DisplayPlayedCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.DisplayPlayedCard ( sPrefixDisplaySlot, nCardID)
--------------------------------------------------------------------------------
	
    local sImg = this.GetActionCardInfos ( nCardID, "Img" ) 
    local sName = this.GetActionCardInfos ( nCardID, "Name" ) 
    local sDescription = this.GetActionCardInfos ( nCardID, "Description" ) 
    
    local sAction = sPrefixDisplaySlot..".PreviewCard_SetInfos"
    
    if(hud.isActionRunning ( this.getUser ( ) , sAction)) then hud.stopAction ( this.getUser ( ), sAction ) end
    hud.callAction ( this.getUser ( ), sAction , sName, sDescription, sImg  )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
