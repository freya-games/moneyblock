--------------------------------------------------------------------------------
--  Function......... : SetCameraState
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.SetCameraState ( nState )
--------------------------------------------------------------------------------
	
    local hScene = user.getScene ( this.getUser ( ) )
    local hCamera = user.getActiveCamera ( this.getUser ( ) )
    
    if(hScene and hCamera)then
        
       
        
        
        if(nState == 1)then
        
            local hB = scene.getTaggedObject ( hScene, "TargetCam_Blocks" )
            if(hB)then
                object.sendEvent ( hCamera,"Camera","onSetTarget", 1, hB, 0, 3, 5 )
            end
        
        end
        
        if(nState == 3)then
        
            local hB = scene.getTaggedObject ( hScene, "TargetCam_Rig" )
            if(hB)then
                object.sendEvent ( hCamera,"Camera","onSetTarget", 1, hB, 0, 1, 4 )
            end
        
        end
        
        if(nState == 4)then
        
            local hB = scene.getTaggedObject ( hScene, "TargetCam_Cards" )
            if(hB)then
                object.sendEvent ( hCamera,"Camera","onSetTarget", 1, hB, 0, 12, 0 )
            end
        
        end
    end

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
