--------------------------------------------------------------------------------
--  Function......... : EffectValueChange
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.EffectValueChange (nCardID ,sResourceIDOptional)
--------------------------------------------------------------------------------
local nTotalActionCard =  hashtable.get ( this.htRuntimeData ( ),"TotalActionCards")
if(nCardID<nTotalActionCard)
then
    local sTag
    if(nTotalActionCard>1)
    then
        sTag="ActionCards.ActionCard."..nCardID
    else
      
        sTag="ActionCards.ActionCard"
    end	
    
    
    local sType=hashtable.get ( this.htActionCards ( ),sTag..".Effect.Attributes.type")
        
    if(sType=="valuechange")
    then
        
        local sResourceType =hashtable.get ( this.htActionCards ( ),sTag..".Effect.Resource.Type")
        local sResourceID =hashtable.get ( this.htActionCards ( ),sTag..".Effect.Resource.ID")
        local sResourceToChange =hashtable.get ( this.htActionCards ( ),sTag..".Effect.Resource.ToChange")
        local sResourceTo =hashtable.get ( this.htActionCards ( ),sTag..".Effect.Resource.To")
     
 
   
        
        local tTagToChange=table.newInstance ( )
     
        sResourceIDOptional="DarkCoin"
      
        
        if(sResourceType==nil)
        then
            if(sResourceID=="PLAYER_CHOICE")
            then
                local  sTag="Game.Resources."..sResourceIDOptional.."."..sResourceToChange
                table.add ( tTagToChange,sTag )
            end
     
        else
            local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount" )
           
            for i=0, nCount-1
            do
                local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")
                if(sResourceType== sType)
                then
                    local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
                    local  sTag="Game.Resources."..sID.."."..sResourceToChange
                    table.add ( tTagToChange,sTag )
                end
            end
        
        end
        

        local bConditionEnable=  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Attributes.enable")
        if(bConditionEnable)
        then
            local sConditionValueResourceType =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Value.Resource.Type")
            local sConditionValueResourceID =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Value.Resource.ID")
            local sConditionValueResourceToWatch =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Value.Resource.ToWatch")
            local sConditionValueResourceOn =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Value.Resource.On")
            local sConditionValueFrom = hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Value.From")
            local sConditionOperatorType = hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Operator.Type")
            local sConditionOperatorValueType = hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Operator.Value.Type")
            local nConditionOperatorValueAmount = hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Operator.Value.Amount")
            local sConditionOperatorValueAmountType =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Condition.Operator.Value.Amount.Attributes.type")
            
            
            
            if(sConditionValueResourceType==nil)
            then
                if(sConditionValueResourceID=="PLAYER_CHOICE")
                then
                    local  sTag="Game.Resources."..sResourceIDOptional.."."..sResourceToChange
                    table.add ( tTagToChange,sTag )
                end
         
            else
                local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount" )
                local tTagToWatch=table.newInstance ( )
                for i=0, nCount-1
                do
                    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")
                    if(sResourceType== sType)
                    then
                        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
                        local  sTag="Game.Resources."..sID.."."..sResourceToChange
                        table.add ( tTagToWatch,sTag )
                    end
                end
            
            end
            
            hashtable.get( this.htRuntimeData ( ),"Game.Resources."..".Price")
            
            
            
            
        end
        
        
     
        
        local sOperatorType  =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Operator.Type")
        local nOperatorValue  =  hashtable.get ( this.htActionCards ( ),sTag..".Effect.Operator.Value")
    
       
        for i=0, table.getSize (  tTagToChange )-1
        do
            if(sOperatorType=="DIVIDE")
            then
                local sTag=table.getAt ( tTagToChange,i )
                
                local nValue=hashtable.get(this.htRuntimeData ( ),sTag)

                local nNewValue=nValue/nOperatorValue 
                
                if(sResourceTo=="GAME")
                then
                    hashtable.set (this.htRuntimeData ( ), sTag,nNewValue )
                end


            end
            
        end
       
        
        
    else
        log.error("[Error]AIMainMoneyBlock.EffectValueChange : Action card is not a valuechange effect " )
        return
    end
    
    log.message (sType )
    
else
    log.error ( "AIMainMoneyBlock.EffectValueChange : Action Card does not exist - ID : "..nCardID )
end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
