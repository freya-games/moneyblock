--------------------------------------------------------------------------------
--  Handler.......... : onKeyboardKeyDown
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onKeyboardKeyDown ( kKeyCode )
--------------------------------------------------------------------------------

    if(system.getClientType ( ) == system.kClientTypeEditor)then
    
        if(kKeyCode== input.kKeyLShift)then

            this.UpdateEndTurnHUD ( )
            this.EnableEndTurnHUD ( true )
        end

    else
        if(kKeyCode== input.kKeyTab)then

            this.UpdateEndTurnHUD ( )
            this.EnableEndTurnHUD ( true )
        end

    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
