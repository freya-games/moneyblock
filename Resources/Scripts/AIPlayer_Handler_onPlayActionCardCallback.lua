--------------------------------------------------------------------------------
--  Handler.......... : onPlayActionCardCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onPlayActionCardCallback ( nCardID,sCryptoID ,bCanPlay) 
--------------------------------------------------------------------------------
	
	if(bCanPlay)
    then
        this.openDialogBox ( "[Server] Action Card "..this.getActionCardName (  nCardID ).." played on "..this.getName ( sCryptoID ) )
        this.removeActionCard ( nCardID )
        
        user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onPlayActionCardCallback", nCardID,sCryptoID )
       
    else
        this.openDialogBox ( "[Server] Cannot Play Action Card "..this.getActionCardName (  nCardID ).."on "..this.getName ( sCryptoID ) )
    
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------


