--------------------------------------------------------------------------------
--  Handler.......... : onGetCryptoCurrentBlockCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetCryptoCurrentBlockCallback (   sResID,nCurrentBlock,nBlockCount )
--------------------------------------------------------------------------------
	
	if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sResID ))
    then
        hashtable.set ( this.htCryptoCurrentBlock  ( ),sResID, nCurrentBlock )
    else
        hashtable.add ( this.htCryptoCurrentBlock  ( ),sResID,nCurrentBlock )
    end
	
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetCryptoCurrentBlockCallback", sResID,nCurrentBlock,nBlockCount )
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
