--------------------------------------------------------------------------------
--  Function......... : SetPlayerHUDReady
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.SetPlayerHUDReady ( nPlayerIndex, bReady )
--------------------------------------------------------------------------------

    if(table.getSize ( this.tPrefixPlayerScoreHUD ( ))>nPlayerIndex )then

        if(bReady)then

            hud.callAction ( this.getUser ( ), table.getAt ( this.tPrefixPlayerScoreHUD ( ), nPlayerIndex )..".Ready" )

        else
            hud.callAction ( this.getUser ( ), table.getAt ( this.tPrefixPlayerScoreHUD ( ), nPlayerIndex )..".NotReady" )
        end

    end

    local nReady = this.GetPlayerReadyCount ( )
    local nMax = table.getSize ( this.tUsers ( ) )
    local hC = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelPlayersReady" )
    local hCMax = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelPlayersReadySuffix" )

    if(hC)then
        hud.setLabelText ( hC, ""..nReady )
    end
    if(hCMax)then
        hud.setLabelText ( hCMax, "/"..nMax.." PLAYERS READY"  )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
