--------------------------------------------------------------------------------
--  State............ : CheckTimer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CheckTimer_onLoop ( )
--------------------------------------------------------------------------------
	
	if(this.nCurPhaseTimeLim ( ))then
        this.nCurPhaseTimer ( this.nCurPhaseTimer ( ) + application.getLastFrameTime ( ) )
        local nRemaningTime = math.max(0, this.nCurPhaseTimeLim ( ) - this.nCurPhaseTimer ( ))
       
        local hC = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelTimer" )
        if(hC)then
        
            local nSecond = math.trunc ( math.mod ( nRemaningTime, 60 ), 0)
            local nMinute = math.floor (  nRemaningTime/60)
            local sPrefix =""
            local sPrefixSecond =":"
            if(nMinute<10)then sPrefix = "0"end
            if(nSecond<10)then sPrefixSecond = " 0"end
        
            hud.setLabelText ( hC, sPrefix..nMinute..sPrefixSecond..nSecond )
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
