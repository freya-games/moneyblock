--------------------------------------------------------------------------------
--  Function......... : EndGame
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.EndGame ( )
--------------------------------------------------------------------------------

this.Idle ( )
log.message ( "END GAME" )
this.sendScoresToAllPlayers ( )
this.stopPhaseTimer ( )



for nPlayerIndex=0,this.nPlayerCount ( )-1
do

    local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    local nScore=this.getPlayerCash ( table.getAt ( this.tUsers ( ),nPlayerIndex ))
    for j=0,nCount-1
    do
        local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then

            local nPrice = this.getResourcePrice ( sCryptoID )
            local nCount = hashtable.get ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sCryptoID..".Count" )
            nScore=nScore+ nPrice * nCount
        end
    end

    table.add ( this.tEndGameScore ( ),nScore )

end

local nWinScore = table.getAt ( this.tEndGameScore ( ),0 )
local nIndexWinScore=0
for nPlayerIndex=1,table.getSize ( this.tEndGameScore ( ) )-1
do
    local nScore=table.getAt ( this.tEndGameScore ( ),nPlayerIndex )
    if(nScore>nWinScore)
    then

        nWinScore=nScore
        nIndexWinScore=nPlayerIndex

    end
end


local nPlayerIndexWinner

local nCount=0
for nPlayerIndex=0,table.getSize ( this.tEndGameScore ( ) )-1
do
    local nScore=table.getAt ( this.tEndGameScore ( ),nPlayerIndex )
    if(nScore==nWinScore)
    then
        nCount=nCount+1




    end
end



local htSortedScoreTable=hashtable.newInstance ( )

local tSortedScoreTable=table.newInstance ( )

local tSortedPlayerIndexTable=table.newInstance ( )

for i=0,  table.getSize ( this.tEndGameScore ( ))-1
do
    hashtable.add ( htSortedScoreTable,""..i,table.getAt ( this.tEndGameScore ( ),i ) )

end
while hashtable.getSize (  htSortedScoreTable )>0
do
    local nWinScore = hashtable.getAt ( htSortedScoreTable,0 )
    local nIndexWinScore=0
    for nPlayerIndex=1, hashtable.getSize (  htSortedScoreTable )-1
    do
        local nScore=hashtable.getAt (  htSortedScoreTable,nPlayerIndex )
        if(nScore>nWinScore)
        then

            nWinScore=nScore
            nIndexWinScore=nPlayerIndex


        end

    end
    table.add ( tSortedScoreTable , nWinScore )

    table.add ( tSortedPlayerIndexTable, string.toNumber (  hashtable.getKeyAt (  htSortedScoreTable,nIndexWinScore) ))
      hashtable.remove( htSortedScoreTable,  hashtable.getKeyAt (  htSortedScoreTable,nIndexWinScore) )
end






local sScoreLeaderBoard=""

for i=0,table.getSize ( tSortedScoreTable )-1
do
      local sName=  hashtable.get ( this.htRuntimeData ( ),"Player."..table.getAt (tSortedPlayerIndexTable ,i )..".Name")
    sScoreLeaderBoard=sScoreLeaderBoard.."\n"..sName.." : "..table.getAt ( tSortedScoreTable,i )
end


for i=0,table.getSize ( tSortedScoreTable )-1
do
    local nScore=table.getAt (tSortedScoreTable,i )
    local nPlayerIndex=table.getAt (tSortedPlayerIndexTable,i )
    if(nScore==nWinScore)
    then
        if(nCount>1)
        then
            this.sendEndGameToPlayer(nPlayerIndex,"Draw",sScoreLeaderBoard)
        else
            this.sendEndGameToPlayer(nPlayerIndex,"Victory",sScoreLeaderBoard)
        end



    else

          this.sendEndGameToPlayer(nPlayerIndex,"Defeat",sScoreLeaderBoard)
    end
end



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------

