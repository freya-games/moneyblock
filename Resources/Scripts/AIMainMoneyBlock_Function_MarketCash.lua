--------------------------------------------------------------------------------
--  Function......... : MarketCash
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.MarketCash (nCardIndex )
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htMarketCards ( ), "MarketCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="MarketCards.MarketCard."..nCardIndex
    else

        sTag="MarketCards.MarketCard"
    end



    local nMin =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Min")
    if(nMin)
    then

        local nMax =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Max")

        local nAmount=nMin
        if(nMin~=nMax)
        then
            nAmount=math.roundToNearestInteger (  math.random ( nMin,nMax ))
        end

        for i =0, this.nPlayerCount ( )-1
        do

            this.changePlayerCash (i,nAmount )

        end

          this.sendMessageToAllUsers ( "[MARKET CARD] Cash Effect Applied, Amount "..nAmount)
    end

     local nMinRatio =hashtable.get (this.htMarketCards ( ),sTag..".Effect.MinRatio")
    if(nMinRatio)
    then
        local nMaxRatio =hashtable.get (this.htMarketCards ( ),sTag..".Effect.MaxRatio")

        local nRatio=nMinRatio
        if(nMinRatio~=nMaxRatio)
        then
            nRatio=math.random ( nMinRatio,nMaxRatio )

            --APPLY EFFECT ON CANCEL BUY
            hashtable.add(this.htCurrentBuyOrders ( ),"TaxOnCancel", 1-nRatio )
        end

        for i =0, this.nPlayerCount ( )-1
        do

            this.changePlayerCashRatio ( i,nRatio )

        end
        this.sendMessageToAllUsers ( "[MARKET CARD] Cash Effect Applied on with Ratio "..nRatio)

    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
