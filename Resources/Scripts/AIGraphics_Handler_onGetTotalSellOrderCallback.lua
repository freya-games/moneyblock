--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalSellOrderCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onGetTotalSellOrderCallback (sCryptoID ,nBuyAmount  )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htSellOrder ( ),sCryptoID ))
    then
        hashtable.set ( this.htSellOrder ( ),sCryptoID, nBuyAmount )
    else
        hashtable.add ( this.htSellOrder ( ),sCryptoID,nBuyAmount )
    end

    this.UpdateMarketHUD (  )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
