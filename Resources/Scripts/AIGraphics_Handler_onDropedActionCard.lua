--------------------------------------------------------------------------------
--  Handler.......... : onDropedActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onDropedActionCard ( sCardTag, nPointX, nPointY  )
--------------------------------------------------------------------------------
	
    
    --REMOVE ALL OTHER ACTIVE CARDS
    hashtable.empty ( this.htCurActiveCards ( ) )
    
    
    local hUser = this.getUser ( )
    
    hud.stopSound ( hUser,3)
    hud.playSound ( hUser,3,127,false )
    
    local hComponent = hud.getComponentAtPoint ( hUser, nPointX, nPointY )
    local bValid = false
    local hCard = hud.getComponent ( hUser, sCardTag )
    local nCardIndex, nCardID = this.GetCardIndex ( sCardTag )
    
    if(hCard== nil)then log.warning ( "onDropedActionCard, Card not found" )end
    
    
    --IS DROP VALID
    if(hComponent)then
        local sTag = hud.getComponentTag ( hComponent  )
        
        for i= 0 , hashtable.getSize ( this.htPrefixSlotCardHUD ( ) ) -1
        do
            if(sTag == hashtable.getAt ( this.htPrefixSlotCardHUD ( ), i )..".ContainerCard")then
            
                bValid = true
                
                --SET
                local sCryptoID = hashtable.getKeyAt ( this.htPrefixSlotCardHUD ( ), i )
                local nUserIndex = this.GetUserIndex ( user.getID ( this.getUser() ) )
                hashtable.add ( this.htCurActiveCards ( ), ""..nUserIndex , sCryptoID.."."..nCardID  )
                this.sTagCurUsedCard ( sCardTag )
                
                --SET CARD CONTAINER
                local hContainer = hud.getComponent ( hUser, sTag )
                local nSizeY = 70
                local nSizeX = nSizeY * this.nRatioCardHUD ( )
                
                hud.setComponentContainer ( hCard, hContainer )
                hud.setComponentSize ( hCard, nSizeX, nSizeY )
                hud.setComponentPosition( hCard, 50, 38 )
                break
            end
        end
    end
    
    
    if(hCard)then
        hud.setComponentIgnoredByMouse ( hCard, false )
    end
    
    --REMOVE CUR USED CARD
    if(not bValid and sCardTag == this.sTagCurUsedCard ( ) )then
        this.sTagCurUsedCard ( "")
    end
    
    this.UpdateActionCardsHUD ( )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
