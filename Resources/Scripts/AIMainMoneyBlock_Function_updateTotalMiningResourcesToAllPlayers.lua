--------------------------------------------------------------------------------
--  Function......... : updateTotalMiningResourcesToAllPlayers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.updateTotalMiningResourcesToAllPlayers ( sCryptoID)
--------------------------------------------------------------------------------
    for i =0, this.nPlayerCount ( )-1
    do
  
        local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
       
            if(this.getResourceType ( sCryptoID)=="CRYPTO")
            then
                for iR=0, nCount-1
                do
                    local sResID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
                    local sType=this.getResourceType ( sResID)
                    if(sType=="MINER" or sType=="ENERGY")
                    then
                        user.sendEvent ( application.getUser (  table.getAt ( this.tUsers ( ),i )),"AIPlayer","onGetTotalMiningResourceCallback",sCryptoID,sResID, this.getTotalMiningResource (  sCryptoID,sResID))
                      
                    end
                end
            end
        
	
    end
    

	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
