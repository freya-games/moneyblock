--------------------------------------------------------------------------------
--  Function......... : NextPhase
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.NextPhase ( )
--------------------------------------------------------------------------------
local nPhaseCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.ChildCount")
local nCurrentPhase=this.nPhase ( )
local bEndGame = false

--END GAME CONDITIONS
if(this.bBlockChainComplete ( ))then bEndGame = true end
--if(this.nValidatedBlockChainCount ( )>=1)then bEndGame = true end

if((nCurrentPhase+1)>nPhaseCount)
then
    if(bEndGame)
    then
        this.EndGame ( )
    else

        this.nTurn ( this.nTurn ( )+1)
        this.nPhase(0)
        this.giveBackMiningResourcesToPlayers ( )
        this.CleanBuffers ( )
    end
end

if(not bEndGame)then
    this.nPhase ( this.nPhase ( )+1)

    this.stopPhaseTimer ( )

    this.StartPhase ( )
    this.sendPhaseToPlayers ( )
end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
