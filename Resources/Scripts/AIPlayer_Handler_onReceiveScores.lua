--------------------------------------------------------------------------------
--  Handler.......... : onReceiveScores
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onReceiveScores ( sAllScore, sDelimiter, sAllTokens, sAllMoney )
--------------------------------------------------------------------------------

    --PARSE SCORES
    local tTemp = table.newInstance ( )
    string.explode (  sAllScore, tTemp, sDelimiter )


    --INIT TABLES
    if(table.getSize ( this.tPlayersCurScores ( ))==0 )then
        for i=0 , table.getSize ( tTemp ) -1
        do
            table.add ( this.tPlayersPrevScores ( ), 0 )
            table.add ( this.tPlayersCurScores ( ), 0 )
        end
    end


    --SET SCORES
    for i= 0, table.getSize ( tTemp ) -1
    do
        table.setAt ( this.tPlayersPrevScores ( ), i, table.getAt ( this.tPlayersCurScores ( ) ,i) )
        table.setAt ( this.tPlayersCurScores ( ), i, string.toNumber ( table.getAt ( tTemp ,i) ))

        --log.message ( " AI Player : Score Player "..i.." received "..table.getAt ( this.tPlayersPrevScores ( ) ,i).." -> "..table.getAt ( this.tPlayersCurScores ( ) ,i) )
    end

    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onReceiveScores", sAllScore, sDelimiter, sAllTokens, sAllMoney )



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
