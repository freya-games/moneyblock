--------------------------------------------------------------------------------
--  Function......... : validBlock
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.validBlock (sCryptoID,bValid )
--------------------------------------------------------------------------------
    if(hashtable.contains ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelSell"  ))
    then
      hashtable.set ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelSell",not bValid)
    else
     hashtable.add( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelSell",not bValid)
    end

   if(hashtable.contains ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelBuy"  ))
    then
      hashtable.set ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelBuy",not bValid)
    else
     hashtable.add( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".CancelBuy",not bValid)
    end


    if(bValid)
    then
        table.add ( this.tCurBlockMined ( ), sCryptoID )

        this.sendMiningRewards ( sCryptoID )


            local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )

             local nBlockChainCount =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".BlockCount"  )
             hashtable.set (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock",nCB+1  )

             if(nCB+1>=this.nValidatedBlockChainCount ( ))
             then
                this.nValidatedBlockChainCount ( nCB+1)
                log.message ( "BlockChain Validated "..sCryptoID )
             end

            if(this.nValidatedBlockChainCount ( )>=nBlockChainCount)then
                log.message ( "Block Chain Complete "..sCryptoID )
                this.bBlockChainComplete (true)
            end
    end



    for i=0, table.getSize ( this.tUsers ( ) )-1
    do
        local nCB =hashtable.get (this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".CurrentBlock"  )





           user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),i ) ),"AIPlayer","onValidBlock" ,sCryptoID,nCB,bValid)


    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
