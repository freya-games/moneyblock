--------------------------------------------------------------------------------
--  Handler.......... : onStartGame
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onStartGame ( sUsers, sDelimiter,nIndex  )
--------------------------------------------------------------------------------

    local nUserID = user.getID ( this.getUser ( ) )
    local sSuffixScene = ""
    if(nUserID>0)then sSuffixScene = "_"..nIndex end

    user.setScene ( this.getUser ( ),"MoneyBlock"..sSuffixScene )
    music.setVolume (user.getScene (   this.getUser ( )),0.2,0  )

    music.play (user.getScene (   this.getUser ( )) , 0,10)
    --SET USERS
    local hScene = user.getScene( this.getUser ( ) )
    string.explode ( sUsers, this.tUsers ( ), sDelimiter )

    --CREATE OBJ
--     local hA=scene.createRuntimeObject ( user.getScene ( this.getUser ( ) ), "BoxMan")
    local hBTarget=scene.createRuntimeObject (  user.getScene ( this.getUser ( ) ), "Dummy")
    local hRigTarget=scene.createRuntimeObject (  user.getScene ( this.getUser ( ) ), "Dummy")
    local hCardTarget=scene.createRuntimeObject (  user.getScene ( this.getUser ( ) ), "Dummy")
    object.setTranslation ( hBTarget, 0, -0.75, 0, object.kGlobalSpace )
    object.setTranslation ( hCardTarget, -1.8, -0.75, 0.9, object.kGlobalSpace )
    object.setTranslation ( hRigTarget, 0, 0.25, 0, object.kGlobalSpace )


    scene.setObjectTag ( hScene, hBTarget, "TargetCam_Blocks" )
    scene.setObjectTag ( hScene, hRigTarget, "TargetCam_Rig" )
    scene.setObjectTag ( hScene, hCardTarget, "TargetCam_Cards" )
    local hC=scene.createRuntimeObject (user.getScene ( this.getUser ( ) ), "Camera")

    this.hCamera ( hC )
    object.addAIModel ( this.hCamera ( ), "Camera" )
    user.setActiveCamera (  this.getUser ( ),this.hCamera ( ) )
    --object.sendEvent ( this.hCamera ( ),"Camera","onSetTarget", 1, hB, 0, 3, 5 )

    this.ParseMoneyBlockConfig ( )
    this.ParseActionCards ( )
    this.ParseMarketCards ( )
    this.getAllResourcesCount ( )
    this.getAllResourcesPrice ( )


   this.sendPlayerInitialized ( )


   user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onInitHUD", sUsers, sDelimiter )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
