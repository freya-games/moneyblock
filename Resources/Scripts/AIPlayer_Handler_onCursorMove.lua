--------------------------------------------------------------------------------
--  Handler.......... : onCursorMove
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onCursorMove (  nAxisX, nAxisY )
--------------------------------------------------------------------------------

    if(math.abs (  nAxisX)>this.nPadTreshold ( ) )
    then
        this.nMouseDeltaX ( nAxisX)
    else
        this.nMouseDeltaX ( 0)

    end
    if(math.abs (  nAxisY)>this.nPadTreshold ( ) )
    then
        this.nMouseDeltaY ( nAxisY)
    else
        this.nMouseDeltaY ( 0)

    end


	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
