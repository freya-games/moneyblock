--------------------------------------------------------------------------------
--  Function......... : UpdateMiningResourceSliders
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningResourceSliders ( )
--------------------------------------------------------------------------------

    local hUser = this.getUser ( )
    local nMaxAmount = 10 --TODO EXTERNALIZE VALUE

     -- GET CUR MINING RESOURCE
        for i=0 ,  hashtable.getSize ( this.htPrefixMiningSliderHUD ( ) ) -1
        do
            local vValue = 0
            local sResID = hashtable.getKeyAt ( this.htPrefixMiningSliderHUD ( ), i )
            local nRequiredAmount = this.GetRequiredResourceToMine ( sResID, this.sCurMiningCryptoID ( ))
            local nReminingAmount = this.GetReminigResourceToMine( sResID, this.sCurMiningCryptoID ( ))
            local nRange = math.min ( nReminingAmount, nMaxAmount  )

            if(hashtable.contains (this.htMining ( ),this.sCurMiningCryptoID ( ).."."..sResID  ))then
                vValue =  hashtable.get (this.htMining ( ),this.sCurMiningCryptoID ( ).."."..sResID  )
            end

             --UPDATE TEXT
            local sActionTag = hashtable.getAt( this.htPrefixMiningSliderHUD(), i)..".SetCurUnits"
            if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
            hud.callAction ( hUser, sActionTag , ""..vValue)

            --UPDATE MAX
            local nSliderSize =  math.min (100,nReminingAmount/nMaxAmount *100)
            --local nSliderCurValue = math.min (100, vValue/nMaxAmount*100)
            local nRequiredAmountBarSize = math.min (100, nRequiredAmount/nMaxAmount*100)

            sActionTag = hashtable.getAt( this.htPrefixMiningSliderHUD(), i)..".SetMaxUnits"
            if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
            -- PARAM = TEXT DISPLAY, SLIDER SIZE (/100), MINIMUM REQUIRED VALUE
            hud.callAction ( hUser, sActionTag , "/"..nRequiredAmount, nSliderSize , nRequiredAmountBarSize)


            --UPDATE SLIDER
            local sPrefix = hashtable.getAt( this.htPrefixMiningSliderHUD(), i)
            local hC = hud.getComponent ( hUser, sPrefix..".SliderResource" )
            if(hC)
                then hud.setSliderRange ( hC, 0, nRange )
                hud.setSliderValue ( hC, vValue )
            end
            --UPDATE PROGRESS BAR
            local hP = hud.getComponent ( hUser, sPrefix..".ProgressResource" )
             if(hP) then hud.setProgressValue ( hP,  math.min ( 255,vValue/nRange*255)   ) end



            --DECREASE OPACITY BUTTON +
            local nOpacity = 255
            --if(nReminingAmount==0)then
                --nOpacity = 127
            --end
            local sTag = hashtable.getAt( this.htPrefixMiningSliderHUD ( ), i)..".ButtonAdd"
            local hButton = hud.getComponent ( hUser, sTag )
            if(hButton)then  hud.setComponentOpacity ( hButton, nOpacity ) end



    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
