--------------------------------------------------------------------------------
--  Function......... : ActionNormalDistribution
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionNormalDistribution ( nCardIndex)
--------------------------------------------------------------------------------

 local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end
    local sType =hashtable.get (this.htActionCards ( ),sTag..".Effect.Attributes.type")
    if(sType=="binormal")
    then
        local nExpectation1 =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution1.Expectation")

        local nDeviation1 =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution1.Deviation")

        local nExpectation2 =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution2.Expectation")

        local nDeviation2 =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution2.Deviation")

        if(nExpectation1 and nDeviation1 and nExpectation2 and nDeviation2)
        then
            if(math.roundToNearestInteger ( math.random ( 0,1 ) )==0)
            then
                return math.gaussianRandom (  nExpectation1,nDeviation1)
            else
                return math.gaussianRandom (  nExpectation2,nDeviation2)
            end
        else
            return nil
        end


    end

    if(sType=="normal")
    then
       local nExpectation =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution.Expectation")

        local nDeviation =hashtable.get (this.htActionCards ( ),sTag..".Effect.Distribution.Deviation")

        if(nExpectation and nDeviation)
        then
            return math.gaussianRandom (  nExpectation,nDeviation)
        else
            return nil
        end


    end


    return nil

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
