--------------------------------------------------------------------------------
--  Function......... : UpdateMiningResourceInfos
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningResourceInfos ( sResID, nResourceCount, nResourcePrice )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    
    local sActionTag = hashtable.get ( this.htPrefixMiningResourceHUD ( ), sResID )..".SetInfosResource"
    if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
    hud.callAction ( hUser, sActionTag, ""..nResourceCount, ""..nResourcePrice..this.GetCurrency (  ) )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
