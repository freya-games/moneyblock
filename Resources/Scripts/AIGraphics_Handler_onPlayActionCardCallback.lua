--------------------------------------------------------------------------------
--  Handler.......... : onPlayActionCardCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onPlayActionCardCallback ( nCardID,sCryptoID )
--------------------------------------------------------------------------------
	
    if(this.bUseDebugHUD ( ))then
    
	local hComponentButton = hud.getComponent ( this.getUser ( ), "PlayerHUD.ActionPlayCard" )
    hud.setComponentVisible ( hComponentButton,false )
    
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
