--------------------------------------------------------------------------------
--  Handler.......... : onReceiveText
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Chat.onReceiveText ( nUserID, sPlayerName, sText )
--------------------------------------------------------------------------------

    --add it in the chat history
    local hChatHistory = hud.getComponent ( this.getUser ( ), "Network_Chat.History" )
    if ( hChatHistory )
    then
        local nNewIndex = hud.addListItem ( hChatHistory, " <"..sPlayerName.."> "..sText )
        hud.setListVerticalScrollPos ( hChatHistory, 255 )
    end

    --draw text above the avatar head
    local s = application.getCurrentUserScene ( )
    if ( s )
    then
        local hPlayerAvatar =  scene.getTaggedObject ( s, "AvatarOf"..nUserID )
        if ( hPlayerAvatar )
        then
            object.sendEvent ( hPlayerAvatar, "Network_Character_HUD", "onSetHUD_Text", sText )
        end
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
