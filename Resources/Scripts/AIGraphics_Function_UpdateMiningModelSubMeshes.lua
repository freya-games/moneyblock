--------------------------------------------------------------------------------
--  Function......... : UpdateMiningModelSubMeshes
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningModelSubMeshes ( )
--------------------------------------------------------------------------------
	
    --FOR ALL CRYPTO
	for i=0 , hashtable.getSize ( this.htMiningModel ( ) )-1
    do
        local sCryptoID = hashtable.getKeyAt ( this.htMiningModel ( ), i )
        local hObj = hashtable.getAt ( this.htMiningModel ( ), i )
    
        if(hObj)then
            --FOR ALL RESOURCES
            for j=0, hashtable.getSize ( this.htResourcePrice ( ) )-1
            do
                local sResID = hashtable.getKeyAt ( this.htResourcePrice ( ), j )
                
                --GET RESOURCE COUNT
                if(this.GetResourceType ( sResID )~="CRYPTO")then
                    local nCount = hashtable.get ( this.htMining ( ), sCryptoID.."."..sResID )
                    if(not nCount)then nCount = 0 end
                        
                    --DISPLAY RESOURCE COUNT
                    for k=0 , object.getChildCount ( hObj )-1
                    do

                        local hChild = object.getChildAt ( hObj, k )
                        local sName = shape.getMeshName ( hChild )
                        

                        if(string.contains ( sName, "Cell" ) and sResID =="Power")then
                       
                            local bVisible = false
                            local tab = table.newInstance ( )
                            string.explode ( sName, tab,"_" )
                            local nID = string.toNumber ( table.getLast ( tab ))
                            if(nID<=nCount)then bVisible = true end

                            object.setVisible ( hChild, bVisible )

                            
                        elseif(string.contains ( sName, "Bay" ) and sResID =="CPU")then
                            
                            local bVisible = false
                            local tab = table.newInstance ( )
                            string.explode ( sName, tab,"_" )
                            local nID = string.toNumber ( table.getLast ( tab ))
                            if(nID<=nCount)then bVisible = true end

                            object.setVisible ( hChild, bVisible )
                        end
                    end
                end
            end
        end
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
