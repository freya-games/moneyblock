--------------------------------------------------------------------------------
--  Function......... : sendScoresToAllPlayers
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendScoresToAllPlayers ( )
--------------------------------------------------------------------------------

local sAllScore = ""
local sAllMoney = ""
local sAllTokens = ""
local sDelimiter = "_"

for nPlayerIndex=0,this.nPlayerCount ( )-1
do

    local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    local nScore=this.getPlayerCash ( table.getAt ( this.tUsers ( ),nPlayerIndex ))
    local nTotalTokens = 0
    sAllMoney = sAllMoney..nScore..sDelimiter

    for j=0,nCount-1
    do
        local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
        if(this.getResourceType ( sCryptoID)=="CRYPTO")
        then

            local nPrice = this.getResourcePrice ( sCryptoID )
            local nCount = hashtable.get ( this.htRuntimeData ( ), "Player."..nPlayerIndex.."."..sCryptoID..".Count" )
            nTotalTokens = nTotalTokens+nCount
            nScore=nScore+ nPrice * nCount
        end
    end

    sAllTokens = sAllTokens..nTotalTokens..sDelimiter
    sAllScore = sAllScore..nScore..sDelimiter
end

--SEND
for nPlayerIndex=0,this.nPlayerCount ( )-1
do
    user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex) ),"AIPlayer","onReceiveScores", sAllScore, sDelimiter, sAllTokens, sAllMoney)
end



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
