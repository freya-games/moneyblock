--------------------------------------------------------------------------------
--  Function......... : EnableBlockChainModel
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableBlockChainModel ( bEnable )
--------------------------------------------------------------------------------

    local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
	
    local nCrypto = hashtable.getSize ( this.htBlockChainCount ( ) )
    for i=0 , nCrypto-1
    do
        local sCryptoID = hashtable.getKeyAt ( this.htBlockChainCount ( ), i)
        local nBlockCount = hashtable.getAt ( this.htBlockChainCount ( ), i)
        
        for j=1 , nBlockCount
        do
            local hBlock = scene.getTaggedObject ( hScene, sCryptoID.."."..j )
            if(hBlock)then
                object.setVisible ( hBlock, bEnable )
            end
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
