--------------------------------------------------------------------------------
--  Function......... : ParseMarketCards
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.ParseMarketCards ( )
--------------------------------------------------------------------------------

if(this.xMarketCards ( )~="")
then

    local hRootElement = xml.getRootElement ( this.xMarketCards ( ) )


    if ( hRootElement )
    then
        local ht=hashtable.newInstance ( )
        this.RecurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htMarketCards ( ),ht)

    end



end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
