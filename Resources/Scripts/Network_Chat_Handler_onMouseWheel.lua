--------------------------------------------------------------------------------
--  Handler.......... : onMouseWheel
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Chat.onMouseWheel ( nDelta )
--------------------------------------------------------------------------------
	
    local hChatHistory = hud.getComponent ( this.getUser ( ), "Network_Chat.History" )
    if ( hChatHistory  and hud.isComponentVisible ( hChatHistory )  ) 
    then
        local nMouseX, nMouseY = hud.getCursorPosition ( this.getUser ( ) )
        if ( ( nMouseX < 30 ) and  ( nMouseY < 16 ) )
        then
            hud.setListVerticalScrollPos ( hChatHistory , hud.getListVerticalScrollPos ( hChatHistory ) - nDelta*22)
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
