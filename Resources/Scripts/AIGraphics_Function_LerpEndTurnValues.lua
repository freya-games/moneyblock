--------------------------------------------------------------------------------
--  Function......... : LerpEndTurnValues
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.LerpEndTurnValues ( )
--------------------------------------------------------------------------------

  --UPDATE LERP VALUES
    local nDt = application.getLastFrameTime ( )
    this.nTimeLerpEndTurnValues ( this.nTimeLerpEndTurnValues ( ) - nDt )
    local nFactor = 1 - this.nTimeLerpEndTurnValues ( )/ this.nTimeTotalLerpEndTurnValues ( )
    math.clamp ( nFactor, 0, 1 )

    for nIndexPlayer=0 , hashtable.getSize ( this.htPrefixEndTurnPlayerHUD ( ) ) -1
    do
        local sPrefix = hashtable.getAt ( this.htPrefixEndTurnPlayerHUD ( ), nIndexPlayer )
        local hCash = hud.getComponent ( this.getUser ( ), sPrefix..".LabelPlayerScore" )
        local hTokens = hud.getComponent ( this.getUser ( ), sPrefix..".LabelPlayerTokens" )

        local _nCash = 0
        local nOldCash = 0
        local nTokens = 0
        local nOldTokens = 0

        --GET OLD AND CUR VALUES
        if(nIndexPlayer<table.getSize ( this.tPlayersCurScores ( ) ))then
            _nCash = table.getAt ( this.tPlayersCurScores ( ), nIndexPlayer )
        end
        if(nIndexPlayer<table.getSize ( this.tPlayersTotalTokens ( ) ))then
            nTokens = table.getAt ( this.tPlayersTotalTokens ( ), nIndexPlayer )
        end
         if(nIndexPlayer<table.getSize ( this.tPlayersPrevScores ( ) ))then
            nOldCash = table.getAt ( this.tPlayersPrevScores ( ), nIndexPlayer )
        end
        if(nIndexPlayer<table.getSize ( this.tPlayersPrevTotalTokens ( ) ))then
            nOldTokens = table.getAt ( this.tPlayersPrevTotalTokens ( ), nIndexPlayer )
        end

        --SET NEW VALUES
        local nNewValueCash = math.floor( math.interpolate ( nOldCash, _nCash, nFactor ))
        local nNewValueTokens =math.floor( math.interpolate ( 0, _nCash - nOldCash, nFactor ))

        --GET CASH COLOR
        local r = 50
        local g = 50
        local b = 50
        local sSuffix = "+"

        if(_nCash>nOldCash)then
            r = 0
            g = 127
            b = 0

        elseif(_nCash<nOldCash)then
            r = 127
            g = 0
            b = 0
             sSuffix = ""
        end



        if(hCash)then hud.setLabelText ( hCash, ""..nNewValueCash ) end
        if(hTokens)then hud.setLabelText ( hTokens, sSuffix..nNewValueTokens ) end
        if(hTokens)then  hud.setComponentForegroundColor ( hTokens, r, g, b, 255 ) end
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
