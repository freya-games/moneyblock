--------------------------------------------------------------------------------
--  Handler.......... : onGetTotalSellOrderCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetTotalSellOrderCallback (sCryptoID ,nBuyAmount  )
--------------------------------------------------------------------------------
	
if(hashtable.contains ( this.htSellOrder ( ),sCryptoID ))
    then
        hashtable.set ( this.htSellOrder ( ),sCryptoID, nBuyAmount )
    else
        hashtable.add ( this.htSellOrder ( ),sCryptoID,nBuyAmount )
    end

user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetTotalSellOrderCallback", sCryptoID ,nBuyAmount  )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
