--------------------------------------------------------------------------------
--  Function......... : GetActionCardInfos
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetActionCardInfos(_CardID,sName )
--------------------------------------------------------------------------------
	
local nTotalCard =  hashtable.get ( this.htActionCards ( ),"ActionCards.ChildCount")

    local sTag
    if(nTotalCard>1)
    then
        sTag="ActionCards.ActionCard.".._CardID 
    else
      
        sTag="ActionCards.ActionCard"
    end	
    
    
    local sName=hashtable.get ( this.htActionCards ( ),sTag.."."..sName)
    return sName
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
