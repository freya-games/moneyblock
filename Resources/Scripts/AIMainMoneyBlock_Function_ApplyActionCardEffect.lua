--------------------------------------------------------------------------------
--  Function......... : ApplyActionCardEffect
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ApplyActionCardEffect (nCardIndex,sCryptoID,nPlayerIndex,bEndTurn )
--------------------------------------------------------------------------------

    --log.error ( nCardIndex, "  ", sCryptoID,"  ", nPlayerIndex," ",bEndTurn  )
    local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end

    local sEffectType =hashtable.get (this.htActionCards ( ),sTag..".Effect.Attributes.type")
    --log.warning ( sEffectType )

    if(bEndTurn)
    then
        if(sEffectType=="attack51")
        then
            this.ActionAttack51 ( nCardIndex, sCryptoID )

        elseif(sEffectType=="nochangecryptocount")
        then
            this.ActionNoChangeCryptoCount (  nCardIndex,sCryptoID,nPlayerIndex)
        end
    else
        if(sEffectType=="cancelsell")
        then
            this.ActionCancelSell ( sCryptoID )

        elseif(sEffectType=="nopriceupdate")
        then
            this.ActionNoPriceUpdate ( nCardIndex,sCryptoID )
        elseif(sEffectType=="cryptoCount")
        then
                    this.ActionCryptoCount (   nCardIndex,sCryptoID)
        elseif(sEffectType=="energyCount")
        then
            this.ActionEnergyCount (   nCardIndex,sCryptoID,nPlayerIndex)

        elseif(sEffectType=="minerCount")
        then
            this.ActionMinerCount (   nCardIndex,sCryptoID,nPlayerIndex)

        end
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
