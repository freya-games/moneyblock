--------------------------------------------------------------------------------
--  Function......... : CreateMiningHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateMiningHUD (  )
--------------------------------------------------------------------------------

    --CREATE MINING HUD
    local nResourceCount = hashtable.getSize ( this.htPrefixMiningSliderHUD ( ) )
    local hUser = this.getUser ( )
    this.sPrefixMiningHUD("MiningHUD")
    hud.newTemplateInstance ( hUser, this.sPrefixMiningHUD(), this.sPrefixMiningHUD() )
    local hContainerResources = hud.getComponent ( hUser, this.sPrefixMiningHUD()..".ContainerMiningResources" )
    local hContainerOrderBook = hud.getComponent ( hUser, this.sPrefixMiningHUD()..".ContainerOrderBook" )

    local nSpaceResource = 10

    --ADD ALL RESOURCES HUD
    if(hContainerResources)then
        for i = 0, nResourceCount -1
        do
            local sResID = hashtable.getKeyAt ( this.htPrefixMiningSliderHUD ( ), i )
            --local r, g, b = this.GetResourceColor ( sResID )
            local sLogo = this.GetResourceLogo ( sResID )

            --CREATE HUD
            local sPrefixResourceHUD = "MiningResource_"..i
            hashtable.set ( this.htPrefixMiningSliderHUD ( ), sResID, sPrefixResourceHUD )
            hud.newTemplateInstance ( hUser, "MiningResource", sPrefixResourceHUD)
            local hChild = hud.getComponent ( hUser, sPrefixResourceHUD..".ContainerGlobal" )
            local hCSprite = hud.getComponent ( hUser, sPrefixResourceHUD..".SpriteResource" )
            local hCName = hud.getComponent ( hUser, sPrefixResourceHUD..".LabelResourceName" )

            --SET HUD TRANS
            hud.setComponentContainer ( hChild, hContainerResources)
            hud.setComponentPosition ( hChild, 50, 100- (i+0.5)*100/(nResourceCount) )
            hud.setComponentSize ( hChild, 100, 100/nResourceCount -  nSpaceResource )
            --if(hCSprite)then hud.setComponentBackgroundColor ( hCSprite, r, g, b, 255 ) end
            if(hCSprite)then hud.setComponentBackgroundImage ( hCSprite, sLogo ) end
            if(hCName)then hud.setLabelText ( hCName, this.GetResourceName ( sResID ) ) end

        end
    end

    --ADD CUR ORDER BOOK
    if(hContainerOrderBook)then

        --CREATE HUD
        local sPrefixOrderBookHUD = "OrderBook"
        hud.newTemplateInstance ( hUser, sPrefixOrderBookHUD, sPrefixOrderBookHUD)
        local hChild = hud.getComponent ( hUser, sPrefixOrderBookHUD..".ContainerGlobal" )

        --SET HUD TRANS
        hud.setComponentContainer ( hChild, hContainerOrderBook)
        hud.setComponentSize ( hChild, 100, 100 )
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
