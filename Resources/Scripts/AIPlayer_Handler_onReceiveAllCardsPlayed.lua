--------------------------------------------------------------------------------
--  Handler.......... : onReceiveAllCardsPlayed
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onReceiveAllCardsPlayed ( sAllInfos, sDelimiterPlayer, sDelimiterInfos  )
--------------------------------------------------------------------------------
	
    hashtable.empty ( this.htCardPlayed ( ) )
    
    --PARSE PLAYERS
	local tPlayedCard = table.newInstance ( )
    string.explode ( sAllInfos, tPlayedCard, sDelimiterPlayer )
    
    --PARSE CARD INFOS
    for i=0 , table.getSize ( tPlayedCard )-1 
    do
    	local tCardInfos = table.newInstance ( )
        string.explode ( table.getAt ( tPlayedCard, i ), tCardInfos, sDelimiterInfos )
        
        local nPlayerIndex, sCrypto, nCardID = table.getRangeAt ( tCardInfos, 0, 3  )
        local nPlayerID = table.getAt ( this.tUsers ( ), string.toNumber ( nPlayerIndex ) )
        
        --STORE
        hashtable.add ( this.htCardPlayed ( ), nPlayerID, sCrypto.."."..nCardID )
    end
    
    
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onReceiveAllCardsPlayed", sAllInfos, sDelimiterPlayer, sDelimiterInfos  )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
