--------------------------------------------------------------------------------
--  Function......... : PickRandomActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.PickRandomActionCard ( )
--------------------------------------------------------------------------------
local nPickIndex = -1

if(table.getSize ( this.tActionCardDrawIndex ( ) )<=0)
then
     this.NewActionDrawCard ( )
end
if(table.getSize ( this.tActionCardDrawIndex ( ) )>0)
then
local nIndex=math.roundToNearestInteger (  math.random (0, table.getSize ( this.tActionCardDrawIndex ( ) )-1 ))
    nPickIndex=table.getAt(this.tActionCardDrawIndex ( ),nIndex)
        
    table.removeAt ( this.tActionCardDrawIndex ( ),nIndex )

end
return nPickIndex
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
