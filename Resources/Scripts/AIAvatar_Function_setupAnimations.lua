--------------------------------------------------------------------------------
--  Function......... : setupAnimations
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIAvatar.setupAnimations ( )
--------------------------------------------------------------------------------
	
    -- Animations must be arranged as follows :
	-- 
    -- 0 - Idle
    -- 1 - Walk
    -- 2 - Run
    --
    local hObject = this.getObject ( )
    
    if ( object.hasController ( hObject, object.kControllerTypeAnimation ) )
    then
        animation.changeClip                ( hObject, 0, 0 )
        animation.changeClip                ( hObject, 1, 1 )
        animation.changeClip                ( hObject, 2, 2 )
        animation.changeClip                ( hObject, 3, 3 )
        animation.changeClip                ( hObject, 4, 4 )
        
        animation.setPlaybackKeyFrameBegin  ( hObject, 0, animation.getClipKeyFrameRangeMin ( hObject, 0 ) )
        animation.setPlaybackKeyFrameBegin  ( hObject, 1, animation.getClipKeyFrameRangeMin ( hObject, 1 ) )
        animation.setPlaybackKeyFrameBegin  ( hObject, 2, animation.getClipKeyFrameRangeMin ( hObject, 2 ) )
        animation.setPlaybackKeyFrameBegin  ( hObject, 3, animation.getClipKeyFrameRangeMin ( hObject, 3 ) )
        animation.setPlaybackKeyFrameBegin  ( hObject, 4, animation.getClipKeyFrameRangeMin ( hObject, 4 ) )
        
        animation.setPlaybackKeyFrameEnd    ( hObject, 0, animation.getClipKeyFrameRangeMax ( hObject, 0 ) )
        animation.setPlaybackKeyFrameEnd    ( hObject, 1, animation.getClipKeyFrameRangeMax ( hObject, 1 ) )
        animation.setPlaybackKeyFrameEnd    ( hObject, 2, animation.getClipKeyFrameRangeMax ( hObject, 2 ) )
        animation.setPlaybackKeyFrameEnd    ( hObject, 3, animation.getClipKeyFrameRangeMax ( hObject, 3 ) )
        animation.setPlaybackKeyFrameEnd    ( hObject, 4, animation.getClipKeyFrameRangeMax ( hObject, 4 ) )
    
        animation.setPlaybackMode           ( hObject, 0, animation.kPlaybackModeLoopMirrored )
        animation.setPlaybackMode           ( hObject, 3, animation.kPlaybackModeOnce )
        animation.setPlaybackMode           ( hObject, 4, animation.kPlaybackModeOnce )
        
        animation.setPlaybackIgnoreIfCursorOutOfRange ( hObject, 3, false )
        animation.setPlaybackIgnoreIfCursorOutOfRange ( hObject, 4, false )
    else
        log.warning ( "ClickAndGo_Character : Missing animation bank for object ", this.getObject ( ) )
    end
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
