--------------------------------------------------------------------------------
--  Function......... : UpdateMiningHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningHUD (  )
--------------------------------------------------------------------------------

if(not this.bUseDebugHUD ( ))then

    local hUser = this.getUser ( )

    this.UpdateMiningModelSubMeshes ( )

    if(this.sCurMiningCryptoID ( )~="")then

        --UPDATE CUR CRYPTO
        --local r, g, b = this.GetResourceColor ( this.sCurMiningCryptoID ( ) )
        local sLogo = this.GetResourceLogo ( this.sCurMiningCryptoID ( ) )
        local sActionTag = this.sPrefixMiningHUD ( )..".SetSprite"
        if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
        hud.callAction ( hUser, sActionTag, sLogo )


        --UPDATE BLOCK LVL
        local nBlockLvl = 0
        if(hashtable.contains ( this.htCryptoCurrentBlock ( ), this.sCurMiningCryptoID ( ) ))then
            nBlockLvl = hashtable.get ( this.htCryptoCurrentBlock ( ), this.sCurMiningCryptoID ( ) )
        end
        sActionTag = this.sPrefixMiningHUD ( )..".SetBlockLvl"
        if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
        hud.callAction ( hUser, sActionTag, ""..nBlockLvl + 1 )

        local sValidation = this.GetBlockChainValidation ( this.sCurMiningCryptoID () ,  nBlockLvl)
        local tRes = table.newInstance ( )
        string.explode ( sValidation, tRes, "-R"  )
        local sReward = table.getLast ( tRes )

        sActionTag = this.sPrefixMiningHUD ( )..".SetAdvice"
        if(hud.isActionRunning ( hUser, sActionTag  ))then hud.stopAction ( hUser, sActionTag  )  end
        hud.callAction ( hUser, sActionTag, sReward.." Tokens Rewarded" )


        --UPDATE ORDER BOOK
        this.UpdateOrderBooksHUD (  )
        this.UpdateMiningResourceSliders ( )
    end

else
----------------------------------DEBUG HUD----------------------------

    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningCrypto" )
    hud.removeListAllItems ( hComponent )
    table.empty ( this.tMiningCryptoHUD ( ) )


    for i=0,hashtable.getSize ( this.htResourceCount ( ) )-1
    do

        local sID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
        local nCount=hashtable.getAt ( this.htResourceCount ( ),i )

        local sID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
        local nPrice=hashtable.getAt ( this.htResourcePrice ( ),i )
        if(nPrice)
        then
            local sType=this.GetResourceType ( sID)
            if(sType=="CRYPTO")
            then

                local nIndex=hud.addListItem ( hComponent,this.GetResourceName (  sID) )
                if(nCount)
                then
                    hud.setListItemTextAt ( hComponent, nIndex, 1,""..nCount )
                else
                    hud.setListItemTextAt ( hComponent, nIndex, 1,"0" )
                end
                table.add (  this.tMiningCryptoHUD ( ),sID)
                local nPrice=hashtable.get( this.htResourcePrice ( ),sID )

                if(nPrice)
                then
                      hud.setListItemTextAt ( hComponent, nIndex, 2,""..nPrice..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")   )
                end

                if(nPrice)
                then
                    if(nCount)
                    then
                      hud.setListItemTextAt ( hComponent, nIndex, 3,""..(nPrice*nCount)..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")   )
                    else
                      hud.setListItemTextAt ( hComponent, nIndex, 3,"0"..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")   )
                      end
                end
                local nBlock=hashtable.get ( this.htCryptoCurrentBlock ( ),sID )
                if(nBlock)
                then
                    local sValidation=this.GetBlockChainValidation (sID,nBlock)
                    if(sValidation)
                    then
                       hud.setListItemTextAt ( hComponent,  nIndex, 4,sValidation)
                    end
                end
            end
        end
    end
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
