--------------------------------------------------------------------------------
--  Function......... : sendCardsPlayedToAllPlayers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendCardsPlayedToAllPlayers ( )
--------------------------------------------------------------------------------
    local sDelimiterPlayer ="_"
    local sDelimiterInfos ="."
    local sAllInfos = ""
    
    --GET ALL CARDS PLAYED
    for nPlayerIndex=0 , table.getSize ( this.tUsers ( ) ) -1
    do
        local sKeyCard = "Player."..nPlayerIndex..".ActionCard.ID"
        local sKeyTarget = "Player."..nPlayerIndex..".ActionCard.CryptoTarget"
        
        if(hashtable.contains ( this.htPlayerActionCards ( ), sKeyCard ))then
            if(hashtable.contains ( this.htPlayerActionCards ( ), sKeyTarget ))then
        
        
                local sCardID = hashtable.get ( this.htPlayerActionCards ( ), sKeyCard )
                local sCrypto = hashtable.get ( this.htPlayerActionCards ( ), sKeyTarget )
                
                sAllInfos = sAllInfos..nPlayerIndex..sDelimiterInfos..sCrypto..sDelimiterInfos..sCardID..sDelimiterPlayer
            end
        end
    end
    
    --SEND TO ALL PLAYERS
    for i=0 , table.getSize ( this.tUsers ( ) )-1 
    do
        local nPlayerID= table.getAt ( this.tUsers ( ), i )
    	user.sendEvent ( application.getUser ( nPlayerID ), "AIPlayer", "onReceiveAllCardsPlayed", sAllInfos, sDelimiterPlayer, sDelimiterInfos  )
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
