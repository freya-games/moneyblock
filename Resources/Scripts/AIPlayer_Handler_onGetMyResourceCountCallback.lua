--------------------------------------------------------------------------------
--  Handler.......... : onGetMyResourceCountCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onGetMyResourceCountCallback (sResID,nCount  )
--------------------------------------------------------------------------------
	
	if(hashtable.contains ( this.htResourceCount ( ),sResID ))
    then
        hashtable.set ( this.htResourceCount ( ),sResID, nCount )
    else
        hashtable.add ( this.htResourceCount ( ),sResID,nCount )
    end
	
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onGetMyResourceCountCallback", sResID,nCount )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
