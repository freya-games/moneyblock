--------------------------------------------------------------------------------
--  Function......... : SetTarget
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Camera.SetTarget ( endX, endY, endZ, endRX, endRY, endRZ )
--------------------------------------------------------------------------------
	
    
    --SET START TRANSFORM
    local hObj = this.getObject ( )
    local x, y, z = object.getTranslation ( hObj, object.kGlobalSpace )
    local rx, ry, rz = object.getRotation ( hObj, object.kGlobalSpace )
    
    table.empty ( this.tLerpStartTransform ( ) )
    table.add ( this.tLerpStartTransform ( ), x  )
    table.add ( this.tLerpStartTransform ( ), y  )
    table.add ( this.tLerpStartTransform ( ), z  )
    table.add ( this.tLerpStartTransform ( ), rx  )
    table.add ( this.tLerpStartTransform ( ), ry  )
    table.add ( this.tLerpStartTransform ( ), rz  )
    
    --SET END TRANSFORM
    table.empty ( this.tLerpEndTransform ( ) )
    table.add ( this.tLerpEndTransform ( ), endX  )
    table.add ( this.tLerpEndTransform ( ), endY  )
    table.add ( this.tLerpEndTransform ( ), endZ  )
    table.add ( this.tLerpEndTransform ( ), endRX  )
    table.add ( this.tLerpEndTransform ( ), endRY  )
    table.add ( this.tLerpEndTransform ( ), endRZ  )
    
    --
    this.LerpTarget ( )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
