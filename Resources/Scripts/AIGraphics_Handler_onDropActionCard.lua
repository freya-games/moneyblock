--------------------------------------------------------------------------------
--  Handler.......... : onDropActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onDropActionCard ( sCardTag )
--------------------------------------------------------------------------------
	
    this.sTagDraggedCard ( "" )
    local hUser = this.getUser ( )
	
if(this.nPhase ( )==4)then
    
    --Disable mouse interaction on CARD 
    local hCard = hud.getComponent ( hUser, sCardTag )
    if(hCard)then
        hud.setComponentIgnoredByMouse ( hCard, true )
    end
    
    local nPosX, nPosY = hud.getCursorPosition ( hUser )
    this.sendEvent ( "onDropedActionCard", sCardTag, nPosX, nPosY )

end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
