--------------------------------------------------------------------------------
--  Function......... : getRandomCrypto
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getRandomCrypto ( )
--------------------------------------------------------------------------------
local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
local t=table.newInstance ( )
for j=0,nCount-1
do
    local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
   
    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".Type")
    if(sType=="CRYPTO")
    then
        table.add ( t,  sCryptoID)
    end
end

return table.getAt (t,  math.roundToNearestInteger (  math.random ( 0,table.getSize ( t )-1 )))

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
