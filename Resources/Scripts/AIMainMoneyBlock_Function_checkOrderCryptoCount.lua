--------------------------------------------------------------------------------
--  Function......... : checkOrderCryptoCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.checkOrderCryptoCount ( nPlayerIndex,sCryptoID,nAmount)
--------------------------------------------------------------------------------
	
local nMaxCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MaxOrderCryptoCount")

if(nMaxCount and nMaxCount>0 )
then

    local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
    local nTotal=0
    for i=0,nCount-1
    do
        if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
        then
            local sResourceID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
            if(sResourceID==sCryptoID)
            then
                 nTotal=nTotal+nAmount
            else

                local nA=hashtable.get( this.htCurrentBuyOrders( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                if(nA)
                then
                    nTotal=nTotal+nA
                end
                    nA=hashtable.get( this.htCurrentSellOrders( ),"Player."..nPlayerIndex.."."..sResourceID..".Count" )
                if(nA)
                then
                    nTotal=nTotal+nA
                end
           end
          
        
       end
    end
            
      
            if(nTotal<=nMaxCount)
            then
             
                return true
            else
                return false
            end
else

    return true
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
