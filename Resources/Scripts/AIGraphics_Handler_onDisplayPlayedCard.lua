--------------------------------------------------------------------------------
--  Handler.......... : onDisplayPlayedCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onDisplayPlayedCard ( sTagButton )
--------------------------------------------------------------------------------
	
    
    local sCrypto = ""
    local nCardID = -1
    
    --GET PLAYER ID
	for nIndexCrypto=0 , hashtable.getSize ( this.htPrefixSlotCardHUD ( ) ) -1
    do
    	for nIndexPlayer=0 , table.getSize ( this.tUsers ( ) )-1
        do
        	local sPrefix = hashtable.getAt ( this.htPrefixSlotCardHUD ( ), nIndexCrypto )
            local nPlayerID = table.getAt ( this.tUsers ( ), nIndexPlayer )
            local sTempTag = sPrefix.."."..nPlayerID..".ContainerGlobal"
            
            --FOUND
            if(sTempTag == sTagButton)then
                
                local sValue = hashtable.get ( this.htCardPlayed ( ), nPlayerID )
                sCrypto, nCardID = this.ParsePlayedCard ( sValue )
                
                if(nCardID~=-1)then
                    
                    --DISPLAY CARD
                    hashtable.set ( this.htSlotPreviewCardID ( ), sPrefix, nCardID )
                    this.DisplayPlayedCard ( sPrefix, nCardID)
                   
                end
            end
        end
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
