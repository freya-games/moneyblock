--------------------------------------------------------------------------------
--  Function......... : giveActionCardToPlayer
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.giveActionCardToPlayer (nUserID,nDebugActionCardID )
--------------------------------------------------------------------------------
	
    local nMaxActionCardsCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MaxActionCardsCount")
    local nPlayerIndex=this.getPlayerIndex (nUserID  )
    local nCardCount=hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards.Count")
    if (nCardCount<nMaxActionCardsCount or nDebugActionCardID)
    then
   
        local nActionCardID
        if(nDebugActionCardID)
        then
             nActionCardID =nDebugActionCardID
        else
            nActionCardID =this.PickRandomActionCard ( )
        end
       -- local nActionCardID =10
        if(nActionCardID>-1)
        then
            hashtable.add ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards."..nCardCount,nActionCardID)
            hashtable.set ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards.Count",hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".ActionCards.Count" )+1)
            user.sendEvent ( application.getUser (nUserID),"AIPlayer","onAddActionCard",nActionCardID  )
        else
            log.message ( "Cannot Add Action Card" )
        end
        
    else
    
        log.message ( "Player "..nUserID.." MaxActionCardsCount"  )
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
