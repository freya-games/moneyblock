--------------------------------------------------------------------------------
--  Function......... : getResourcePrice
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getResourcePrice (sResourceID )
--------------------------------------------------------------------------------
	

if(hashtable.contains (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Price" ))
then
    return hashtable.get (  this.htRuntimeData ( ),"Game.Resources."..sResourceID..".Price" )
else
    log.error (  )
    return nil
end
 
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
