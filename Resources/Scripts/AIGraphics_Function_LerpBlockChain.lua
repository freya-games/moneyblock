--------------------------------------------------------------------------------
--  Function......... : LerpBlockChain
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.LerpBlockChain ( sCryptoIDTarget, nOffsetIndexSelection)
--------------------------------------------------------------------------------
	
	if(this.nTimeLerpBlockChain ( )>=0 and sCryptoIDTarget~= "")then
    
        --GET BlockChain
        local hBC = this.hBlockChain ( )
        
        if(hBC)then
        
            --GET FINAL ROT
            local nCrypto = hashtable.getSize ( this.htPrefixCryptoHUD ( ) )
            local nFinalRotY = 0
            
            for nIndexCrypto=0 , nCrypto -1
            do
            	if(hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ),nIndexCrypto ) == sCryptoIDTarget)then
                    if(nOffsetIndexSelection == nil)then nOffsetIndexSelection = 0 end
                    nFinalRotY = 360/nCrypto  * (-nIndexCrypto+nOffsetIndexSelection)
                    break
                end
            end
        
            --GET FACTOR
            local nNewTime = application.getAverageFrameTime ( ) + this.nTimeLerpBlockChain ( )
            local nNewValue = math.min ( 1, nNewTime*this.nSpeedLerpBlockChain ( ))
            if(nNewValue>=1)then this.nTimeLerpBlockChain (-1)end
            
            --APPLY
            local x, y, z = object.getRotation ( hBC, object.kGlobalSpace )
            x, y, z = math.vectorInterpolate ( x,y,z, x,nFinalRotY,z, nNewValue )
            object.setRotation ( hBC, x, y, z, object.kGlobalSpace )
        end
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
