--------------------------------------------------------------------------------
--  Function......... : recurseXMLNode
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.recurseXMLNode (hXMLEntry,xPath,hTable,htTableTemporary )
--------------------------------------------------------------------------------

  
  
    if(xml.getElementAttributeCount ( hXMLEntry )>0)
    then
        for i=0,xml.getElementAttributeCount ( hXMLEntry )-1
        do
            local hAttrib=xml.getElementAttributeAt(hXMLEntry,i)
            
           local nV=string.toNumber ( xml.getAttributeValue(hAttrib) )
            if(nV)
            then
               hashtable.add (hTable, xPath..".Attributes."..xml.getAttributeName(hAttrib),nV )
                hashtable.add (hTable, xPath..".Attributes."..i,nV )
            else
                 hashtable.add (hTable, xPath..".Attributes."..xml.getAttributeName(hAttrib),xml.getAttributeValue(hAttrib) )
                hashtable.add (hTable, xPath..".Attributes."..i,xml.getAttributeValue(hAttrib) )
            end
            
        end
            
          hashtable.add (hTable, xPath..".Attributes.Count",xml.getElementAttributeCount ( hXMLEntry ) )  
    end
    
    
    
    local childCount=xml.getElementChildCount ( hXMLEntry)
    
   
    if(childCount>0)
    then
    
      hashtable.add (hTable, xPath..".ChildCount",childCount)
      
        for i=0,childCount-1
        do
            local hChild=xml.getElementChildAt ( hXMLEntry,i )
          
            local sChildName= xml.getElementName ( hChild )
            
            if(hashtable.contains (htTableTemporary , xPath.."."..sChildName ))
            then
                hashtable.set ( htTableTemporary ,  xPath.."."..sChildName ,true)
           
            else
                hashtable.add (htTableTemporary ,xPath.."."..sChildName,false )
            end
        
         
            
        end
      
        for i=0,childCount-1
        do
            local hChild=xml.getElementChildAt ( hXMLEntry,i )
          
          local newPath
              if(hashtable.get(htTableTemporary ,xPath.."."..xml.getElementName (  hChild)))
              then
                    newPath=xPath.."."..xml.getElementName (  hChild).."."..i
              else
                      newPath=xPath.."."..xml.getElementName (  hChild)
              end
            this.recurseXMLNode ( hChild,newPath,hTable,htTableTemporary  )
        end
    else
    
        local nV=string.toNumber ( xml.getElementValue(hXMLEntry) )
        if(nV)
        then
            hashtable.add (hTable, xPath,nV )
        else
             hashtable.add (hTable, xPath,xml.getElementValue(hXMLEntry) )
        end
       

    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
