--------------------------------------------------------------------------------
--  Handler.......... : onCommitSellOrder
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onCommitSellOrder ( sResID,nAmount,nTotalCount,bCancel )
--------------------------------------------------------------------------------
	
	
     
        local sMessage
        if(bCancel)
        then
            sMessage="Sell order Canceled "..nAmount.." "..sResID
            
            if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Sell" ))
            then
                hashtable.set ( this.htPendingOrders ( ),sResID..".Sell" ,0)
            else
                hashtable.add ( this.htPendingOrders ( ),sResID..".Sell" ,0)
            end
            
        else
            sMessage="Sell order Commited "..nAmount.." "..sResID
            if(hashtable.contains ( this.htResourceCount ( ),sResID ))
            then
                hashtable.set ( this.htResourceCount ( ),sResID, nTotalCount)
            else
                hashtable.add ( this.htResourceCount ( ),sResID,nTotalCount )
            end
        
            
   
        end
        
        user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onCommitSellOrder", sResID,nAmount,nTotalCount,bCancel )
        this.openDialogBox (sMessage )
 
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
