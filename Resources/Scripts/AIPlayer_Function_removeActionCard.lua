--------------------------------------------------------------------------------
--  Function......... : removeActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.removeActionCard (nCardID)
--------------------------------------------------------------------------------
	
for i=0, table.getSize ( this.tActionCards ( ) )-1
do
    if(table.getAt ( this.tActionCards ( ),i )==nCardID)
    then
        table.removeAt ( this.tActionCards ( ),i )
        break
    end
end
	
--user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onRemoveActionCard", nCardID )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
