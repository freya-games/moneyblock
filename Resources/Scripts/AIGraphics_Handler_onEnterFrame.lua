--------------------------------------------------------------------------------
--  Handler.......... : onEnterFrame
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onEnterFrame (  )
--------------------------------------------------------------------------------

--     if(this.nPhase ( ) == 3)then
--         this.LerpMiningScalableMeshes()
--     end

    --GO TO SELECTED CRYPTO
    if(this.nPhase ( ) == 2)then
        this.LerpBlockChain ( this.sCurTradeCryptoID ( ), 2 )
    end

    --RESET POS
     if(this.nPhase ( ) == 4 or this.nPhase ( ) == 5  )then
        this.LerpBlockChain ( hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ),0 ) )
    end

    if(this.nPhase ( ) == 1 or this.nPhase ( ) == 2 or this.nPhase ( ) == 4 or this.nPhase ( ) == 5  )then
        this.LerpTokens ( )
    end

    if(this.nTimeLerpEndTurnValues ( )>0)then

        this.LerpEndTurnValues ( )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
