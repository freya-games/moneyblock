--------------------------------------------------------------------------------
--  Handler.......... : onReceiveAllCardsPlayed
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onReceiveAllCardsPlayed ( sAllInfos, sDelimiterPlayer, sDelimiterInfos  )
--------------------------------------------------------------------------------
	
    hashtable.empty ( this.htCardPlayed ( ) )
    hashtable.empty ( this.htSlotPreviewCardID ( ) )
    
    --PARSE PLAYERS
	local tPlayedCard = table.newInstance ( )
    string.explode ( sAllInfos, tPlayedCard, sDelimiterPlayer )
    
    --PARSE CARD INFOS
    for i=0 , table.getSize ( tPlayedCard )-1 
    do
    	local tCardInfos = table.newInstance ( )
        string.explode ( table.getAt ( tPlayedCard, i ), tCardInfos, sDelimiterInfos )
        
        local nPlayerIndex, sCrypto, nCardID = table.getRangeAt ( tCardInfos, 0, 3  )
        local nPlayerID = table.getAt ( this.tUsers ( ), string.toNumber ( nPlayerIndex ) )
        
        --STORE
        hashtable.add ( this.htCardPlayed ( ), nPlayerID, sCrypto.."."..nCardID )
        
        
        --DISPLAY FIRST CARD OR CUR USER CARD
        local sPrefix = hashtable.get(this.htPrefixSlotCardHUD ( ), sCrypto )
        
        if(not hashtable.contains ( this.htSlotPreviewCardID ( ), sPrefix ))then
            hashtable.add ( this.htSlotPreviewCardID ( ), sPrefix, nCardID )
        elseif(nPlayerID == user.getID ( this.getUser ( ) ))then
            hashtable.set ( this.htSlotPreviewCardID ( ), sPrefix, nCardID )
        end
        
    end


-- 
--     if( not(this.nPhase ( ) == 4 and (not this.bPhaseValided ( ))))then
--         this.EnablePlayerCardPreview ( true )
--     end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
