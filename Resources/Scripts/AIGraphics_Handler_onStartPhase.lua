--------------------------------------------------------------------------------
--  Handler.......... : onStartPhase
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onStartPhase ( _nPhase, _nTurn )
--------------------------------------------------------------------------------

if(_nPhase<=0)then return  end
this.nPhase ( _nPhase )
this.bPhaseValided ( false )

--RESET ON END TURN
if(this.nPhase ( )==1)then
    this.CleanBuffers ( )
    this.UpdateHUD ( )
    this.UpdateMarketHUD ( )
end


------------------------------------
if(not this.bUseDebugHUD ( ))
then
    --ENABLE BUTTON
    this.postEvent ( 0.44, "onStartEndTurnButtonIdle" )

    --RESET READY HUD
    hashtable.empty ( this.htPlayerReady ( ) )
    for i=0 , table.getSize ( this.tUsers ( ) ) -1
    do this.SetPlayerHUDReady ( i, false ) end

    -- PHASE TEXT
    local nIndex=this.nPhase()-1
    local sPhaseName=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.Phase."..nIndex..".Name")
    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelPhase" )
    hud.setLabelText ( hComponent, sPhaseName )

    --SET TIMER
    this.nCurPhaseTimer (0)
    local hC = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerTimeInfos" )
    local nCurrentPhaseTimeLim=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.Phase."..nIndex..".TimeLimit")
    if(nCurrentPhaseTimeLim and nCurrentPhaseTimeLim>0 )
    then
        this.nCurPhaseTimeLim (nCurrentPhaseTimeLim)
        this.CheckTimer ( )
        hud.setComponentVisible ( hC, true )
    else
        this.nCurPhaseTimeLim (nil)
        this.Idle ( )
        hud.setComponentVisible ( hC, false )
    end

    --SET CAM
    this.SetCameraState ( _nPhase )

    if(this.nPhase ( ) == 1)then

        this.EnableCryptoButton ( false )
        this.EnablePlayerCardPreview ( false )
        this.EnableMiningHUD (false)
        this.EnableActionCards ( false )
        this.EnableMarketCardHUD( true )
        this.UpdatePendingHUD (  )

        if(_nTurn>1)then
            this.UpdateEndTurnHUD ( )
            this.EnableEndTurnHUD ( true )
        end

    elseif(this.nPhase ( ) == 2)then

        -- RESET AND OPEN TRADE HUD
        for i = 0, hashtable.getSize ( this.htPrefixCryptoHUD ( ) )-1
        do hud.callAction (this.getUser ( ), "Crypto_"..i..".Reset"  )end

        this.EnableCryptoButton ( true )
        this.EnableMarketCardHUD( false )
        this.EnableTradeHUD ( true, this.sCurTradeCryptoID ( ) )

    elseif(this.nPhase ( ) == 3)then

        this.EnableTradeHUD ( false, this.sCurTradeCryptoID ( ) )
        this.sCurMiningCryptoID ( "" )
        this.EnableMiningHUD (true)
        this.UpdateMiningModelPosition ( )
        this.UpdateOrderBooksHUD ( )
        this.UpdateMiningHUD (  )
        this.EnableBlockChainModel(false)
        this.UpdateCashHUD ( )

    elseif(this.nPhase ( ) == 4)then

        this.EnableCryptoButton ( false )
        this.EnableMiningHUD (false)
        this.EnableBlockChainModel(true)
        this.EnableActionCards ( true )
        this.UpdateSlotInfos()

        this.OpenContainerCardsHUD (  )


    elseif(this.nPhase ( ) == 5)then

        --reset cur hover card
        if(this.sTagDraggedCard ( )~="")then
            this.sendEvent ( "onDropedActionCard", this.sTagDraggedCard ( ), -1, -1 )
        end

        this.EnablePlayerCardPreview ( true )
        this.ResetPendingHUD ( )

        this.CloseContainerCardsHUD (  )
    end

  ----------------DEBUG HUD---------------------
else

    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.Crypto" )
    if(this.nPhase ( )==2)
    then
        hud.enableListSelection ( hComponent,true )
    else
         hud.enableListSelection ( hComponent,false )
    end

    hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningCrypto" )
    if(this.nPhase ( )==3)
    then
        local hComponentCryptoAll = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningCryptoAll" )
        hud.removeListAllItems ( hComponentCryptoAll )
        hud.enableListSelection ( hComponent,true )
    else
         hud.enableListSelection ( hComponent,false )
    end


   local hComponentButton = hud.getComponent ( this.getUser ( ), "PlayerHUD.ActionPlayCard" )
    if(this.nPhase ( )==4)
    then
        hud.setComponentVisible ( hComponentButton,true )
    else
         hud.setComponentVisible ( hComponentButton,false )
    end


    if(this.nPhase ( )~=3)
    then
        local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.Mining" )
        hud.setComponentVisible ( hComponent,false )
    end


        hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.PhaseContainer" )
        hud.setComponentVisible ( hComponent,true )

        local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.ValidPhase" )
        hud.setComponentVisible ( hComponent,true )

         local nIndex=this.nPhase()-1
        local sPhaseName=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.Phase."..nIndex..".Name")
        local sPhaseDesc=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.Phases.Phase."..nIndex..".Description")
        if(sPhaseName)
        then
            hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.PhaseLabel" )
            hud.setLabelText ( hComponent,sPhaseName )
        end
        if(sPhaseDesc)
        then
             hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.PhaseDescription" )
             hud.setLabelText ( hComponent,sPhaseDesc )
        end

            local sTurn="Turn ".._nTurn

            hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.Turn" )
            hud.setLabelText ( hComponent,sTurn )


        if(this.nPhase ( )==1)
        then
            --PICK CARD
            local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MarketCardContainer" )
            hud.setComponentVisible ( hComponent,true )

            hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.ContainerAction" )
            hud.setComponentVisible ( hComponent,true )

        elseif(this.nPhase ( )==2)
        then
            --TRADING
            local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.ContainerResources" )
            hud.setComponentVisible ( hComponent,true )

        elseif(this.nPhase ( )==3)
        then
            --MINING
            local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.Mining" )
            hud.setComponentVisible ( hComponent,true )

        elseif(this.nPhase ( )==5)
        then


        end


end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
