--------------------------------------------------------------------------------
--  Function......... : ActionMinerCount
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionMinerCount (nCardIndex,sCryptoID,nPlayerIndex )
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end



    local nMin =hashtable.get (this.htActionCards ( ),sTag..".Effect.Min")
    if(nMin)
    then

        local nMax =hashtable.get (this.htActionCards ( ),sTag..".Effect.Max")

        local nAmount=nMin
        if(nMin~=nMax)
        then
            nAmount=math.roundToNearestInteger (  math.random ( nMin,nMax ))
        end

          local sResID=hashtable.get (this.htActionCards ( ),sTag..".Effect.ID")
        if(sResID)
        then

--         for i =0, this.nPlayerCount ( )-1
--         do
--
--
--
--                 this.changePlayerMiningResource (  nPlayerIndex ,sCryptoID,sResID,nAmount )
--
--         end
--
    this.changeMiningResource ( sCryptoID,sResID,nAmount )
        else
            log.error ( "Error ActionMinerCount" )
        end
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
