--------------------------------------------------------------------------------
--  Handler.......... : onDragActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onDragActionCard ( sTag )
--------------------------------------------------------------------------------
 
    --IS MARKET CARD
    local bMarketCard = false
    if(string.contains ( sTag, "MarketCard" ))
    then 
        bMarketCard = true 
    else   
        local hUser=this.getUser ( )
        hud.stopSound ( hUser,3)
        hud.playSound ( hUser,3,127,false )
    end
    
    if(this.nPhase ( ) ~= 4 or bMarketCard or this.bPhaseValided ())then
    
        local nCardID = this.nMarketCardID ( )
        if(bMarketCard == false)then
            local nCardIndex = -1
            nCardIndex, nCardID = this.GetCardIndex ( sTag )
        end
        
        this.EnableCardDetailHUD ( nCardID, bMarketCard )
    
    else
    
        local hUser = this.getUser ( )
        this.sTagDraggedCard ( sTag )
        
        local hCard = hud.getComponent ( hUser, sTag )
        if(hCard)then 
            hud.setComponentContainer ( hCard, nil )
            local nSizeY = 40
            hud.setComponentSize ( hCard, nSizeY * this.nRatioCardHUD ( ), nSizeY  )
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
