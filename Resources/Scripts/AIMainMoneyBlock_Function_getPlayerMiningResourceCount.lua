--------------------------------------------------------------------------------
--  Function......... : getPlayerMiningResourceCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getPlayerMiningResourceCount (nUserID,nResID)
--------------------------------------------------------------------------------
	
   for i =0, table.getSize ( this.tUsers ( ) )-1
    do
        if(hashtable.get ( this.htRuntimeData ( ),"Player."..i..".UserID" )==nUserID)
        then
            return hashtable.get( this.htRuntimeData ( ),"Player."..i.."."..nResID..".Count" )
        end
    end
	return nil
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
