--------------------------------------------------------------------------------
--  Function......... : UpdateMiningModelPosition
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningModelPosition ( )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
    local hCam = user.getActiveCamera ( hUser )
    local nPx, nPy, nPz = object.getTranslation ( hCam, object.kGlobalSpace )
    
    --DEFAULT POSITION
    local nStartX = 0
    local nStartY = 0
    local nStartZ = 0
    local nEndX = 0
    local nEndY = 0
    local nEndZ = 0
    
    --GET ANCHOR POSITION
    nStartX, nStartY, nStartZ, nEndX, nEndY, nEndZ = this.Set3DAnchorPosition ( -0.47, -0.1, 0.67, -0.1 )
         
   
    --SET MINING BOX POSITION
    for i=0 , hashtable.getSize ( this.htMiningModel ( ) ) -1
    do
        local x, y, z = math.vectorInterpolate ( nStartX, nStartY, nStartZ, nEndX, nEndY, nEndZ, i/(hashtable.getSize (this.htMiningModel ()) -1) )

        local hObj = hashtable.getAt ( this.htMiningModel ( ), i )
        if(hObj)then
           -- object.setTranslation ( hObj, x, y, z, object.kGlobalSpace )
            --object.lookAt ( hObj, x, 0, -nPz, object.kGlobalSpace, 1  )
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
