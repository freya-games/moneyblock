--------------------------------------------------------------------------------
--  Function......... : SendAllTransaction
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.SendAllTransaction ( )
--------------------------------------------------------------------------------

for i=0, hashtable.getSize ( this.htHUDTradeValue( ) )-1
do

    local vValue = hashtable.getAt (  this.htHUDTradeValue ( ), i )
    local sCryptoID = hashtable.getKeyAt (  this.htHUDTradeValue ( ), i )
    
    if(vValue>0)then
    
       user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onBuy", sCryptoID, vValue )
    
    elseif(vValue<0)then
    
       user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onSell", sCryptoID, -vValue )
    end

end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
