--------------------------------------------------------------------------------
--  Handler.......... : onEnableChat
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Chat.onEnableChat ( bEnable )
--------------------------------------------------------------------------------
	
	hud.destroyTemplateInstance ( this.getUser ( ), "Network_Chat" )
    if ( bEnable )
    then
        hud.newTemplateInstance     ( this.getUser ( ), "Network_Chat", "Network_Chat" )
        local hChatHistory = hud.getComponent ( this.getUser ( ), "Network_Chat.History" )
        if ( hChatHistory )
        then
            hud.setListVerticalScrollBarWidth ( hChatHistory, 0 )
            hud.setListColumnTextAlignmentAt  ( hChatHistory, 0, hud.kAlignLeft, hud.kAlignCenter )            
        end
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
