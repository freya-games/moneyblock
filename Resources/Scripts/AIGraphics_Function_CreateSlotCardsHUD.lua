--------------------------------------------------------------------------------
--  Function......... : CreateSlotCardsHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateSlotCardsHUD ( )
--------------------------------------------------------------------------------

    local hUser = this.getUser ( )
    local nSlot = hashtable.getSize( this.htPrefixSlotCardHUD ( ))
    local hComponent = hud.getComponent ( hUser, "PlayerMainHUD.ContainerSlotCards" )

    local nSpaceY = 2
    local nSizeY = 100/ math.ceil ( nSlot/2 ) - nSpaceY
    local nSizeX = nSizeY * 1.2

    for i = 0, nSlot - 1
    do
        local nSign = 1
        local sPrefix = "SlotCard_"..i
        local sCryptoID = hashtable.getKeyAt ( this.htPrefixSlotCardHUD ( ), i )
        hashtable.set( this.htPrefixSlotCardHUD ( ), sCryptoID , sPrefix )

        if(hud.newTemplateInstance ( hUser, "SlotCard", sPrefix  ))then
            local hChild = hud.getComponent ( this.getUser ( ), sPrefix..".ContainerGlobal" )

            if(hComponent and hChild)then

                local nPosX = 0
                local nPosY = (math.mod ( i, math.ceil ( nSlot/2  )) + 0.5 ) * 100 /math.ceil ( nSlot/2  )
                nPosY = 100 - nPosY

                --OPPOSITE SIDE
                if(i>=math.ceil ( nSlot/2  ))then
                    nSign = -1
                    nPosX = 100
                    nPosY = 100- nPosY
                    local hCInfos = hud.getComponent ( this.getUser ( ), sPrefix..".ContainerInfos" )
                    if(hCInfos)then
                        hud.setComponentOrigin ( hCInfos, hud.kOriginRight )
                        hud.setComponentPosition ( hCInfos, 100-12.5, 50 )
                    end
                    local hCList = hud.getComponent ( this.getUser ( ), sPrefix..".ContainerPlayer" )
                    if(hCInfos)then
                        local x, y = hud.getComponentPosition( hCList)
                        hud.setComponentPosition ( hCList, 100-x, y )
                    end

                    local hBG = hud.getComponent ( this.getUser ( ), sPrefix..".BackGroundSlot" )
                    if(hBG)then
                        hud.setComponentBackgroundImageUVOffset ( hBG, 1, 0 )
                        hud.setComponentBackgroundImageUVScale ( hBG, -1, 1 )
                    end
                    local hSprite = hud.getComponent ( this.getUser ( ), sPrefix..".SpriteCrypto" )
                    if(hSprite)then
                        local x, y = hud.getComponentPosition( hSprite)
                        hud.setComponentPosition ( hSprite, 100-x, y )
                    end
                end

                local nOffsetX = 20 * nSign
                nPosX = nPosX + nOffsetX

                hud.setComponentContainer ( hChild, hComponent )
                hud.setComponentPosition ( hChild, nPosX, nPosY )
                hud.setComponentSize ( hChild, nSizeX, nSizeY )

            end

            --SET COLOR
            --local r, g, b = this.GetResourceColor ( sCryptoID )
            --hud.callAction ( hUser, sPrefix..".SetSlotColor", r, g, b )
            local sLogo = this.GetResourceLogo ( sCryptoID )
            hud.callAction ( hUser, sPrefix..".SetSprite", sLogo )


            --ADD ORDER BOOK
            local sPrefixOrderBook = "CardOrderBook_"..i
            hashtable.add ( this.htPrefixSlotOrderBookHUD ( ), sCryptoID,  sPrefixOrderBook )
            if(hud.newTemplateInstance ( hUser, "OrderBook", sPrefixOrderBook  ))then

                local hChild = hud.getComponent ( hUser, sPrefixOrderBook..".ContainerGlobal" )
                local hC = hud.getComponent ( hUser, sPrefix..".ContainerOrderBook")
                hud.setComponentContainer ( hChild, hC )

            end

            --ADD MINING RESOURCE
            for j=0 , hashtable.getSize ( this.htPrefixMiningResourceHUD ( ) ) -1
            do
                local sResID = hashtable.getKeyAt ( this.htPrefixMiningResourceHUD ( ), j )
                --local r, g, b = this.GetResourceColor ( sResID )
                local sLogo = this.GetResourceLogo ( sResID )
                local hList = hud.getComponent ( hUser, sPrefix..".ListMiningResource")

                 local sPrefixCardResource = "Card_"..i.."_MiningResource_"..j
                 hashtable.add ( this.htPrefixSlotMiningResourceHUD ( ), sCryptoID.."."..sResID,  sPrefixCardResource )
                 if(hud.newTemplateInstance ( hUser, "CardMiningResource", sPrefixCardResource  ))then

                    local hChild = hud.getComponent ( hUser, sPrefixCardResource..".ContainerGlobal" )

                    local nIndex = hud.addListItem ( hList, "" )
                    hud.setListItemChildAt ( hList, nIndex, 0, hChild )

                   local hSprite = hud.getComponent ( hUser, sPrefixCardResource..".SpriteResource" )
                   --if(hSprite)then hud.setComponentBackgroundColor ( hSprite, r, g, b, 255 ) end
                   if(hSprite)then hud.setComponentBackgroundImage ( hSprite, sLogo ) end
                end

            end

            --ADD Player BUTTON
            local hCPlayer = hud.getComponent ( hUser, sPrefix..".ContainerPlayer")

            for j=0 , table.getSize ( this.tUsers() )-1
            do
                local nPlayerID = table.getAt ( this.tUsers(), j )
                if(hud.newTemplateInstance ( hUser, "PlayerButton" , sPrefix.."."..nPlayerID ))then


                    local hC = hud.getComponent ( hUser, sPrefix.."."..nPlayerID..".ContainerGlobal" )

                    if(hC and hCPlayer)then
                        hud.setComponentContainer ( hC, hCPlayer )
                        hud.setComponentSize ( hC, 100, 100/ this.GetMaxPlayerCount ( ) )
                        hud.setComponentPosition ( hC, 50, (1- j/this.GetMaxPlayerCount ( ))*100 )
                    end

                    local hC = hud.getComponent ( hUser, sPrefix.."."..nPlayerID..".SpritePlayer" )
                    if(hC)then
                        if(nSign>0)then
                            hud.setComponentOrigin ( hC, hud.kOriginRight )
                            hud.setComponentPosition ( hC,100, 50 )
                        end
                    end
                end
            end
        end
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
