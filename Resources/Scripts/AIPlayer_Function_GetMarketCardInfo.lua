--------------------------------------------------------------------------------
--  Function......... : GetMarketCardInfo
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.GetMarketCardInfo (_nMarketCardID,sName )
--------------------------------------------------------------------------------
	
local nTotalMarketCard =  hashtable.get ( this.htMarketCards ( ),"MarketCards.ChildCount")

    local sTag
    if(nTotalMarketCard>1)
    then
        sTag="MarketCards.MarketCard.".._nMarketCardID 
    else
      
        sTag="MarketCards.MarketCard"
    end	
    
    
    local sName=hashtable.get ( this.htMarketCards ( ),sTag.."."..sName)
    return sName
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
