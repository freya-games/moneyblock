--------------------------------------------------------------------------------
--  Function......... : Init
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.Init ( )
--------------------------------------------------------------------------------
user.setScene (this.getUser ( ),"MoneyBlock"  )
--music.setVolume ( this.getUser ( ),0.2,0  )

--music.play ( this.getUser ( ) , 0,10)


    this.InitTranslation ( )


    this.ParseMarketCards ( )
    this.ParseActionCards ( )



    this.InitGame ( )
    this.InitPlayers ( )


    this.nTurn ( this.nTurn ( )+1)


    local nTimeLimit=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.EndGame.TimeLimit")
    if(nTimeLimit and nTimeLimit>0)
    then
        this.nTimeGameLimit ( nTimeLimit)
        this.bGlobalTimerLimit ( true)
    end

    local _nBlockChainValidationCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.EndGame.BlockChainValidationCount")
    if(_nBlockChainValidationCount and _nBlockChainValidationCount>0)
    then
        this.nBlockChainValidationCount ( _nBlockChainValidationCount)
    end
    this.nPlayerCheckCount ( 0)
    this.startGlobalTimer ( )

    this.sendInitToAllPlayers ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
