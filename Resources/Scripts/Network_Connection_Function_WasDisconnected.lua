--------------------------------------------------------------------------------
--  Function......... : WasDisconnected
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.WasDisconnected ( )
--------------------------------------------------------------------------------

    -- Go back to the main menu
    --

    --Inform the main AI we have to leave the scene
    user.sendEvent ( this.getUser ( ), "Network_Main", "onChangeScene", "")

    --return to the main menu
    hud.callAction ( this.getUser ( ), "Network_Connection.ShowConnection" )
    this.Idle ( )


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
