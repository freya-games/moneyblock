--------------------------------------------------------------------------------
--  State............ : LerpTarget
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Camera.LerpTarget_onLoop ( )
--------------------------------------------------------------------------------
	
	
    
    --ADD TIME
    this.nLerpCurTime ( this.nLerpCurTime ( ) + application.getLastFrameTime ( ) )
    
    --GET PROGRESS
    local nProgress = 1
    if(this.nLerpTotalTime ( )>0)then
        nProgress = math.min ( 1, this.nLerpCurTime ( )/this.nLerpTotalTime ( ))
    end
    
    --GET TRANSFORM
    local sx, sy, sz, srx, sry, srz = table.getRangeAt ( this.tLerpStartTransform ( ),0, 6 )
    local ex, ey, ez, erx, ery, erz = table.getRangeAt ( this.tLerpEndTransform ( ),0, 6 )
    
    
    local x, y, z = math.vectorInterpolate ( sx, sy, sz, ex, ey, ez, nProgress )
    local rx, ry, rz = math.vectorInterpolate ( srx, sry, srz, erx, ery, erz, nProgress )
    object.setTranslation ( this.getObject ( ), x, y, z, object.kGlobalSpace )
    object.setRotation ( this.getObject ( ), rx, ry, rz, object.kGlobalSpace )
    
    --END
	if(nProgress >=1)then 
        this.Idle ( )
        return
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
