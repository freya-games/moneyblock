--------------------------------------------------------------------------------
--  Function......... : sendPlayerDetailsToAllPlayers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.sendPlayerDetailsToAllPlayers ( )
--------------------------------------------------------------------------------

local sAllNames = ""
local sAllPictures = ""
local sDelimiter = "_"

for nPlayerIndex = 0 , this.nPlayerCount ( )-1 
do
    sAllNames = sAllNames..hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".Name")..sDelimiter
    sAllPictures = sAllPictures..hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".Picture")..sDelimiter
end


for nPlayerIndex = 0 , this.nPlayerCount ( )-1 
do
    user.sendEvent ( application.getUser ( table.getAt ( this.tUsers ( ),nPlayerIndex) ),"AIPlayer","onReceiveAllPlayerDetails", sAllNames, sAllPictures, sDelimiter)
end

    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
