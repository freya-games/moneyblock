--------------------------------------------------------------------------------
--  Function......... : StartEndTurnButtonIdle
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.StartEndTurnButtonIdle ( )
--------------------------------------------------------------------------------
	
    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.SpriteEndTurn" )
    if(hComponent)then hud.setComponentOpacity (  hComponent, 255 )end
    
    local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ContainerEndTurn" )
    if(hComponent)then hud.setComponentVisible ( hComponent,true )end
    
     local hComponent = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.ButtonEndTurn" )
    if(hComponent)then hud.setComponentActive ( hComponent,true )end
    
    
    if(hud.isActionRunning ( this.getUser ( ), "PlayerMainHUD.EndTurnIdle" ))then 
            --hud.stopAction ( this.getUser ( ), "PlayerMainHUD.EndTurnIdle" ) 
        return
    end
    
     if(hud.isActionRunning ( this.getUser ( ), "PlayerMainHUD.EndTurnIdleB" ))then 
        --hud.stopAction ( this.getUser ( ), "PlayerMainHUD.EndTurnIdleB" ) 
        return
    end
    
    local sAction = "EndTurnIdle"
    hud.callAction ( this.getUser ( ), "PlayerMainHUD."..sAction, this.nDurationEndTurnIdle ( ) )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
