--------------------------------------------------------------------------------
--  Function......... : checkTransationCount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.checkTransationCount ( nPlayerIndex)
--------------------------------------------------------------------------------
	
local nMaxTransactionCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Players.MaxTransactionCount")

if(nMaxTransactionCount and nMaxTransactionCount>0)
then

    local nTransactionCount=hashtable.get ( this.htRuntimeData ( ),"Player."..nPlayerIndex..".TransactionCount")
    if(nTransactionCount<nMaxTransactionCount)
    then
        
        return true
    else
        return false
    end
else

    return true
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
