--------------------------------------------------------------------------------
--  Function......... : CreatePlayerDetailsHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreatePlayerDetailsHUD ( )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    local nMaxPlayerCount = this.GetMaxPlayerCount ( )
    local nSpacePlayerScore = 2
    local hContainerPlayerScore = hud.getComponent ( hUser, "PlayerMainHUD.ContainerPlayerScore" )
    
	 --ADD ALL PLAYER SCORE HUD
    if(hContainerPlayerScore)then
        for i =0 , table.getSize ( this.tPlayersPictureID ( ) )-1
        do
        
            local nPicture = table.getAt ( this.tPlayersPictureID ( ), i )
               
            --CREATE HUD 
            local sPrefixPlayerScore = "PlayerScore_"..i
            table.add ( this.tPrefixPlayerScoreHUD ( ), sPrefixPlayerScore )
            hud.newTemplateInstance ( hUser, "PlayerScore", sPrefixPlayerScore)
            local hChild = hud.getComponent ( hUser, sPrefixPlayerScore..".ContainerGlobal" )
            
            --SET HUD TRANS
            hud.setComponentContainer ( hChild, hContainerPlayerScore)
            hud.setComponentPosition ( hChild, (i+0.5)*100/(nMaxPlayerCount), 50 )
            hud.setComponentSize ( hChild, 100/nMaxPlayerCount -  nSpacePlayerScore, 100 )
            
            --SET GRAPH
            hud.callAction ( hUser, sPrefixPlayerScore..".SetSpritePlayer", "Picture"..nPicture  )
            
        end
    end
	
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
