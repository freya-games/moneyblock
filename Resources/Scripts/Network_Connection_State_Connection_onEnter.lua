--------------------------------------------------------------------------------
--  State............ : Connection
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.Connection_onEnter ( )
--------------------------------------------------------------------------------

    --connect to the selected server.
    if ( string.isEmpty ( this.sServerIP ( ) ) )
    then
        log.error ( "You must set a Initial value to the variable sServerIP.\nEdit the AIModel named 'Network_Connection'." )
        this.WasDisconnected ( )
    else
        network.setCurrentServer ( this.sServerIP ( ) )
    end



--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
