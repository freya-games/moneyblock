--------------------------------------------------------------------------------
--  Handler.......... : onReceiveAllPlayerDetails
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onReceiveAllPlayerDetails ( sAllNames, sAllPictures, sDelimiter)
--------------------------------------------------------------------------------
	
	string.explode ( sAllNames, this.tPlayersNames ( ), sDelimiter )
	string.explode ( sAllPictures, this.tPlayersPictureID ( ), sDelimiter )
    
    user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onReceiveAllPlayerDetails", sAllNames, sAllPictures, sDelimiter )
    
    
    --LOG
--     for i=0 , table.getSize ( this.tPlayersNames ( )) -1
--     do
--     	log.message ( " AI Player : Details Player "..i.." received "..table.getAt ( this.tPlayersNames ( ) ,i).." and "..table.getAt ( this.tPlayersPictureID ( ) ,i) )
--     end
    
    
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
