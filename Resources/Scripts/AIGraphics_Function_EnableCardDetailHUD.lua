--------------------------------------------------------------------------------
--  Function......... : EnableCardDetailHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableCardDetailHUD ( CardID, bMarketCard )
--------------------------------------------------------------------------------
	
    if(CardID)then
    
    local sName, sText, sImg
    if(bMarketCard)then
        sName= this.GetMarketCardInfo (CardID,"Name")
        sText= this.GetMarketCardInfo (CardID,"Description" )
        sImg= this.GetMarketCardInfo (CardID,"Img" )
        hud.callAction ( this.getUser ( ), this.sPrefixCardDetailHUD ( )..".SetMarketCard" )
    else
        sName= this.GetActionCardInfos (CardID, "Name")
        sText= this.GetActionCardInfos (CardID, "Description")
        sImg= this.GetActionCardInfos (CardID, "Img")
        hud.callAction ( this.getUser ( ), this.sPrefixCardDetailHUD ( )..".SetActionCard" )
    end

    hud.callAction ( this.getUser ( ), this.sPrefixCardDetailHUD ( )..".Show" )
    hud.callAction ( this.getUser ( ), this.sPrefixCardDetailHUD ( )..".SetInfos", sName, sText, sImg  )
    
    end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
