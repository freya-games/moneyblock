--------------------------------------------------------------------------------
--  Function......... : MarketLessActiveCryptoPrice
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.MarketLessActiveCryptoPrice ( nCardIndex)
--------------------------------------------------------------------------------
 local nCardCount = hashtable.get (this.htMarketCards ( ), "MarketCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="MarketCards.MarketCard."..nCardIndex
    else

        sTag="MarketCards.MarketCard"
    end

     local nRatio =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Ratio")
    if(nRatio)
    then
        local sMode =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Mode")
        local sCryptoID=this.getLessActiveCrypto ( sMode )


        if(sCryptoID)
        then
            log.message ( "MarketLessActiveCryptoPrice : "..sCryptoID )
            this.changeGameResourcePriceRatio ( sCryptoID,nRatio )

            this.sendMessageToAllUsers ( "[MARKET CARD] Loi de Gresham Effect Applied on "..sCryptoID.." with Ratio "..nRatio)


        else

            log.error ( " MarketLessActiveCryptoPrice Error ")
        end

    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
