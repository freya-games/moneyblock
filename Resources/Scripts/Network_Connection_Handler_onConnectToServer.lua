--------------------------------------------------------------------------------
--  Handler.......... : onConnectToServer
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onConnectToServer (  )
--------------------------------------------------------------------------------
--Get player name
    --
    this.sPlayerName ( hud.getEditText ( hud.getComponent ( this.getUser ( ), "Network_Connection.Connection_Edit_Name" ) ) )

    --build one if none
    if ( string.isEmpty ( this.sPlayerName ( ) ) or this.sPlayerName ( )== " " )
    then
        this.sPlayerName ( "Player "..user.getID ( this.getUser ( ) ) )
    end

    this.sServerIP ( hud.getEditText ( hud.getComponent ( this.getUser ( ), "Network_Connection.ServerIP" ) ) )

    --change state
    this.Connection ( )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
