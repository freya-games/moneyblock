--------------------------------------------------------------------------------
--  Function......... : UpdateMiningResourceAmountList
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMiningResourceAmountList ( )
--------------------------------------------------------------------------------
	
	local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResourceValue" )
hud.removeListAllItems ( hComponent )

    local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD.MiningResource" )

    local nItem=hud.getListSelectedItemAt ( hC,0 )
  
    local sID=table.getAt ( this.tMiningResourceHUD ( ),nItem )
 local nCount=hashtable.get ( this.htResourceCount ( ),sID )


local nTotalMining=0
for i=0,hashtable.getSize ( this.htMining ( ) )-1
do

    local sS=hashtable.getKeyAt ( this.htMining ( ),i )
    local t=table.newInstance ( )
    string.explode ( sS,t,"." )
    
    local sMiningCryptoID=table.getAt ( t,0 )
    local sMiningResourceID=table.getAt ( t,1 )
    local nMiningResourceCount= hashtable.getAt ( this.htMining ( ),i )
    if(sMiningResourceID==sID)
    then
        nTotalMining=nTotalMining+nMiningResourceCount
    end
    
end

for i=0,nCount-nTotalMining
do
  hud.addListItem ( hComponent,""..i )

end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
