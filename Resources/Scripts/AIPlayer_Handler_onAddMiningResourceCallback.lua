--------------------------------------------------------------------------------
--  Handler.......... : onAddMiningResourceCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onAddMiningResourceCallback ( sMiningCryptoID,sMiningResourceID,nMiningResourceCount,bCanAdd )
--------------------------------------------------------------------------------
	
    if(bCanAdd)
    then
     
      this.openDialogBox ( "[Server]  Added resources "..sMiningCryptoID.." "..sMiningResourceID.." "..nMiningResourceCount  )
    
    else
    
        this.openDialogBox ( "[Server] Cannot Add resources "..sMiningCryptoID.." "..sMiningResourceID.." "..nMiningResourceCount  )
    end

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
