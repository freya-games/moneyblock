--------------------------------------------------------------------------------
--  Function......... : CreatePlayerViewports
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.CreatePlayerViewports (nPlayerCountParam )
--------------------------------------------------------------------------------
	local nScreenLeft, nScreenBottom, nScreenWidth, nScreenHeight = user.getViewport ( this.getUser ( ) ) --user 0 has the full viewport     







if(nPlayerCountParam==1)
then
    local hPlayer = application.createUser ( )   
    if ( hPlayer )    
    then                

        user.addAIModel  ( hPlayer, "AIPlayer" )   
        user.setViewport ( hPlayer, nScreenLeft, nScreenBottom , nScreenWidth , nScreenHeight )
        
           hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".L",nScreenLeft)  
        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".B",nScreenBottom )  
        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".W",nScreenWidth)  
        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".H",nScreenHeight  )  
        
        table.add ( this.tUsers ( ), user.getID ( hPlayer ))
        hashtable.add ( this.htValidPhase ( ),""..user.getID (  hPlayer ),true)
       
    end 

else

  local nR=1
    if(nPlayerCountParam>1)
    then
        nR=2
    end
    local nC=nPlayerCountParam
    if(math.mod(nPlayerCountParam,2)==1)
    then
        nC=nPlayerCountParam+1
    end
    local nC= nC/2


    for i=0,nR-1
    do
        for j=0,nC-1
        do
            if(j==nC-1 )
            then
                 if( math.mod(nPlayerCountParam,2)==0 or i<nR-1 )
                 then
                    local hPlayer = application.createUser ( )   
                    if ( hPlayer )    
                    then                
                  
                        user.addAIModel  ( hPlayer, "AIPlayer" )   
                        user.setViewport ( hPlayer, nScreenLeft+j*nScreenWidth /nC , nScreenBottom+(1-i)*nScreenHeight/nR , nScreenWidth / nC, nScreenHeight/nR  )
                 
                        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".L",nScreenLeft+j*nScreenWidth /nC )  
                        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".B",nScreenBottom+(1-i)*nScreenHeight/nR )  
                        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".W",nScreenWidth / nC )  
                        hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".H",nScreenHeight/nR  )  
                        table.add ( this.tUsers ( ), user.getID ( hPlayer ))
                        hashtable.add ( this.htValidPhase ( ),""..user.getID (  hPlayer ),true)
                         
                    end 
               
   
                end
            else
            
                local hPlayer = application.createUser ( )   
                if ( hPlayer )    
                then                
          
                    user.addAIModel  ( hPlayer, "AIPlayer" )   
                    user.setViewport ( hPlayer, nScreenLeft+j*nScreenWidth /nC , nScreenBottom+(1-i)*nScreenHeight/nR , nScreenWidth / nC, nScreenHeight/nR  )
                                     
                    hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".L", nScreenLeft+j*nScreenWidth /nC ) 
                    hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".B", nScreenBottom+(1-i)*nScreenHeight/nR ) 
                    hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".W",nScreenWidth / nC ) 
                    hashtable.add ( this.htPlayerViewport ( ),table.getSize ( this.tUsers ( ))..".H",nScreenHeight/nR ) 
                    table.add ( this.tUsers ( ), user.getID ( hPlayer ))
                    hashtable.add ( this.htValidPhase ( ),""..user.getID (  hPlayer ),true)
            
                end 
            end
        
        end
    end

end
    
  
 
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
