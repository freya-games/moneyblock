--------------------------------------------------------------------------------
--  Function......... : ApplyMarketCardEffect
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ApplyMarketCardEffect (nCardIndex,bEndTurn )
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htMarketCards ( ), "MarketCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="MarketCards.MarketCard."..nCardIndex
    else

        sTag="MarketCards.MarketCard"
    end

    local sEffectType =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Attributes.type")

    if(bEndTurn)
    then
        if(sEffectType=="cash")
        then

            this.MarketCash ( nCardIndex )
        elseif(sEffectType=="lessActiveCryptoPrice")
        then
            this.MarketLessActiveCryptoPrice ( nCardIndex )
        elseif(sEffectType=="cryptoCount")
        then

            this.MarketCryptoCount ( nCardIndex )

        end

    else
        if(sEffectType=="randomActionCardOnCrypto")
        then
            this.MarketRandomAction ( nCardIndex )

        elseif(sEffectType=="anonymousOrder")
        then
            this.MarketAnonymous ( nCardIndex )
         elseif(sEffectType=="marketCap")
        then

            this.MarketCap (  )
        end
    end





--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
