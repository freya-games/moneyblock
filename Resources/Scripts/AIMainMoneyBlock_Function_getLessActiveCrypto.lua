--------------------------------------------------------------------------------
--  Function......... : getLessActiveCrypto
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getLessActiveCrypto ( sMode)
--------------------------------------------------------------------------------
	local htBuy,htSell
    if(sMode and sMode=="global")
    then
    
        htBuy=this.htCurrentBuyOrdersGlobal ( )
        htSell=this.htCurrentSellOrdersGlobal ( )
    else
        
        htBuy=this.htCurrentBuyOrders ( )
        htSell=this.htCurrentSellOrders ( )
    end

local nMin=10000000000
local sReturnCrypto=nil

local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

for j=0,nCount-1
do
    local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
   
    local sType=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".Type")
    if(sType=="CRYPTO")
    then
        local nTotal=0

        for i =0, this.nPlayerCount ( )-1
        do
            local nBuy = hashtable.get( htBuy,"Player."..i.."."..sCryptoID..".Count" )
            local nSell = hashtable.get( htSell,"Player."..i.."."..sCryptoID..".Count" )
            if(nBuy)
            then
                 nTotal=nTotal+nBuy
            end
            if(nSell)
            then
                nTotal=nTotal+nSell
            
            end
            
        end
         if(nTotal==nMin)
        then
            if(math.roundToNearestInteger (  math.random ( 0,1 ))==0)
            then
                sReturnCrypto=sCryptoID
                nMin=nTotal   
            end
        end
        
        if(nTotal<nMin)
        then
            sReturnCrypto=sCryptoID
            nMin=nTotal
        end
    end
end


return sReturnCrypto



	
 
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
