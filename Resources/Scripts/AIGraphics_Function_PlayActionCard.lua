--------------------------------------------------------------------------------
--  Function......... : PlayActionCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.PlayActionCard ( )
--------------------------------------------------------------------------------
    
    if(hashtable.getSize ( this.htCurActiveCards ( ))==0 )then return end
    
    
    local nUserIndex = this.GetUserIndex ( user.getID ( this.getUser() ) )
    local sValue = hashtable.get ( this.htCurActiveCards ( ), ""..nUserIndex)
    
    --log.warning ( nCardID.." PLAY "..sCryptoID )

    if(sValue)
    then
        local sCryptoID, nCardID = this.ParsePlayedCard ( sValue )
        user.sendEvent (  this.getUser ( ), this.sAIPlayer ( ), "onPlayActionCard", nCardID, sCryptoID)
    end
    
    this.sTagCurUsedCard ( "" )
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
