--------------------------------------------------------------------------------
--  Function......... : UpdateMarketHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateMarketHUD ( )
--------------------------------------------------------------------------------
if(not this.bUseDebugHUD ( ))then


---------------------------------DEBUG HUD ---------------------------------
else

    local hComponentTrading = hud.getComponent ( this.getUser ( ), "PlayerHUD.Market" )
    hud.removeListAllItems ( hComponentTrading )
        
    local nIndexName=hud.addListItem ( hComponentTrading,"" )
    local nIndexPrice=hud.addListItem ( hComponentTrading,"" )
    local nIndexCurrentBlock=hud.addListItem ( hComponentTrading,"" )
    local nIndexValidation=hud.addListItem ( hComponentTrading,"" )
    local nIndexBuySellOrder=hud.addListItem ( hComponentTrading,"" )
    local iN=0
    for i=0,hashtable.getSize ( this.htResourcePrice ( ) )-1
    do

        local sID=hashtable.getKeyAt ( this.htResourcePrice ( ),i )
        local nPrice=hashtable.getAt ( this.htResourcePrice ( ),i )
        if(nPrice)
        then
            local sType=this.GetResourceType ( sID)
            if(sType=="CRYPTO")
            then
                hud.setListItemTextAt ( hComponentTrading, nIndexName, iN,""..this.GetResourceName (  sID))
                hud.setListItemTextAt ( hComponentTrading, nIndexPrice, iN,""..nPrice..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Currency")  )
                local nBlock=hashtable.get ( this.htCryptoCurrentBlock ( ),sID )
                if(nBlock)
                then
                    hud.setListItemTextAt ( hComponentTrading,  nIndexCurrentBlock, iN,"Bloc "..nBlock.."/"..this.GetCurrentBlockCount (sID ))
                 
               
                    local sValidation=this.GetBlockChainValidation (sID,nBlock)
                    if(sValidation)
                    then
                    
                         
                       hud.setListItemTextAt ( hComponentTrading,  nIndexValidation, iN,sValidation)
                    end
                end
                
                local nBuy=hashtable.get ( this.htBuyOrder ( ),sID )
                  if(not nBuy)
                  then
                  
                  
                    nBuy=0
                  
                    
                  end
             
                local nSell=hashtable.get ( this.htSellOrder ( ),sID )
                  if(not nSell)
                  then
                  
                  
                    nSell=0
                  
                    
                  end
                  
                  local sTotalMiningR=""
                    for iM=0,hashtable.getSize ( this.htTotalMining ( ) )-1
                    do

                        local sS=hashtable.getKeyAt ( this.htTotalMining ( ),iM )
                        local t=table.newInstance ( )
                        string.explode ( sS,t,"." )

                        local sCID=table.getAt ( t,0 )
                        local sRID=table.getAt ( t,1 )
                        if(sCID==sID)
                        then
                            sTotalMiningR=sTotalMiningR..sRID.." "..hashtable.getAt ( this.htTotalMining ( ),iM ).."-"
                        end
                    

                    end
                  
                       hud.setListItemTextAt ( hComponentTrading,  nIndexBuySellOrder, iN,"B "..nBuy.." - S "..nSell.."\n"..sTotalMiningR)
                  
                iN=iN+1
            end   
        end
    end
    this.UpdateMiningHUD ( )

end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
