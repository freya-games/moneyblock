--------------------------------------------------------------------------------
--  Handler.......... : onBuyCallback
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.onBuyCallback ( sResID,nAmount,nTotalCount,bCanBuy )
--------------------------------------------------------------------------------

    local sResName = this.getName ( sResID )

    if(bCanBuy)
    then
        local sMes
        if(this.getType ( sResID )=="CRYPTO")
        then
            if(hashtable.contains ( this.htPendingOrders ( ),sResID..".Buy" ))
            then
                hashtable.set ( this.htPendingOrders ( ),sResID..".Buy" ,nAmount)
            else
                hashtable.add ( this.htPendingOrders ( ),sResID..".Buy" ,nAmount)
            end


             if(hashtable.contains ( this.htPendingOrders ( ), sResID..".Sell" ))
            then
                hashtable.set ( this.htPendingOrders ( ) ,sResID..".Sell" ,0)

            end
            sMes="Pending Buy "
        else
             sMes="Bought "
        end

        this.openDialogBox (sMes..nAmount.." "..sResName )
        user.sendEvent ( this.getUser ( ), this.sAIGraphics ( ), "onBuyCallback", sResID,nAmount,nTotalCount,bCanBuy  )
    else

        this.openDialogBox ( "Cannot buy "..nAmount.." "..sResName )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
