--------------------------------------------------------------------------------
--  Handler.......... : onAddMiningResourceCallback
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onAddMiningResourceCallback (sCryptoID,sResID,nAmount)
--------------------------------------------------------------------------------


local nBlock=hashtable.get ( this.htCryptoCurrentBlock ( ),sCryptoID )
if(nBlock)
then
    local currentResCount=hashtable.get ( this.htResourceCount ( ),sResID)
       if(nAmount> currentResCount)
        then
            --DISPLAY CANT MINE MESSAGE
            if(this.bUseDebugHUD ( ))then
               this.OpenDialogBox ("Cannot Add, not enough resources : "..nAmount.." "..this.GetResourceName ( sResID ).." to mine "..this.GetResourceName ( sCryptoID ) )
            else
                log.message ( "Cannot Add, not enough resources : "..nAmount.." "..this.GetResourceName ( sResID ).." to mine "..this.GetResourceName ( sCryptoID ) )
            end
        else
            if(hashtable.contains (this.htMining ( ),sCryptoID.."."..sResID  ))
            then

                hashtable.set (this.htMining ( ),sCryptoID.."."..sResID,nAmount  )
            else
                hashtable.add (this.htMining ( ),sCryptoID.."."..sResID ,nAmount )
            end
                
            
            if(not this.bUseDebugHUD ( ))then
            
                --log.message ( nAmount.." "..this.GetResourceName ( sResID ).." added to mine "..this.GetResourceName( sCryptoID )  )
                this.UpdateMiningHUD (  )
                
            ------------------DEBUG HUD------------------
            else
            
                this.OpenDialogBox (nAmount.." "..this.GetResourceName ( sResID ).." added to mine "..this.GetResourceName( sCryptoID ) )
            
                local hComponentResource = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResource" )
                hud.removeListAllItems ( hComponentResource )

                local hComponent = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResourceValue" )
                hud.removeListAllItems ( hComponent )
                table.empty (  this.tMiningResourceHUD  ( ) )

                 
                this.UpdateMiningCryptoAllHUD (  )
                this.UpdateHUD (  ) 
            end
        end
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
