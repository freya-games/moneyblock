--------------------------------------------------------------------------------
--  Function......... : ActionCryptoCount
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionCryptoCount ( nCardIndex,sCryptoID)
--------------------------------------------------------------------------------

    local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end



    local nMin =hashtable.get (this.htActionCards ( ),sTag..".Effect.Min")
    if(nMin)
    then

        local nMax =hashtable.get (this.htActionCards ( ),sTag..".Effect.Max")

        local nAmount=nMin

        if(nMin~=nMax)
        then
            nAmount=math.roundToNearestInteger (  math.random ( nMin,nMax ))
        end

        for i =0, this.nPlayerCount ( )-1
        do

            local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

--             for j=0,nCount-1
--             do
--


                    this.ChangePlayerResource (  i,sCryptoID,nAmount )

                --local sCryptoID= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..j..".ID")
--                   local nLock=  hashtable.get (this.htRuntimeData ( ), "Player."..i.."."..sCryptoID..".Count.Lock" )
--                   if(nLock)
--                   then
--
--                   else


                 --  this.ChangePlayerResource (  i,sCryptoID,nAmount )
--                   end

           -- end
        end
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
