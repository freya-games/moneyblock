--------------------------------------------------------------------------------
--  Function......... : CreatePlayerMainHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreatePlayerMainHUD ( )
--------------------------------------------------------------------------------




    --CREATE MAIN HUD
    local nCryptoCount = hashtable.getSize ( this.htPrefixCryptoHUD ( ) )
    local nResourceCount = hashtable.getSize ( this.htPrefixMiningResourceHUD ( ) )
    local hUser = this.getUser ( )


    local sPrefixMainHUD = "PlayerMainHUD"
    hud.newTemplateInstance ( hUser, sPrefixMainHUD, sPrefixMainHUD )

    local hContainerCrypto = hud.getComponent ( hUser, sPrefixMainHUD..".ContainerCrypto" )
    local hContainerResources = hud.getComponent ( hUser, sPrefixMainHUD..".ContainerResources" )
    hud.newTemplateInstance ( hUser, "CardHUD", sPrefixMainHUD )

    local nSpaceCrypto = 2
    local nSpaceResource = 10

    --ADD ALL CRYPTO HUD
    if(hContainerCrypto)then
        for i =0 , nCryptoCount-1
        do
            local sResID = hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ), i )
            local r, g, b = this.GetResourceColor ( sResID )
            local sLogo = this.GetResourceLogo ( sResID )
            local sName = this.GetResourceName ( sResID )

            --CREATE HUD
            local sPrefixCryptoHUD = "Crypto_"..i
            hashtable.set ( this.htPrefixCryptoHUD ( ), sResID, sPrefixCryptoHUD )
            hud.newTemplateInstance ( hUser, "Crypto", sPrefixCryptoHUD)
            local hChild = hud.getComponent ( hUser, sPrefixCryptoHUD..".ContainerGlobal" )
            local hCName = hud.getComponent ( hUser, sPrefixCryptoHUD..".LabelCoinName" )
            local hCCoin = hud.getComponent ( hUser, sPrefixCryptoHUD..".LabelCoin" )
            local hCSprite = hud.getComponent ( hUser, sPrefixCryptoHUD..".SpriteCrypto" )


            --SET HUD TRANS
            hud.setComponentContainer ( hChild, hContainerCrypto)
            hud.setComponentPosition ( hChild, (i+0.5)*100/(nCryptoCount), 50 )
            hud.setComponentSize ( hChild, 100/nCryptoCount -  nSpaceCrypto, 100 )

            --INIT NAME AND COLOR
            if(hCName)then hud.setLabelText ( hCName, sName ) end
            if(hCName)then hud.setComponentForegroundColor ( hCName, r, g, b, 255 ) end
            if(hCCoin)then hud.setComponentForegroundColor ( hCCoin, r, g, b, 255 ) end
            if(hCSprite and sLogo~="")then hud.setComponentBackgroundImage ( hCSprite, sLogo ) end

        end
    end

    --ADD ALL RESOURCES HUD
    if(hContainerResources)then
        for i = 0, nResourceCount -1
        do
            local sResID = hashtable.getKeyAt ( this.htPrefixMiningResourceHUD ( ), i )
            -- local r, g, b = this.GetResourceColor ( sResID )
            local sLogo = this.GetResourceLogo ( sResID )

             --CREATE HUD
            local sPrefixResourceHUD = "Resource_"..i
            hashtable.set ( this.htPrefixMiningResourceHUD ( ), sResID, sPrefixResourceHUD )
            hud.newTemplateInstance ( hUser, "Resource", sPrefixResourceHUD)
            local hChild = hud.getComponent ( hUser, sPrefixResourceHUD..".ContainerGlobal" )
            local hCSprite= hud.getComponent ( hUser, sPrefixResourceHUD..".SpriteResource" )


            --SET HUD TRANS
            hud.setComponentContainer ( hChild, hContainerResources)
            hud.setComponentPosition ( hChild, 50, 100-(i+0.5)*100/(nResourceCount) )
            hud.setComponentSize ( hChild, 100, 100/nResourceCount -  nSpaceResource )
            --if(hCSprite)then hud.setComponentBackgroundColor ( hCSprite, r, g, b, 255 ) end
            if(hCSprite)then hud.setComponentBackgroundImage( hCSprite, sLogo ) end

        end
    end


--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
