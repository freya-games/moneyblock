--------------------------------------------------------------------------------
--  Function......... : LerpTokens
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.LerpTokens ( )
--------------------------------------------------------------------------------
	
    local nSpeedRot = 30
    
	for nIndexToken=0, table.getSize ( this.tTokenToRotate ( ) )-1
    do
        
    	local hToken = table.getAt ( this.tTokenToRotate ( ), nIndexToken )
        local nValue = application.getLastFrameTime ( )*nSpeedRot
        
        local x, y, z = object.getRotation ( hToken, object.kGlobalSpace )
        --if(nIndexToken==0)then log.message (y) end
        
        object.setRotation ( hToken, x, math.mod (  y+nValue, 360), z, object.kGlobalSpace  )
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
