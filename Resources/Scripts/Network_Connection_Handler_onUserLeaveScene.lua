--------------------------------------------------------------------------------
--  Handler.......... : onUserLeaveScene
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Network_Connection.onUserLeaveScene ( nUserID )
--------------------------------------------------------------------------------

    if ( hashtable.contains ( this.aPlayerName ( ), ""..nUserID ) )
    then
        hashtable.remove  ( this.aPlayerName ( ), ""..nUserID )
    end

    --inform Main AI about the disparition of the user
    user.sendEvent ( this.getUser ( ), "Network_Main", "onRemovePlayer", nUserID )

    log.message ( "User Leave Scene : ", nUserID )
    --remove the user to the voip diffusion list ( if the diffusion is not started, this will have no impact )
    microphone.removeUserFromDiffusionList ( nUserID )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
