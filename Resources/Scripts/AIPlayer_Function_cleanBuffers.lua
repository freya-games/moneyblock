--------------------------------------------------------------------------------
--  Function......... : cleanBuffers
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIPlayer.cleanBuffers ( )
--------------------------------------------------------------------------------
	
hashtable.empty (this.htMining ( )  )
hashtable.empty ( this.htTotalMining ( ) )
hashtable.empty ( this.htPendingOrders ( ) )
hashtable.empty ( this.htBuyOrder ( ) )
hashtable.empty ( this.htSellOrder ( ) )
hashtable.empty ( this.htCardPlayed ( ) )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
