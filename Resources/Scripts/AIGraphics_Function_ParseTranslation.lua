--------------------------------------------------------------------------------
--  Function......... : ParseTranslation
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.ParseTranslation ( )
--------------------------------------------------------------------------------
	
if(this.xTranslation ( )~="")
then
 
    local hRootElement = xml.getRootElement ( this.xTranslation ( ) )

    
    if ( hRootElement )
    then
            local ht=hashtable.newInstance ( )
        this.RecurseXMLNode ( hRootElement,xml.getElementName ( hRootElement ),this.htTranslation( ),      ht)
           
    end


end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
