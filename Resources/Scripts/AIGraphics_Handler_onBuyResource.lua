--------------------------------------------------------------------------------
--  Handler.......... : onBuyResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onBuyResource ( sType, sListTag,sTag )
--------------------------------------------------------------------------------

if(not this.bUseDebugHUD ( ))then

    --GET RES ID
    local sID = ""
    for i=0 , hashtable.getSize ( this.htPrefixMiningResourceHUD ( ) ) -1
    do
    	if( string.contains ( sTag, hashtable.getAt ( this.htPrefixMiningResourceHUD ( ), i  )))then
            sID = hashtable.getKeyAt ( this.htPrefixMiningResourceHUD ( ), i )
            break
        end
    end
    if(sID == "")then return end
    
    --is able to buy ?
    local cash = this.nCash ( )
    if(this.nPhase ( )==2)then cash = cash - this.GetTotalBuyAmount (  )end
    if(cash< hashtable.get ( this.htResourcePrice ( ) , sID ))then
        log.message ( "too much expensive" )
        return
    end

    user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onBuy" , sID, 1)
	
else
    if(sType=="MINER")
    then

        local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

        local nItem=hud.getListSelectedItemAt ( hC,0 )
        if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
        then
            user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onBuy" , table.getAt ( this.tMinerHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
        
        end
    elseif(sType=="ENERGY")
    then
        local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

        local nItem=hud.getListSelectedItemAt ( hC,0 )
        if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
        then
            user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onBuy" , table.getAt ( this.tEnergyHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
        
        end
    elseif(sType=="CRYPTO")
    then
            local hC=hud.getComponent ( this.getUser ( ),"PlayerHUD."..sListTag )

        local nItem=hud.getListSelectedItemAt ( hC,0 )
        if(hashtable.contains ( this.htHUDVars ( ),sTag ) and nItem>-1)
        then
            user.sendEvent ( this.getUser ( ), this.sAIPlayer ( ), "onBuy" , table.getAt ( this.tCryptoHUD ( ),nItem ),hashtable.get ( this.htHUDVars ( ),sTag ))
        
        end
    end
end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
