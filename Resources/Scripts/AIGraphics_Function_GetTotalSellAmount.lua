--------------------------------------------------------------------------------
--  Function......... : GetTotalSellAmount
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetTotalSellAmount ( )
--------------------------------------------------------------------------------
	
local nTotal = 0
for i=0, hashtable.getSize ( this.htHUDTradeValue ( ) )-1
do
    local sCryptoID=hashtable.getKeyAt ( this.htHUDTradeValue ( ),i )
    local nCount = hashtable.getAt ( this.htHUDTradeValue ( ), i)
    local nPrice=hashtable.get (this.htResourcePrice ( ), sCryptoID )
    
    if(nPrice)
    then
        if(nCount <0)then
            nTotal=nTotal+ math.abs ( nPrice*nCount)
        end
    end
end
	
return nTotal

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
