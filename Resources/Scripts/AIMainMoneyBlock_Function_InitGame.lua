--------------------------------------------------------------------------------
--  Function......... : InitGame
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.InitGame ( )
--------------------------------------------------------------------------------


local nCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")

for i=0,nCount-1
do
    hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".Count",hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Count"))
    hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".Price",hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Price"))
    hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".Type",hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type"))
    hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".PriceMini",hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".PriceMini"))
   local coef=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".MiningRewardCoef")
   if(coef)
   then
      hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".MiningRewardCoef",coef)
   end





 -- BlockChain

  local nBlockChainCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".BlockChain.ChildCount")
    if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Type")=="CRYPTO")
    then
      if(nBlockChainCount)
        then
            hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".BlockCount", nBlockChainCount)
        else
            log.error ( "AIMainMoneyBlock.InitGame  : No BlockCount Tag for "..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID") )
        end
         local nCurrentBlock =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".CurrentBlock")
        local nC=0
        if(nCurrentBlock)
        then
            nC=nCurrentBlock

        end
        hashtable.add ( this.htRuntimeData ( ),"Game.Resources."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".CurrentBlock", nC)

    end


    for j =0, this.nPlayerCount ( )-1
    do
    local nInitValue= hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".Init")
    if(not nInitValue)
    then
        nInitValue=0
    end

        hashtable.add ( this.htRuntimeData ( ),"Player."..j.."."..hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")..".Count",nInitValue)
    end
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
