--------------------------------------------------------------------------------
--  Function......... : EnableTradeHUD
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableTradeHUD ( bEnable, sCryptoID )
--------------------------------------------------------------------------------


if(sCryptoID ~= "")then

    local sAction = ".CloseTradePanel"
    if(bEnable)then	
        sAction = ".OpenTradePanel" 
    end

    local nAnimTime = 440
    local sPrefix = hashtable.get ( this.htPrefixCryptoHUD ( ), sCryptoID)
        
    if(sPrefix ~= "")then
        if(hud.isActionRunning ( this.getUser ( ), sPrefix..sAction ))then hud.stopAction ( this.getUser ( ), sPrefix..sAction )end
        hud.callAction ( this.getUser ( ), sPrefix..sAction, nAnimTime  )
    end

end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
