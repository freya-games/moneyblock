--------------------------------------------------------------------------------
--  Function......... : isMiningResourceValid
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.isMiningResourceValid( sResID,sMiningResID,nCryptoCurrentBlock   )
--------------------------------------------------------------------------------
	


    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
	for iR=0, nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..iR..".ID")
        if(sID ==sResID)
        then
            
            local nResourceIndex=iR
           
             local nBlockChainCount =hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.ChildCount")
              for i=0,nBlockChainCount-1
              do
                local nOrder=i
                if(nOrder==nCryptoCurrentBlock)
                then
                    local sValidation
                    local sTag
                    if(hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.ChildCount")>1)
                    then
                          sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block."..i
                    else
                          sTag="MoneyBlockConfig.Game.Resources.Resource."..nResourceIndex..".BlockChain.Block"
                    
                    end
                    
                    local nResCount=hashtable.get ( this.htGameConfig ( ),sTag..".ChildCount")
                    if(nResCount>1)
                    then
                        for j=0,nResCount-1
                        do
                            local sBCResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource."..j..".ID")
                            if(sBCResID==sMiningResID)
                            then
                                return true
                            end
                          
                        end
                    end
                    if(nResCount==1)
                    then
                        local sBCResID=hashtable.get ( this.htGameConfig ( ),sTag..".Resource.ID")
                     if(sBCResID==sMiningResID)
                            then
                                return true
                            end
                    end
                    
                end
                
              end
            
        end
    
    end

 return false
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
