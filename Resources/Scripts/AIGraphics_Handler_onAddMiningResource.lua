--------------------------------------------------------------------------------
--  Handler.......... : onAddMiningResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onAddMiningResource ( sTag, nNewValue, nAdded )
--------------------------------------------------------------------------------

if(not this.bUseDebugHUD ( ))then

    local nMaxAmount = 10 --TODO EXTERNALIZE VALUE
    nNewValue = math.clamp ( nNewValue + nAdded, 0, nMaxAmount)

    --Invalid crypto ID
    if(this.sCurMiningCryptoID ( ) == "")then return end

    local hUser = this.getUser ( )


    --GET RES ID
    local sID = ""
    for i=0 , hashtable.getSize ( this.htPrefixMiningSliderHUD ( ) ) -1
    do
    	if( string.contains ( sTag, hashtable.getAt ( this.htPrefixMiningSliderHUD ( ), i  )))then
            sID = hashtable.getKeyAt ( this.htPrefixMiningSliderHUD ( ), i )
            break
        end
    end
    if(sID == "")then return end
   

    -- COUNT RESOURCES
    local nRequiredAmount = this.GetRequiredResourceToMine ( sID, this.sCurMiningCryptoID ( ))
    local nCurrentResCount = this.GetReminigResourceToMine ( sID, this.sCurMiningCryptoID ()  )


    --ENOUGHT RESOURCE ?
    if(nCurrentResCount - nNewValue <0)then 
        
        log.message ( "not enought resource  "..nCurrentResCount.." / "..nRequiredAmount )
        
        user.sendEvent(this.getUser ( ), this.sAIPlayer ( ), "onAddMiningResource", this.sCurMiningCryptoID ( ),sID,nCurrentResCount )
    else
        user.sendEvent(this.getUser ( ), this.sAIPlayer ( ), "onAddMiningResource", this.sCurMiningCryptoID ( ),sID,nNewValue )
    end
    
     --DECREASE OPACITY BUTTON +
    local nOpacity = 255
    if(nNewValue <=0)then nOpacity = 127 end
    local sTag = hashtable.get( this.htPrefixMiningSliderHUD ( ), sID)..".ButtonAdd"
    local hButton = hud.getComponent ( hUser, sTag )
    if(hButton)then  hud.setComponentOpacity ( hButton, nOpacity ) end

--------------------------------------------------------------
else

    local hC = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResource" )
    local nItem=hud.getListSelectedItemAt ( hC,0 )
    if(nItem>-1)
    then

        local sID=table.getAt ( this.tMiningResourceHUD ( ),nItem )
        hC = hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningResourceValue" )

        nItem=hud.getListSelectedItemAt ( hC,0 )
        if(nItem>-1)
        then
            local nMiningResourceCount=string.toNumber (  hud.getListItemTextAt ( hC,nItem, 0))



            hC= hud.getComponent ( this.getUser ( ), "PlayerHUD.MiningCrypto" )
            nItem=hud.getListSelectedItemAt ( hC,0 )
            if(nItem>-1)
            then
                local sMiningCryptoID=table.getAt ( this.tMiningCryptoHUD ( ),nItem )

                user.sendEvent(this.getUser ( ), this.sAIPlayer ( ), "onAddMiningResource", sMiningCryptoID,sID,nMiningResourceCount )
            else
                this.OpenDialogBox (this.Translate ( "SelectMiningCryptoError" )  )
            end
        else
            this.OpenDialogBox (this.Translate ( "SelectMiningResourceAmountError" )  )
        end
    else
        this.OpenDialogBox (this.Translate ( "SelectMiningResourceError" )  )
    end
end	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
