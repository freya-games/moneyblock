--------------------------------------------------------------------------------
--  Handler.......... : onEndTurnButtonIdleLoop
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onEndTurnButtonIdleLoop ( sTagProgress, bReverse, nDuration )
--------------------------------------------------------------------------------
	
    local hComponent = hud.getComponent ( this.getUser ( ), sTagProgress )
    
   
   
    
    if(hComponent)then
        local kProgress = hud.kProgressTypeLeftToRight
        hud.setProgressValue ( hComponent, 0 )
        if(bReverse)then 
            hud.setProgressValue ( hComponent, 255 )
            kProgress = hud.kProgressTypeRightToLeft 
        end
        hud.setProgressType ( hComponent, kProgress )
    end
    
    

        
         if(bReverse)then
            hud.callAction ( this.getUser ( ), "PlayerMainHUD.EndTurnIdleB", this.nDurationEndTurnIdle ( )) 
               
        else
            hud.callAction ( this.getUser ( ), "PlayerMainHUD.EndTurnIdle", this.nDurationEndTurnIdle ( )) 
        end
            
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
