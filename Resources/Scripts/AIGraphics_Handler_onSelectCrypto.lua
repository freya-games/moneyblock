--------------------------------------------------------------------------------
--  Handler.......... : onSelectCrypto
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onSelectCrypto ( sTag )
--------------------------------------------------------------------------------
local sCryptoID =""
local sPrefix = ""

for i=0, hashtable.getSize ( this.htPrefixCryptoHUD ( ) ) -1
do
    sPrefix = hashtable.getAt ( this.htPrefixCryptoHUD ( ),i )
    
    if(sPrefix..".ContainerGlobal" == sTag)then
        sCryptoID = hashtable.getKeyAt ( this.htPrefixCryptoHUD ( ),i )
        break
    end
end
    
    
if(sCryptoID ~= "")then
    
    
    --OPEN / CLOSE TRADE PANEL
    if(this.nPhase ( ) == 2 and this.bPhaseValided ( )== false)then

        this.EnableTradeHUD ( false, this.sCurTradeCryptoID ( ) )
        
        if(sCryptoID ~= this.sCurTradeCryptoID ( ))then
            this.EnableTradeHUD ( true, sCryptoID )
            this.sCurTradeCryptoID ( sCryptoID )
        else
            this.sCurTradeCryptoID ("")
        end
        
        --ENABLE ROT
        this.nTimeLerpBlockChain (0)
        
        
    elseif(this.nPhase ( ) == 3 and this.bPhaseValided ( )== false)then

        local hObj = hashtable.get ( this.htMiningModel ( ) , sCryptoID )

        --RIG SELECTED
        if(hObj)then
            this.sCurMiningCryptoID ( sCryptoID )
            this.EnableMiningPanelHUD ( true, hObj )
            this.UpdateMiningHUD(true)
        end
    end

end
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
