--------------------------------------------------------------------------------
--  Function......... : ActionNoPriceUpdate
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.ActionNoPriceUpdate(nCardIndex,sCryptoID )
--------------------------------------------------------------------------------

 local nCardCount = hashtable.get (this.htActionCards ( ), "ActionCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="ActionCards.ActionCard."..nCardIndex
    else

        sTag="ActionCards.ActionCard"
    end


 if(hashtable.contains ( this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price.Lock"))
 then
    hashtable.set (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price.Lock",true )
 else
    hashtable.add (this.htRuntimeData ( ), "Game.Resources."..sCryptoID..".Price.Lock",true )
 end








--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
