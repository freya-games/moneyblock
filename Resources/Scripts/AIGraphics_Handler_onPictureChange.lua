--------------------------------------------------------------------------------
--  Handler.......... : onPictureChange
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.onPictureChange ( nDirection )
--------------------------------------------------------------------------------

this.nPictureID (math.mod ( this.nPictureID ( )+nDirection,this.nTotalPicture ( ) ))
    if(this.nPictureID ( )==-1)
    then
        this.nPictureID ( this.nTotalPicture ( )-1)
    end
    local hC=hud.getComponent ( this.getUser ( ),"PlayerDetails.Picture" )

hud.setComponentBackgroundImage ( hC,"Picture"..this.nPictureID ( ) )

local sAction = ""
if(nDirection>0)then
    sAction = "PlayerDetails.ClicRightArrow"
else
    sAction = "PlayerDetails.ClicLeftArrow"
end

if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction ) end
hud.callAction ( this.getUser ( ), sAction  )
--hC=hud.getComponent ( this.getUser ( ),"PlayerDetails.Name" )


--hud.setEditText( hC,table.getAt ( this.tRandomName ( ),this.nPictureID ( ) ) )

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
