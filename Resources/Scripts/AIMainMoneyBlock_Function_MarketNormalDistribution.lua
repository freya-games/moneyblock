--------------------------------------------------------------------------------
--  Function......... : MarketNormalDistribution
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.MarketNormalDistribution ( nCardIndex)
--------------------------------------------------------------------------------

 local nCardCount = hashtable.get (this.htMarketCards ( ), "MarketCards.ChildCount")

    local sTag
    if(nCardIndex and nCardCount>1)
    then
        sTag="MarketCards.MarketCard."..nCardIndex
    else

        sTag="MarketCards.MarketCard"
    end

     local nExpectation =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Distribution.Expectation")

     local nDeviation =hashtable.get (this.htMarketCards ( ),sTag..".Effect.Distribution.Deviation")
     if(nExpectation and nDeviation)
     then
        return math.gaussianRandom (  nExpectation,nDeviation)
    else
        return nil
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
