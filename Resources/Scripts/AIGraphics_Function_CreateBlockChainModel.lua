--------------------------------------------------------------------------------
--  Function......... : CreateBlockChainModel
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateBlockChainModel ( )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
    
    --CREATE PARENT
    local hBlockChainParent = scene.createRuntimeObject ( hScene, "" )
    scene.setObjectTag ( hScene, hBlockChainParent, "BlockChain" )
    this.hBlockChain ( hBlockChainParent )
    
    
    --GET CHAIN COUNT
    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
	for i=0 , nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        
        if(this.GetResourceType ( sID )=="CRYPTO" )then
            local nBlockCount = hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".BlockChain.ChildCount")
            hashtable.add ( this.htBlockChainCount ( ), sID, nBlockCount )
        end
    end
    
    
    local x, y, z = this.GetBodyPosition ( )
    local nSpaceBlock = 0.5
    local nScale = 0.4
    
    local nCrypto = hashtable.getSize ( this.htBlockChainCount ( ) )
    for i=0 , nCrypto-1
    do
        local sCryptoID = hashtable.getKeyAt ( this.htBlockChainCount ( ), i)
        local nBlockCount = hashtable.getAt ( this.htBlockChainCount ( ), i)
        local r,g,b = this.GetResourceColor ( sCryptoID )
        
        for j=0 , nBlockCount-1
        do
            if(not scene.getTaggedObject ( hScene, sCryptoID.."."..nBlockCount-j ))then
                local hBlock = scene.createRuntimeObject ( hScene, "P_CubeTrade" )
                
                scene.setObjectTag ( hScene, hBlock, sCryptoID.."."..nBlockCount-j )
                
                object.setRotation ( hBlock, 0, 360/nCrypto * (i+0.5), 0, object.kGlobalSpace )
                object.setTranslation ( hBlock, x, 0, z, object.kGlobalSpace )
                object.setTranslation ( hBlock, 0,0, -(j+1) * nSpaceBlock, object.kLocalSpace )
                object.setScale ( hBlock, 0.2,0.05,0.2 )
                object.setParent ( hBlock, hBlockChainParent, true )
                
                --HIDE SUB OBJ
                for nIndexSubObj=1 ,  object.getChildCount ( hBlock )-1
                do
                	object.setVisible ( object.getChildAt ( hBlock, nIndexSubObj ), false )
                end
               
               local hToken = object.getChildAt ( hBlock, 1 )
               if(hToken)then
                    table.add ( this.tTokenToRotate ( ), hToken  )
                    shape.setMeshMaterial ( hToken, "Block_Trame_"..i )
                    
                    --SET RANDOM START ROT
                    local rx, ry, rz = object.getRotation ( hToken, object.kGlobalSpace )
                    object.setRotation ( hToken, rx, math.random ( 0,360 ), rz, object.kGlobalSpace  )
               end
            end
        end
    end
	this.UpdateBlockChainModel (  )
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
