--------------------------------------------------------------------------------
--  Function......... : GetReminigResourceToMine
--  Author........... : 
--  Description...... : return how many resource the player can still use
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetReminigResourceToMine ( sResID, sIgnoredCryptoID )
--------------------------------------------------------------------------------
	
local currentResCount=hashtable.get ( this.htResourceCount ( ),sResID)
    
    
for i=0, hashtable.getSize ( this.htResourceCount ( ) )-1
do
    local sCryptoID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
    if(this.GetResourceType (sCryptoID )=="CRYPTO" and sCryptoID ~= sIgnoredCryptoID )
    then
        --GET RESOURCE USED
        if(hashtable.contains (this.htMining ( ),sCryptoID.."."..sResID  ))then
            
            currentResCount = currentResCount - hashtable.get (this.htMining ( ),sCryptoID.."."..sResID  )
        end
    end
end
    
return currentResCount
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
