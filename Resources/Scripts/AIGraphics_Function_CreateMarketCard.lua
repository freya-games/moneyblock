--------------------------------------------------------------------------------
--  Function......... : CreateMarketCard
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.CreateMarketCard ( )
--------------------------------------------------------------------------------
    
    this.sPrefixMarketCardHUD ( "MarketCard")
    hud.newTemplateInstance ( this.getUser ( ), "CardHolderHUD", "MarketCardHolder"  )
	hud.newTemplateInstance ( this.getUser ( ), "Card", "MarketCard" )
    
    local hComponent = hud.getComponent ( this.getUser ( ), "MarketCard.ContainerGlobal" )
    hud.setComponentIgnoredByMouse ( hComponent, true )
    hud.setComponentActive ( hComponent, false )
    
    local hParent = hud.getComponent ( this.getUser ( ), "MarketCardHolder.ContainerCard" )
    if(hComponent and hParent)then
        hud.setComponentContainer ( hComponent, hParent )
        hud.setComponentSize ( hComponent, 100*this.nRatioCardHUD ( ),100 )
        hud.setComponentPosition ( hComponent, 50,50 )
    end
	
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
