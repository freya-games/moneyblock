--------------------------------------------------------------------------------
--  Handler.......... : onSetTarget
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function Camera.onSetTarget (nTime, hTargetVal, nDistX, nDistY, nDistZ  )
--------------------------------------------------------------------------------
	
	this.hTarget ( hTargetVal)
    
    local sX, sY, sZ = object.getTranslation ( this.getObject ( ), object.kGlobalSpace )
    local srX, srY, srZ = object.getRotation ( this.getObject ( ), object.kGlobalSpace )
    
    local pX, pY, pZ = object.getTranslation ( hTargetVal, object.kGlobalSpace )
    object.setTranslation ( this.getObject ( ), pX + nDistX, pY + nDistY, pZ +nDistZ,  object.kGlobalSpace )
    object.lookAt ( this.getObject ( ), pX, pY, pZ, object.kGlobalSpace, 1  )
    
    
    local endX, endY, endZ = object.getTranslation ( this.getObject ( ), object.kGlobalSpace )
    local endRX, endRY, endRZ = object.getRotation ( this.getObject ( ), object.kGlobalSpace )
    object.setTranslation ( this.getObject ( ),  sX, sY, sZ,  object.kGlobalSpace )
    object.setRotation ( this.getObject ( ), srX, srY, srZ,  object.kGlobalSpace )
    
    
    this.nLerpCurTime ( 0 )
    this.nLerpTotalTime ( nTime )
	this.SetTarget ( endX, endY, endZ, endRX, endRY, endRZ )

	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
