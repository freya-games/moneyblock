--------------------------------------------------------------------------------
--  Function......... : Get3DAnchorPosition
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.Get3DAnchorPosition ( )
--------------------------------------------------------------------------------
	
	local hUser = this.getUser ( )
    local hScene = user.getScene ( hUser )
    

    local hMinDummy = scene.getTaggedObject ( hScene, "Min" )
    local hMaxDummy = scene.getTaggedObject ( hScene, "Max" )
    
    if(not hMinDummy )then
        hMinDummy = scene.createRuntimeObject ( hScene, "" )
        scene.setObjectTag ( hScene, hMinDummy, "Min" )
    end
    if(not hMaxDummy )then
        hMaxDummy = scene.createRuntimeObject ( hScene, "" )
        scene.setObjectTag ( hScene, hMaxDummy, "Max" )
    end
    
    
    local nMinX, nMinY, nMinZ = object.getTranslation ( hMinDummy, object.kGlobalSpace )
    local nMaxX, nMaxY, nMaxZ = object.getTranslation ( hMaxDummy, object.kGlobalSpace )
    
    return nMinX, nMinY, nMinZ, nMaxX, nMaxY, nMaxZ
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
