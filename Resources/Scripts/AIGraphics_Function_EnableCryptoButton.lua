--------------------------------------------------------------------------------
--  Function......... : EnableCryptoButton
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableCryptoButton ( bEnable )
--------------------------------------------------------------------------------
	
	for nIndexCrypto=0, hashtable.getSize ( this.htPrefixCryptoHUD () ) -1
    do
    	local sPrefix = hashtable.getAt ( this.htPrefixCryptoHUD (), nIndexCrypto  )
        local hCryptoButton = hud.getComponent ( this.getUser ( ), sPrefix..".ButtonCrypto" )
        
        if(hCryptoButton)then
            hud.setComponentActive ( hCryptoButton, bEnable )
        end
    end
    
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
