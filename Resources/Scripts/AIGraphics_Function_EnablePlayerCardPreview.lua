--------------------------------------------------------------------------------
--  Function......... : EnablePlayerCardPreview
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnablePlayerCardPreview ( bEnable )
--------------------------------------------------------------------------------
	
    local hUser = this.getUser ( )
    
    
    
    if(not bEnable)then
    
        --HIDE ALL HUD
        for i = 0, hashtable.getSize( this.htPrefixSlotCardHUD ( )) - 1
        do
            local sPrefix = hashtable.getAt ( this.htPrefixSlotCardHUD ( ), i )
            local sCryptoID = hashtable.getKeyAt ( this.htPrefixSlotCardHUD ( ), i )

            for j=0 , table.getSize ( this.tUsers() )-1
            do
                local nPlayerID =  table.getAt ( this.tUsers(), j ) 
                local sPrefixPlayerButton = sPrefix.."."..nPlayerID
                
                --HIDE HUD
                local hC = hud.getComponent ( hUser, sPrefixPlayerButton..".ContainerGlobal" )
                
                if(hC)then
                    hud.setComponentVisible ( hC, false )
                    hud.setComponentActive( hC, false )
                end
            end
        end
    
    else

        -- ENABLE PLAYER BUTTON ON ACTIVE SLOT
        for i = 0, hashtable.getSize( this.htCardPlayed ( )) - 1
        do
            local sPlayer = hashtable.getKeyAt ( this.htCardPlayed ( ), i )
            local sValue = hashtable.getAt ( this.htCardPlayed ( ), i )
            
            local sCryptoID, nCardID = this.ParsePlayedCard ( sValue )
            local sPrefix = hashtable.get ( this.htPrefixSlotCardHUD ( ), sCryptoID )
            local sPrefixPlayerButton = sPrefix.."."..sPlayer
            
            --SET VISIBLE PLAYER BUTTONS
            local hC = hud.getComponent ( hUser, sPrefixPlayerButton..".ContainerGlobal" )
            if(hC)then
                hud.setComponentVisible ( hC, true )
                hud.setComponentActive( hC, true )
            end
            
            --SET PLAYER SPRITE
            hC = hud.getComponent ( hUser, sPrefixPlayerButton..".SpritePlayer" )
            if(hC)then
                hud.setComponentBackgroundImage ( hC, "Picture"..table.getAt ( this.tPlayersPictureID ( ), this.GetUserIndex( sPlayer ) ))
            end
        end
        

        --DISPLAY CARD PLAYED AND HIDE SLOT UNUSED
        for i=0 , hashtable.getSize ( this.htPrefixSlotCardHUD ( ) ) -1
        do
            local sPrefix = hashtable.getAt (  this.htPrefixSlotCardHUD ( ), i )
            local bVisible = false
        
            
        	--DISPLAY PREVIEW CARD
            if(hashtable.contains ( this.htSlotPreviewCardID ( ), sPrefix ))then
                local nCardID = hashtable.get(this.htSlotPreviewCardID ( ), sPrefix)
                this.DisplayPlayedCard ( sPrefix, nCardID) 
                bVisible = true
                
            end
            
            --HIDE CARD SLOT UNUSED
            local hC = hud.getComponent ( hUser, sPrefix..".ContainerGlobal" )
            if(hC)then
            
                if(bVisible == false)then
                    hud.setComponentOpacity ( hC, 127 )
                end
                --hud.setComponentVisible ( hC, bVisible )
                hud.setComponentActive( hC, bVisible )
            end

        end
        
    end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
