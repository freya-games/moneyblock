--------------------------------------------------------------------------------
--  Function......... : UpdateTotal
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.UpdateTotal ( )
--------------------------------------------------------------------------------
	
local nTotal=this.nCash ( )
for i=0, hashtable.getSize ( this.htResourceCount ( ) )-1
do

    
    local sCryptoID=hashtable.getKeyAt ( this.htResourceCount ( ),i )
    if(this.GetResourceType (sCryptoID )=="CRYPTO")
    then
        local nCount=hashtable.getAt ( this.htResourceCount ( ),i )
        local nPrice=hashtable.get (this.htResourcePrice ( ), sCryptoID )
        if(nPrice)
        then
            nTotal=nTotal+nPrice*nCount
        end
    end
end

if(not this.bUseDebugHUD ( ))then
    local hTotal = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelTotalPortfolio" )
    local hMaxBuy = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelUserBuyMax" )
    local hMaxSell = hud.getComponent ( this.getUser ( ), "PlayerMainHUD.LabelUserSellMax" )
    hud.setLabelText ( hTotal,""..nTotal  )
    hud.setLabelText ( hMaxBuy,""..this.nCash ( )  )
    hud.setLabelText ( hMaxSell,""..nTotal - this.nCash ( )  )

------------------------DEBUG HUD-------------------------------
else
    local hTotal = hud.getComponent ( this.getUser ( ), "PlayerHUD.Total" )
    hud.setLabelText ( hTotal,""..nTotal..this.GetCurrency ( )  )
end
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
