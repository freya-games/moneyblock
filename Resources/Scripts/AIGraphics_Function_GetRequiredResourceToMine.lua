--------------------------------------------------------------------------------
--  Function......... : GetRequiredResourceToMine
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.GetRequiredResourceToMine ( sResID, sCryptoID)
--------------------------------------------------------------------------------
	
    -- GET BLOCK LVL
    local nBlockLvl =0
    if(hashtable.contains ( this.htCryptoCurrentBlock ( ),sCryptoID ))
    then
        nBlockLvl = hashtable.get ( this.htCryptoCurrentBlock  ( ),sCryptoID )
    end
        
    -- 
    local nCount=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.ChildCount")
	for i=0, nCount-1
    do
        local sID=hashtable.get ( this.htGameConfig ( ),"MoneyBlockConfig.Game.Resources.Resource."..i..".ID")
        if(sID ==sCryptoID)
        then
            local sPathBlock = "MoneyBlockConfig.Game.Resources.Resource."..i..".BlockChain.Block."..nBlockLvl
            local nResourceCount =  hashtable.get ( this.htGameConfig ( ), sPathBlock..".ChildCount")
            for j=0 , nResourceCount-1
            do
                local sID = hashtable.get ( this.htGameConfig ( ),sPathBlock..".Resource."..j..".ID")
                if(sID == sResID )then
                    return hashtable.get ( this.htGameConfig ( ),sPathBlock..".Resource."..j..".Amount")
                end
            end
            
            
        end
    end
	
    return 0
    
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
