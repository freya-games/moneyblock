--------------------------------------------------------------------------------
--  Function......... : getTotalMiningResource
--  Author........... : 
--  Description...... : 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIMainMoneyBlock.getTotalMiningResource ( sCryptoID ,sResID)
--------------------------------------------------------------------------------
	
local nTotal=0	
for i =0, this.nPlayerCount ( )-1
do
  
    local sTypeCrypto=hashtable.get ( this.htRuntimeData ( ),"Game.Resources."..sCryptoID..".Type")
    local sTypeRes=hashtable.get ( this.htRuntimeData ( ),"Game.Resources."..sResID..".Type")
    if(sTypeCrypto=="CRYPTO")
    then
    
        local nBuy = hashtable.get( this.htCurrentBuyOrders( ),"Player."..i.."."..sCryptoID..".Count" )

        if(sTypeRes=="MINER" or sTypeRes=="ENERGY")
        then
            local nAmount= hashtable.get ( this.htMining ( ), "Player."..i.."."..sCryptoID.."."..sResID )
            
            if(nAmount)
            then
            
                nTotal=nTotal+nAmount
            end
        end
 
    end
   
end
local nAmountActionCard = hashtable.get ( this.htMining ( ), sCryptoID.."."..sResID )

if(not nAmountActionCard)
then
    nAmountActionCard=0
end

return nTotal + nAmountActionCard
	
--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
