--------------------------------------------------------------------------------
--  Function......... : EnableEndTurnHUD
--  Author........... :
--  Description...... :
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function AIGraphics.EnableEndTurnHUD ( bEnable, sTitle )
--------------------------------------------------------------------------------

    if(sTitle==nil)then sTitle="END TURN" end

    local sPrefix = this.sPrefixEndTurnHUD ( )
    if(sPrefix ~="")then
        local sAction = sPrefix..".Hide"

        if(bEnable==true)then
            sAction = sPrefix..".Show"
            this.bHasUserSeenLastScores ( true)

            local hTitle = hud.getComponent ( this.getUser ( ), this.sPrefixEndTurnHUD ( )..".LabelTitle")
            if(hTitle)then hud.setLabelText ( hTitle, sTitle ) end
        end

        if(hud.isActionRunning ( this.getUser ( ), sAction ))then hud.stopAction ( this.getUser ( ), sAction )end
        hud.callAction ( this.getUser ( ), sAction )
    end

--------------------------------------------------------------------------------
end
--------------------------------------------------------------------------------
